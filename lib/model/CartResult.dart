// To parse this JSON data, do
//
//     final cartResult = cartResultFromJson(jsonString);

import 'dart:convert';

List<CartResult> cartResultFromJson(String str) => List<CartResult>.from(json.decode(str).map((x) => CartResult.fromJson(x)));

String cartResultToJson(List<CartResult> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class CartResult {
  CartResult({
    this.cartId,
    this.proId,
    this.userId,
    this.cartDate,
    this.quantity,
    this.cartIp,
    this.proName,
    this.proTitle,
    this.proDescription,
    this.proStatus,
    this.proSku,
    this.photo,
    this.proCatid,
    this.proSubcatid,
    this.proTags,
    this.proColor,
    this.proMaterial,
    this.proDate,
    this.proPrice,
    this.proAfterprice,
    this.proSlider,
    this.proFeatured,
    this.proDailydeals,
    this.proDealcountdown,
    this.proFresh,
  });

  String cartId;
  String proId;
  String userId;
  String cartDate;
  String quantity;
  String cartIp;
  String proName;
  String proTitle;
  String proDescription;
  String proStatus;
  String proSku;
  String photo;
  String proCatid;
  String proSubcatid;
  String proTags;
  String proColor;
  String proMaterial;
  String proDate;
  String proPrice;
  String proAfterprice;
  String proSlider;
  String proFeatured;
  String proDailydeals;
  String proDealcountdown;
  String proFresh;

  factory CartResult.fromJson(Map<String, dynamic> json) => CartResult(
    cartId: json["cart_id"],
    proId: json["pro_id"],
    userId: json["user_id"],
    cartDate: json["cart_date"],
    quantity: json["quantity"],
    cartIp: json["cart_ip"],
    proName: json["pro_name"],
    proTitle: json["pro_title"],
    proDescription: json["pro_description"],
    proStatus: json["pro_status"],
    proSku: json["pro_sku"],
    photo: json["photo"],
    proCatid: json["pro_catid"],
    proSubcatid: json["pro_subcatid"],
    proTags: json["pro_tags"],
    proColor: json["pro_color"],
    proMaterial: json["pro_material"],
    proDate: json["pro_date"],
    proPrice: json["pro_price"],
    proAfterprice: json["pro_afterprice"],
    proSlider: json["pro_slider"],
    proFeatured: json["pro_featured"],
    proDailydeals: json["pro_dailydeals"],
    proDealcountdown: json["pro_dealcountdown"],
    proFresh: json["pro_fresh"],
  );

  Map<String, dynamic> toJson() => {
    "cart_id": cartId,
    "pro_id": proId,
    "user_id": userId,
    "cart_date": cartDate,
    "quantity": quantity,
    "cart_ip": cartIp,
    "pro_name": proName,
    "pro_title": proTitle,
    "pro_description": proDescription,
    "pro_status": proStatus,
    "pro_sku": proSku,
    "photo": photo,
    "pro_catid": proCatid,
    "pro_subcatid": proSubcatid,
    "pro_tags": proTags,
    "pro_color": proColor,
    "pro_material": proMaterial,
    "pro_date": proDate,
    "pro_price": proPrice,
    "pro_afterprice": proAfterprice,
    "pro_slider": proSlider,
    "pro_featured": proFeatured,
    "pro_dailydeals": proDailydeals,
    "pro_dealcountdown": proDealcountdown,
    "pro_fresh": proFresh,
  };
}
