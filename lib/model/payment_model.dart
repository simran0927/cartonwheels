/// fname : "asd"
/// lname : ""
/// addr1 : "asd"
/// postcode : "3000"
/// delivary_date : "Tuesday,03-11-2020"
/// email : "asdasd@afaasd.com"
/// phone : "989878787"
/// odr_total : "1"
/// pay_type : "wallet"
/// service_fee : "0"
/// pay_from_wallet : "1"
/// full_total : "1"
/// odrnote : "asdas"
/// did_not_find : "asdasds"
/// login_type : "app"
/// user_id : "82"

class PaymentModel {
  String _fname;
  String _lname;
  String _addr1;
  String _postcode;
  String _delivaryDate;
  String _email;
  String _phone;
  String _odrTotal;
  String _payType;
  String _serviceFee;
  String _payFromWallet;
  String _fullTotal;
  String _odrnote;
  String _didNotFind;
  String _loginType;
  String _userId;

  String get fname => _fname;
  String get lname => _lname;
  String get addr1 => _addr1;
  String get postcode => _postcode;
  String get delivaryDate => _delivaryDate;
  String get email => _email;
  String get phone => _phone;
  String get odrTotal => _odrTotal;
  String get payType => _payType;
  String get serviceFee => _serviceFee;
  String get payFromWallet => _payFromWallet;
  String get fullTotal => _fullTotal;
  String get odrnote => _odrnote;
  String get didNotFind => _didNotFind;
  String get loginType => _loginType;
  String get userId => _userId;

  PaymentModel({
      String fname, 
      String lname, 
      String addr1, 
      String postcode, 
      String delivaryDate, 
      String email, 
      String phone, 
      String odrTotal, 
      String payType, 
      String serviceFee, 
      String payFromWallet, 
      String fullTotal, 
      String odrnote, 
      String didNotFind, 
      String loginType, 
      String userId}){
    _fname = fname;
    _lname = lname;
    _addr1 = addr1;
    _postcode = postcode;
    _delivaryDate = delivaryDate;
    _email = email;
    _phone = phone;
    _odrTotal = odrTotal;
    _payType = payType;
    _serviceFee = serviceFee;
    _payFromWallet = payFromWallet;
    _fullTotal = fullTotal;
    _odrnote = odrnote;
    _didNotFind = didNotFind;
    _loginType = loginType;
    _userId = userId;
}

  PaymentModel.fromJson(dynamic json) {
    _fname = json["fname"];
    _lname = json["lname"];
    _addr1 = json["addr1"];
    _postcode = json["postcode"];
    _delivaryDate = json["delivary_date"];
    _email = json["email"];
    _phone = json["phone"];
    _odrTotal = json["odr_total"];
    _payType = json["pay_type"];
    _serviceFee = json["service_fee"];
    _payFromWallet = json["pay_from_wallet"];
    _fullTotal = json["full_total"];
    _odrnote = json["odrnote"];
    _didNotFind = json["did_not_find"];
    _loginType = json["login_type"];
    _userId = json["user_id"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["fname"] = _fname;
    map["lname"] = _lname;
    map["addr1"] = _addr1;
    map["postcode"] = _postcode;
    map["delivary_date"] = _delivaryDate;
    map["email"] = _email;
    map["phone"] = _phone;
    map["odr_total"] = _odrTotal;
    map["pay_type"] = _payType;
    map["service_fee"] = _serviceFee;
    map["pay_from_wallet"] = _payFromWallet;
    map["full_total"] = _fullTotal;
    map["odrnote"] = _odrnote;
    map["did_not_find"] = _didNotFind;
    map["login_type"] = _loginType;
    map["user_id"] = _userId;
    return map;
  }

}