import 'package:flutter/material.dart';

import 'food_model.dart';

class MyCart extends ChangeNotifier {
  List<CartItem> items = [];
  List<CartItem> get cartItems => items;

  bool addItem(CartItem cartItem) {
    for (CartItem cart in cartItems) {
      if (cartItem.food.shop.id != cart.food.shop.id) {
        return false;
      }
      if (cartItem.food.name == cart.food.name) {
        cartItems[cartItems.indexOf(cart)].quantity++;
        notifyListeners();
        return true;
      }
    }

    items.add(cartItem);
    notifyListeners();
    return true;
  }

  void clearCart() {
    items.clear();
    notifyListeners();
  }

  void decreaseItem(CartItem cartModel) {
    if (cartModel.quantity <= 1) {
      return;
    }
    cartModel.quantity -= 1;
    notifyListeners();
  }

  void increaseItem(CartItem cartModel) {
    print("called ${cartModel.food.price}");
    cartModel.quantity+= 1;
    for (CartItem cart in cartItems) {
      print(cart.quantity);

    }

    notifyListeners();
  }

  void removeAllInCart(Food food) {
    cartItems.removeWhere((f) {
      return f.food.name == food.name;
    });
    notifyListeners();
  }
}

class CartItem {
  Food food;
  int quantity;
  bool isLiked;

  CartItem({this.food, this.quantity, this.isLiked =true});
}
