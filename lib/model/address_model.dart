
import 'dart:convert';

List<AddressModel> addressModelFromJson(String str) => List<AddressModel>.from(json.decode(str).map((x) => AddressModel.fromJson(x)));

String addressModelToJson(List<AddressModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class AddressModel {
    String adr_user_id;
    String date;
    String del_address;
    String del_adr_id;
    String status;

    AddressModel({this.adr_user_id, this.date, this.del_address, this.del_adr_id, this.status});

    factory AddressModel.fromJson(Map<String, dynamic> json) {
        return AddressModel(
            adr_user_id: json['adr_user_id'],
            date: json['date'],
            del_address: json['del_address'],
            del_adr_id: json['del_adr_id'],
            status: json['status'],
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['adr_user_id'] = this.adr_user_id;
        data['date'] = this.date;
        data['del_address'] = this.del_address;
        data['del_adr_id'] = this.del_adr_id;
        data['status'] = this.status;
        return data;
    }
}