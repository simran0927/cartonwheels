
import 'dart:convert';

List<UserList> userListDataFromJson(String str) => List<UserList>.from(json.decode(str).map((x) => UserList.fromJson(x)));

String userListDataToJson(List<UserList> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class UserList {
    String date;
    String list_id;
    String list_name;
    String user_id;

    UserList({this.date, this.list_id, this.list_name, this.user_id});

    factory UserList.fromJson(Map<String, dynamic> json) {
        return UserList(
            date: json['date'],
            list_id: json['list_id'],
            list_name: json['list_name'],
            user_id: json['user_id'],
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['date'] = this.date;
        data['list_id'] = this.list_id;
        data['list_name'] = this.list_name;
        data['user_id'] = this.user_id;
        return data;
    }
}