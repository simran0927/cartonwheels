// To parse this JSON data, do
//
//     final productData = productDataFromJson(jsonString);

import 'dart:convert';

List<ProductData> productDataFromJson(String str) => List<ProductData>.from(json.decode(str).map((x) => ProductData.fromJson(x)));

String productDataToJson(List<ProductData> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ProductData {
  ProductData({
    this.proName,
    this.proTitle,
    this.proDescription,
    this.proStatus,
    this.proSku,
    this.photo,
    this.proCatid,
    this.proSubcatid,
    this.proTags,
    this.proColor,
    this.proMaterial,
    this.proDate,
    this.proPrice,
    this.proAfterprice,
    this.proSlider,
    this.proId,
    this.proFeatured,
    this.proDailydeals,
    this.proDealcountdown,
    this.proFresh,
    this.proStock
  });

  String proName;
  String proTitle;
  String proDescription;
  String proStatus;
  String proSku;
  String photo;
  String proCatid;
  String proSubcatid;
  String proTags;
  String proColor;
  String proMaterial;
  String proDate;
  String proPrice;
  String proAfterprice;
  String proSlider;
  String proId;
  String proFeatured;
  String proDailydeals;
  String proDealcountdown;
  String proFresh;
  String proStock;

  factory ProductData.fromJson(Map<String, dynamic> json) => ProductData(
    proName: json["pro_name"],
    proTitle: json["pro_title"],
    proDescription: json["pro_description"],
    proStatus: json["pro_status"],
    proSku: json["pro_sku"],
    photo: json["photo"],
    proCatid: json["pro_catid"],
    proSubcatid: json["pro_subcatid"],
    proTags: json["pro_tags"],
    proColor: json["pro_color"],
    proMaterial: json["pro_material"],
    proDate: json["pro_date"],
    proPrice: json["pro_price"],
    proAfterprice: json["pro_afterprice"],
    proSlider: json["pro_slider"],
    proId: json["pro_id"],
    proFeatured: json["pro_featured"],
    proDailydeals: json["pro_dailydeals"],
    proDealcountdown: json["pro_dealcountdown"],
    proFresh: json["pro_fresh"],
    proStock: json["pro_stock"]
  );

  Map<String, dynamic> toJson() => {
    "pro_name": proName,
    "pro_title": proTitle,
    "pro_description": proDescription,
    "pro_status": proStatus,
    "pro_sku": proSku,
    "photo": photo,
    "pro_catid": proCatid,
    "pro_subcatid": proSubcatid,
    "pro_tags": proTags,
    "pro_color": proColor,
    "pro_material": proMaterial,
    "pro_date": proDate,
    "pro_price": proPrice,
    "pro_afterprice": proAfterprice,
    "pro_slider": proSlider,
    "pro_id": proId,
    "pro_featured": proFeatured,
    "pro_dailydeals": proDailydeals,
    "pro_dealcountdown": proDealcountdown,
    "pro_fresh": proFresh,
    "pro_stock": proStock,
  };

  bool isStockAvailable() {
    return proStock.toLowerCase() != "yes";
  }
}
