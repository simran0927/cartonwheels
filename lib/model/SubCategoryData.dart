// To parse this JSON data, do
//
//     final subCategoryData = subCategoryDataFromJson(jsonString);

import 'dart:convert';

List<SubCategoryData> subCategoryDataFromJson(String str) => List<SubCategoryData>.from(json.decode(str).map((x) => SubCategoryData.fromJson(x)));

String subCategoryDataToJson(List<SubCategoryData> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class SubCategoryData {
  SubCategoryData({
    this.subcatId,
    this.catId,
    this.subcatName,
    this.subcatTitle,
    this.subcatStatus,
    this.subcatDesc,
    this.photo,
  });

  String subcatId;
  String catId;
  String subcatName;
  String subcatTitle;
  SubcatStatus subcatStatus;
  String subcatDesc;
  Photo photo;

  factory SubCategoryData.fromJson(Map<String, dynamic> json) => SubCategoryData(
    subcatId: json["subcat_id"],
    catId: json["cat_id"],
    subcatName: json["subcat_name"],
    subcatTitle: json["subcat_title"],
    subcatStatus: subcatStatusValues.map[json["subcat_status"]],
    subcatDesc: json["subcat_desc"],
    photo: photoValues.map[json["photo"]],
  );

  Map<String, dynamic> toJson() => {
    "subcat_id": subcatId,
    "cat_id": catId,
    "subcat_name": subcatName,
    "subcat_title": subcatTitle,
    "subcat_status": subcatStatusValues.reverse[subcatStatus],
    "subcat_desc": subcatDesc,
    "photo": photoValues.reverse[photo],
  };
}

enum Photo { EMPTY, BG_JPG }

final photoValues = EnumValues({
  "bg.jpg": Photo.BG_JPG,
  "": Photo.EMPTY
});

enum SubcatStatus { ACTIVE }

final subcatStatusValues = EnumValues({
  "Active": SubcatStatus.ACTIVE
});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
