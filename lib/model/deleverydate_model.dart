

import 'dart:convert';

List<DeleveryDateModel> deleveryDateModelFromJson(String str) => List<DeleveryDateModel>.from(json.decode(str).map((x) => DeleveryDateModel.fromJson(x)));

String deleveryDateModelToJson(List<DeleveryDateModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));


class DeleveryDateModel {
    String post_code;
    String post_day;
    String post_id;
    String status;
    String date;

    DeleveryDateModel({this.post_code, this.post_day, this.post_id, this.status});

    factory DeleveryDateModel.fromJson(Map<String, dynamic> json) {
        return DeleveryDateModel(
            post_code: json['post_code'],
            post_day: json['post_day'],
            post_id: json['post_id'],
            status: json['status'],
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['post_code'] = this.post_code;
        data['post_day'] = this.post_day;
        data['post_id'] = this.post_id;
        data['status'] = this.status;
        return data;
    }
}