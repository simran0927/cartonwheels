// To parse this JSON data, do
//
//     final orderHistory = orderHistoryFromJson(jsonString);

import 'dart:convert';

List<OrderHistory> orderHistoryFromJson(String str) => List<OrderHistory>.from(json.decode(str).map((x) => OrderHistory.fromJson(x)));

String orderHistoryToJson(List<OrderHistory> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class OrderHistory {
  OrderHistory({
    this.odrId,
    this.fname,
    this.lname,
    this.email,
    this.company,
    this.transactionId,
    this.productId,
    this.usrId,
    this.address,
    this.town,
    this.phone,
    this.postcode,
    this.odrnote,
    this.didNotFind,
    this.country,
    this.ordQuintity,
    this.odrTotal,
    this.delivaryDate,
    this.deliveryDateFormat,
    this.ordDate,
    this.ordStatus,
  });

  String odrId;
  String fname;
  String lname;
  String email;
  String company;
  String transactionId;
  String productId;
  String usrId;
  String address;
  String town;
  String phone;
  String postcode;
  String odrnote;
  String didNotFind;
  String country;
  String ordQuintity;
  String odrTotal;
  String delivaryDate;
  String deliveryDateFormat;
  DateTime ordDate;
  String ordStatus;

  factory OrderHistory.fromJson(Map<String, dynamic> json) => OrderHistory(
    odrId: json["odr_id"],
    fname: json["fname"],
    lname: json["lname"],
    email: json["email"],
    company: json["company"],
    transactionId: json["transaction_id"],
    productId: json["product_id"],
    usrId: json["usr_id"],
    address: json["address"],
    town: json["town"],
    phone: json["phone"],
    postcode: json["postcode"],
    odrnote: json["odrnote"],
    didNotFind: json["did_not_find"],
    country: json["country"],
    ordQuintity: json["ord_quintity"],
    odrTotal: json["odr_total"],
    delivaryDate: json["delivary_date"],
    deliveryDateFormat: json["delivery_date_format"],
    ordDate: DateTime.parse(json["ord_date"]),
    ordStatus: json["ord_status"],
  );

  Map<String, dynamic> toJson() => {
    "odr_id": odrId,
    "fname": fname,
    "lname": lname,
    "email": email,
    "company": company,
    "transaction_id": transactionId,
    "product_id": productId,
    "usr_id": usrId,
    "address": address,
    "town": town,
    "phone": phone,
    "postcode": postcode,
    "odrnote": odrnote,
    "did_not_find": didNotFind,
    "country": country,
    "ord_quintity": ordQuintity,
    "odr_total": odrTotal,
    "delivary_date": delivaryDate,
    "delivery_date_format": deliveryDateFormat,
    "ord_date": ordDate.toIso8601String(),
    "ord_status": ordStatus,
  };
}
