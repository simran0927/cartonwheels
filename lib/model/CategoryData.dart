// To parse this JSON data, do
//
//     final categoryData = categoryDataFromJson(jsonString);

import 'dart:convert';

List<CategoryData> categoryDataFromJson(String str) => List<CategoryData>.from(json.decode(str).map((x) => CategoryData.fromJson(x)));

String categoryDataToJson(List<CategoryData> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class CategoryData {
  CategoryData({
    this.catId,
    this.catName,
    this.catTitle,
    this.catStatus,
    this.catDesc,
    this.photo,
    this.catSlider,
  });

  String catId;
  String catName;
  String catTitle;
  CatStatus catStatus;
  String catDesc;
  String photo;
  CatSlider catSlider;

  factory CategoryData.fromJson(Map<String, dynamic> json) => CategoryData(
    catId: json["cat_id"],
    catName: json["cat_name"],
    catTitle: json["cat_title"],
    catStatus: catStatusValues.map[json["cat_status"]],
    catDesc: json["cat_desc"],
    photo: json["photo"],
    catSlider: catSliderValues.map[json["cat_slider"]],
  );

  Map<String, dynamic> toJson() => {
    "cat_id": catId,
    "cat_name": catName,
    "cat_title": catTitle,
    "cat_status": catStatusValues.reverse[catStatus],
    "cat_desc": catDesc,
    "photo": photo,
    "cat_slider": catSliderValues.reverse[catSlider],
  };
}

enum CatSlider { YES, NO }

final catSliderValues = EnumValues({
  "No": CatSlider.NO,
  "Yes": CatSlider.YES
});

enum CatStatus { ACTIVE }

final catStatusValues = EnumValues({
  "Active": CatStatus.ACTIVE
});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
