// To parse this JSON data, do
//
//     final loginData = loginDataFromJson(jsonString);

import 'dart:convert';

List<LoginData> loginDataFromJson(String str) => List<LoginData>.from(json.decode(str).map((x) => LoginData.fromJson(x)));

String loginDataToJson(List<LoginData> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class LoginData {
  LoginData({
    this.usersId,
    this.name,
    this.familyName,
    this.address2,
    this.city,
    this.state,
    this.address,
    this.phone,
    this.email,
    this.password,
    this.postcode,
    this.uniqueNumber,
    this.authenticationKey,
    this.photo,
    this.address1,
    this.postalCode,
    this.company,
    this.walletTotal,
    this.referPrice,
    this.referUsrId,
  });

  String usersId;
  String name;
  String familyName;
  String address2;
  String city;
  String state;
  String address;
  String phone;
  String email;
  String password;
  String postcode;
  dynamic uniqueNumber;
  dynamic authenticationKey;
  dynamic photo;
  String address1;
  String postalCode;
  String company;
  String walletTotal;
  String referPrice;
  String referUsrId;

  factory LoginData.fromJson(Map<String, dynamic> json) => LoginData(
    usersId: json["users_id"],
    name: json["name"],
    familyName: json["family_name"],
    address2: json["Address2"],
    city: json["city"],
    state: json["State"],
    address: json["address"],
    phone: json["phone"],
    email: json["email"],
    password: json["password"],
    postcode: json["postcode"],
    uniqueNumber: json["unique_number"],
    authenticationKey: json["authentication_key"],
    photo: json["photo"],
    address1: json["Address1"],
    postalCode: json["postal_code"],
    company: json["company"],
    walletTotal: json["wallet_total"],
    referPrice: json["refer_price"],
    referUsrId: json["refer_usr_id"],
  );

  Map<String, dynamic> toJson() => {
    "users_id": usersId,
    "name": name,
    "family_name": familyName,
    "Address2": address2,
    "city": city,
    "State": state,
    "address": address,
    "phone": phone,
    "email": email,
    "password": password,
    "postcode": postcode,
    "unique_number": uniqueNumber,
    "authentication_key": authenticationKey,
    "photo": photo,
    "Address1": address1,
    "postal_code": postalCode,
    "company": company,
    "wallet_total": walletTotal,
    "refer_price": referPrice,
    "refer_usr_id": referUsrId,
  };
}
