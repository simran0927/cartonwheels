// To parse this JSON data, do
//
//     final orderDetail = orderDetailFromJson(jsonString);

import 'dart:convert';

List<OrderDetail> orderDetailFromJson(String str) => List<OrderDetail>.from(json.decode(str).map((x) => OrderDetail.fromJson(x)));

String orderDetailToJson(List<OrderDetail> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class OrderDetail {
  OrderDetail({
    this.itemId,
    this.odrId,
    this.proId,
    this.proQuty,
    this.proPrice,
    this.status,
    this.date,
    this.proName,
    this.proTitle,
    this.proDescription,
    this.proStatus,
    this.proSku,
    this.photo,
    this.proCatid,
    this.proSubcatid,
    this.proTags,
    this.proColor,
    this.proMaterial,
    this.proDate,
    this.proAfterprice,
    this.proSlider,
    this.proFeatured,
    this.proStock,
    this.proDailydeals,
    this.proDealcountdown,
    this.proFresh,
    this.fname,
    this.lname,
    this.email,
    this.company,
    this.transactionId,
    this.productId,
    this.usrId,
    this.address,
    this.town,
    this.phone,
    this.postcode,
    this.odrnote,
    this.didNotFind,
    this.country,
    this.ordQuintity,
    this.odrTotal,
    this.delivaryDate,
    this.deliveryDateFormat,
    this.ordDate,
    this.ordStatus,
  });

  bool isSuccessful() {
    return ordStatus != null && ordStatus == "Payment Successful";
  }

  String itemId;
  String odrId;
  String proId;
  String proQuty;
  String proPrice;
  String status;
  DateTime date;
  String proName;
  String proTitle;
  String proDescription;
  String proStatus;
  String proSku;
  String photo;
  String proCatid;
  String proSubcatid;
  String proTags;
  String proColor;
  String proMaterial;
  String proDate;
  String proAfterprice;
  String proSlider;
  String proFeatured;
  String proStock;
  String proDailydeals;
  String proDealcountdown;
  String proFresh;
  String fname;
  String lname;
  String email;
  String company;
  String transactionId;
  String productId;
  String usrId;
  String address;
  String town;
  String phone;
  String postcode;
  String odrnote;
  String didNotFind;
  String country;
  String ordQuintity;
  String odrTotal;
  String delivaryDate;
  String deliveryDateFormat;
  DateTime ordDate;
  String ordStatus;

  factory OrderDetail.fromJson(Map<String, dynamic> json) => OrderDetail(
    itemId: json["item_id"],
    odrId: json["odr_id"],
    proId: json["pro_id"],
    proQuty: json["pro_quty"],
    proPrice: json["pro_price"],
    status: json["status"],
    date: DateTime.parse(json["date"]),
    proName: json["pro_name"],
    proTitle: json["pro_title"],
    proDescription: json["pro_description"],
    proStatus: json["pro_status"],
    proSku: json["pro_sku"],
    photo: json["photo"],
    proCatid: json["pro_catid"],
    proSubcatid: json["pro_subcatid"],
    proTags: json["pro_tags"],
    proColor: json["pro_color"],
    proMaterial: json["pro_material"],
    proDate: json["pro_date"],
    proAfterprice: json["pro_afterprice"],
    proSlider: json["pro_slider"],
    proFeatured: json["pro_featured"],
    proStock: json["pro_stock"],
    proDailydeals: json["pro_dailydeals"],
    proDealcountdown: json["pro_dealcountdown"],
    proFresh: json["pro_fresh"],
    fname: json["fname"],
    lname: json["lname"],
    email: json["email"],
    company: json["company"],
    transactionId: json["transaction_id"],
    productId: json["product_id"],
    usrId: json["usr_id"],
    address: json["address"],
    town: json["town"],
    phone: json["phone"],
    postcode: json["postcode"],
    odrnote: json["odrnote"],
    didNotFind: json["did_not_find"],
    country: json["country"],
    ordQuintity: json["ord_quintity"],
    odrTotal: json["odr_total"],
    delivaryDate: json["delivary_date"],
    deliveryDateFormat: json["delivery_date_format"],
    ordDate: DateTime.parse(json["ord_date"]),
    ordStatus: json["ord_status"],
  );

  Map<String, dynamic> toJson() => {
    "item_id": itemId,
    "odr_id": odrId,
    "pro_id": proId,
    "pro_quty": proQuty,
    "pro_price": proPrice,
    "status": status,
    "date": date.toIso8601String(),
    "pro_name": proName,
    "pro_title": proTitle,
    "pro_description": proDescription,
    "pro_status": proStatus,
    "pro_sku": proSku,
    "photo": photo,
    "pro_catid": proCatid,
    "pro_subcatid": proSubcatid,
    "pro_tags": proTags,
    "pro_color": proColor,
    "pro_material": proMaterial,
    "pro_date": proDate,
    "pro_afterprice": proAfterprice,
    "pro_slider": proSlider,
    "pro_featured": proFeatured,
    "pro_stock": proStock,
    "pro_dailydeals": proDailydeals,
    "pro_dealcountdown": proDealcountdown,
    "pro_fresh": proFresh,
    "fname": fname,
    "lname": lname,
    "email": email,
    "company": company,
    "transaction_id": transactionId,
    "product_id": productId,
    "usr_id": usrId,
    "address": address,
    "town": town,
    "phone": phone,
    "postcode": postcode,
    "odrnote": odrnote,
    "did_not_find": didNotFind,
    "country": country,
    "ord_quintity": ordQuintity,
    "odr_total": odrTotal,
    "delivary_date": delivaryDate,
    "delivery_date_format": deliveryDateFormat,
    "ord_date": ordDate.toIso8601String(),
    "ord_status": ordStatus,
  };
}
