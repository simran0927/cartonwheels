// To parse this JSON data, do
//
//     final walletTransactions = walletTransactionsFromJson(jsonString);

import 'dart:convert';

List<WalletTransactions> walletTransactionsFromJson(String str) => List<WalletTransactions>.from(json.decode(str).map((x) => WalletTransactions.fromJson(x)));

String walletTransactionsToJson(List<WalletTransactions> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class WalletTransactions {
  WalletTransactions({
    this.wltId,
    this.userId,
    this.money,
    this.paymentMethod,
    this.transactionId,
    this.date,
    this.wltStatus,
  });

  String wltId;
  String userId;
  String money;
  String paymentMethod;
  String transactionId;
  DateTime date;
  String wltStatus;

  factory WalletTransactions.fromJson(Map<String, dynamic> json) => WalletTransactions(
    wltId: json["wlt_id"],
    userId: json["user_id"],
    money: json["money"],
    paymentMethod: json["payment_method"],
    transactionId: json["transaction_id"],
    date: DateTime.parse(json["date"]),
    wltStatus: json["wlt_status"],
  );

  Map<String, dynamic> toJson() => {
    "wlt_id": wltId,
    "user_id": userId,
    "money": money,
    "payment_method": paymentMethod,
    "transaction_id": transactionId,
    "date": date.toIso8601String(),
    "wlt_status": wltStatus,
  };
}
