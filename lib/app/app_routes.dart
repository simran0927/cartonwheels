
import 'package:ecommerceapp/screens/details/product_details.dart';
import 'package:ecommerceapp/screens/forgot_pass/forgot_password.dart';
import 'package:ecommerceapp/screens/lists/SavedListsScreen.dart';
import 'package:ecommerceapp/screens/mainHome/MyHomePage.dart';
import 'package:ecommerceapp/screens/more/ContactUs.dart';
import 'package:ecommerceapp/screens/more/TermsConditions.dart';
import 'package:ecommerceapp/screens/orders/Allorders.dart';
import 'package:ecommerceapp/screens/orders/OrderDetailScreen.dart';
import 'package:ecommerceapp/screens/products/Products.dart';
import 'package:ecommerceapp/screens/products/product_item.dart';
import 'package:ecommerceapp/screens/register/sign_up.dart';
import 'package:ecommerceapp/screens/sub_category/sub_category_page.dart';
import 'package:flutter/material.dart';
import 'package:ecommerceapp/screens/login/login.dart';

class PageRouter {
  static Map<String, WidgetBuilder> routes() {
    return {
     // LoginView.routeName:(ctx)=> LoginView(),
      LoginView.routeName:(ctx)=> LoginView(),
      SignUpScreen.routeName:(ctx)=>SignUpScreen(),
      MyHomePage.routeName:(ctx)=>MyHomePage(),
      Products.routeName:(ctx)=>Products(),
      ProductDetailScreen.routeName:(ctx)=>ProductDetailScreen(),
      ForgotPasswordScreen.routeName:(ctx)=>ForgotPasswordScreen(),
      SavedListsScreen.routeName:(ctx)=>SavedListsScreen(),
      AllOrders.routeName:(ctx)=>AllOrders(),
      TermsConditions.routeName:(ctx)=> TermsConditions(),
      SubCategoryPage.routeName:(ctx) => SubCategoryPage(),
      ContactUs.routeName:(ctx)=>ContactUs(),
      OrderDetailScreen.routeName:(ctx)=>OrderDetailScreen()
    };
  }
}
