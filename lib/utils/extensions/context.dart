


import 'package:ecommerceapp/utils/alert/EdgeAlert.dart';
import 'package:flutter/material.dart';


extension contextUtil on BuildContext {

  showAlert(String desc) {
    EdgeAlert.show(this, title: 'Alert', description: desc, gravity: EdgeAlert.TOP, icon: Icons.warning, backgroundColor: Colors.redAccent, duration: 3);
  }

  showInfoAlert(String desc) {
    EdgeAlert.show(this, title: 'Info', description: desc, gravity: EdgeAlert.TOP, icon: Icons.info_outline, backgroundColor: Colors.blueGrey, duration: 3);
  }

  showSuccessAlert(String desc) {
    EdgeAlert.show(this, title: 'Alert', description: desc, gravity: EdgeAlert.TOP, icon: Icons.warning, backgroundColor: Colors.green, duration: 3);
  }

}