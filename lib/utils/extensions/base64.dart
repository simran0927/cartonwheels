import 'dart:convert';

extension Base64 on String {


  String encodeBase64() {
    Codec<String, String> func = utf8.fuse(base64);
    return func.encode(this);
  }

  String decodeBase64() {
    Codec<String, String> func = utf8.fuse(base64);
    return func.decode(this);
  }


  bool isBase64() {
    RegExp _base64 = RegExp(
        r'^(?:[A-Za-z0-9+\/]{4})*(?:[A-Za-z0-9+\/]{2}==|[A-Za-z0-9+\/]{3}=|[A-Za-z0-9+\/]{4})$');
    return _base64.hasMatch(this);
  }
}
