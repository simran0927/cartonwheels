
extension ParseNumbers on String {
  int parseInt() {
    try {
    return int.parse(this);
    } catch(e) {
      return 0;
    }
  }
  double parseDouble() {
    try {
      return double.parse(this);
    } catch(e) {
      return 0.0;
    }
  }
}