import 'package:intl/intl.dart';

class AppUtils {
  AppUtils._();


  static String formatDate(dateString){
    DateFormat formatter, FORMATTER;
    formatter = new DateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSz");
    String oldDate = dateString;
    DateTime date = formatter.parse(oldDate.substring(0, 26) + oldDate.substring(27, 29));
    FORMATTER = new DateFormat("MM/dd/yyyy");


    return FORMATTER.format(date);
  }

  static String formatTime(dateString){
    DateFormat formatter, FORMATTER;
    formatter = new DateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSz");
    String oldDate = dateString;
    DateTime date = formatter.parse(oldDate.substring(0, 26) + oldDate.substring(27, 29));
    FORMATTER = new DateFormat("hh:mm a");


    return FORMATTER.format(date);
  }


}

