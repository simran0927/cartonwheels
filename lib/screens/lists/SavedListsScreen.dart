 import 'package:ecommerceapp/base/base_state.dart';
import 'package:ecommerceapp/base/base_stateful_widget.dart';
import 'package:ecommerceapp/base/base_statefull_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/framework.dart';

class SavedListsScreen extends BaseStatefulWidget{
  static const routeName = '/saved_list';
  @override
  _SavedListsScreenState createState() => _SavedListsScreenState();

}


class _SavedListsScreenState extends BaseState<SavedListsScreen> with BaseStatefulScreen{
  @override
  Widget buildBody(BuildContext context) {
    return Container();
  }

  @override
  String screenName()=>null;

}