import 'package:dio/dio.dart';
import 'package:ecommerceapp/commons/constants/app_constants.dart';
import 'package:ecommerceapp/commons/repo/UserRepository.dart';
import 'package:ecommerceapp/model/LoginData.dart';

import 'package:ecommerceapp/network/dio_rest_client.dart';

class UserService {
  Future<bool> updateUser(
      name,lname, email, phone, address) async {
    ApiResponse apiResponse =
        await ApiHitter.getPostApiResponse(AppConstants.UPDATE_USER,
            formData: FormData.fromMap({
              "users_id":UserRepo.user.usersId,
              "name": name,
              "family_name":lname,
              "email": email,
              "phone": phone,
              "Address1": address,
              "Address2": UserRepo.user.address2,
              "company": UserRepo.user.company,
              "postcode": UserRepo.user.postcode,
              "city": UserRepo.user.city,
              "password": UserRepo.user.password,
            }));
    try {
      return apiResponse.response != null
          ? true
          : false;
    } catch (e) {
      print(e..toString());
      return false;
    }
  }
}
