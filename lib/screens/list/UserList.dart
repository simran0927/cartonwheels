import 'package:ecommerceapp/app/app_theme.dart';
import 'package:ecommerceapp/commons/repo/UserRepository.dart';
import 'package:ecommerceapp/model/OrderHistory.dart';
import 'package:ecommerceapp/model/UserListData.dart';
import 'package:ecommerceapp/screens/list/service/ListService.dart';
import 'package:ecommerceapp/screens/orders/OrderDetailScreen.dart';
import 'package:ecommerceapp/screens/orders/OrderDetailScreen.dart';
import 'package:ecommerceapp/screens/orders/service/OrderService.dart';
import 'package:ecommerceapp/utils/extensions/basic_extensions.dart';
import 'package:ecommerceapp/utils/responsive/size_extension.dart';
import 'package:ecommerceapp/widgets/ProgressDialogType.dart' as pr;
import 'package:flutter/material.dart';
import 'package:focus_detector/focus_detector.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';

import 'UserListDetail.dart';

class UserListScreen extends StatefulWidget {
  @override
  _UserListScreenState createState() => _UserListScreenState();
}

class _UserListScreenState extends State<UserListScreen> {
  pr.ProgressDialog progressDialog;
  ListService listService;
  List<UserList> userLists;

  OrderService orderService;
  List<OrderHistory> ordersList;

  @override
  void initState() {
    super.initState();
    progressDialog = pr.ProgressDialog(context,
        type: pr.ProgressDialogType.Normal,
        isDismissible: false,
        showLogs: true);
    listService = ListService();
    orderService=OrderService();
    getAllLists();
    getAllOrders();
  }

  final _resumeDetectorKey = UniqueKey();

  @override
  Widget build(BuildContext context) {
    return FocusDetector(
      key: _resumeDetectorKey,
      onFocusGained: () {
        getAllLists();
        getAllOrders();
      },
      child: SingleChildScrollView(
          child: Column(
            children: [
              Container(

                child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
                InkWell(
                  onTap: () async {
                    final String listName = await showDialog(
                        context: context, builder: (context) => AddListDialog());
                    if (listName != null && listName.isNotEmpty) {
                      createList(UserRepo.user.usersId, listName);
                    }
                  },
                  child: Container(
                    height: 50.h,
                    color: Colors.black12,
                    child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text("CREATE NEW LIST",
                                style: TextStyle(
                                    fontSize: 16,
                                    color: AppTheme.primaryColorDark,
                                    fontWeight: FontWeight.w600))
                            .paddingLeft(16)),
                  ).elevation(0, elevation: 3),
                ),
                Text("Lists",
                        style: TextStyle(
                            fontSize: 16,
                            color: Colors.black87,
                            fontWeight: FontWeight.w600))
                    .paddingAll(left: 16, right: 16, top: 12),
                userLists != null && userLists.isNotEmpty
                    ? ListView.builder(
                        itemCount: userLists.length,
                        shrinkWrap: true,
                        itemBuilder: (context, pos) {
                          return InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            UserListDetail(userLists[pos])));
                              },
                              child: ListItem(userLists[pos], (listId) async {
                                final shouldDelete = await showDialog(
                                    context: context,
                                    builder: (context) {
                                      return AlertDialog(
                                          title: Text(
                                              "Are you sure you want to delete the list?"),
                                          actions: <Widget>[
                                            FlatButton(
                                                onPressed: () {
                                                  Navigator.pop(context, false);
                                                },
                                                child: Text("Keep",
                                                    style: TextStyle(
                                                        fontWeight: FontWeight.w500,
                                                        color: AppTheme
                                                            .primaryColorDark))),
                                            FlatButton(
                                                onPressed: () {
                                                  Navigator.pop(context, true);
                                                },
                                                child: Text("Delete",
                                                    style: TextStyle(
                                                        fontWeight: FontWeight.w500,
                                                        color:
                                                            AppTheme.primaryColorDark)))
                                          ]);
                                    });
                                if (shouldDelete != null && shouldDelete) {
                                  deleteList(userLists[pos]);
                                }
                              }));
                        }).paddingTop(16)
                    : Container(
                  child: Center(
                      child: Text(
                        "You have no list created.",
                        style: TextStyle(
                            color: AppTheme.primaryColorDark,
                            fontWeight: FontWeight.w500),
                      ).paddingTop(33)),
                )
        ],
      ),
              ),
              SizedBox(height: 28,),
              ordersList!=null && ordersList.length>0?
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Previous Orders",
                      style: TextStyle(
                          fontSize: 16,
                          color: Colors.black87,
                          fontWeight: FontWeight.w600))
                      .paddingAll(left: 16, right: 16, top: 12),
SizedBox(height: 16,),
                      ListView.builder(
                        shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemCount: ordersList.length,
                          itemBuilder: (context,pos){
                        return InkWell(
                          onTap: (){
                            Navigator.of(context).pushNamed(OrderDetailScreen.routeName,arguments: ordersList[pos]);
                          },
                          child: OrderListItem(ordersList[pos]),
                        );
                      })
                ],
              ):SizedBox()
            ],
          )),
    );
  }

  void createList(String userId, String listName) async {
    await progressDialog.show();
    listService
        .createList(userId, listName)
        .then((value) {})
        .whenComplete(() async {
      await progressDialog.hide();
      getAllLists();
    });
  }

  void deleteList(UserList list) async {
    await progressDialog.show();
    listService.deleteList(list.list_id).then((value) {

    }).whenComplete(() async {
      await progressDialog.hide();
      getAllLists();
    });
  }

  void getAllLists() async {
    listService.getAllUserLists(UserRepo.user.usersId).then((value) {
      if(value != null)
        setState(() {
          userLists = value;
        });
    }).whenComplete(() async {
    });
  }
  void getAllOrders() async {

    orderService.seeAllOrders(UserRepo.user.usersId).then((value) {
      if (value != null && value.length > 0) {
        setState(() {
          ordersList = value.reversed.toList();
        });
      }
    }).whenComplete(() async {

    });
  }
}



class ListItem extends StatelessWidget {
  final UserList userList;
  final Function(String) onDeletePressed;

  ListItem(this.userList, this.onDeletePressed);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          height: 45.h,
          color: Colors.white,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text("${userList.list_name}",
                      style: TextStyle(
                          fontSize: 16,
                          color: AppTheme.primaryColorDark,
                          fontWeight: FontWeight.w600))
                  .paddingLeft(16),
              Spacer(),
              IconButton(
                      icon: Icon(
                        Icons.delete,
                        color: Colors.black87,
                      ),
                      onPressed: () {
                        onDeletePressed(userList.list_id);
                      }).paddingRight(8)
            ],
          ),
        ),
        Container(
          color: Colors.black38,
          height: 0.6,
        )
      ],
    );
  }
}

class OrderListItem extends StatelessWidget {
  final OrderHistory orderList;


  OrderListItem(this.orderList);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          height: 45.h,
          color: Colors.white,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(" ${DateFormat(DateFormat.WEEKDAY).format(orderList.ordDate)} ,  ${DateFormat("dd-MM-yyyy").format(orderList.ordDate)}",
                  style: TextStyle(
                      fontSize: 16,
                      color: AppTheme.primaryColorDark,
                      fontWeight: FontWeight.w600))
                  .paddingLeft(16),
            ],
          ),
        ),
        Container(
          color: Colors.black38,
          height: 0.6,
        )
      ],
    );
  }
}


class AddListDialog extends StatefulWidget {
  @override
  _AddListDialogState createState() => _AddListDialogState();
}

class _AddListDialogState extends State<AddListDialog> {
  final textController = TextEditingController();
  bool saveDisabled = true;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      elevation: 3,
      backgroundColor: Colors.white,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
      child: IntrinsicHeight(
        child: Container(
          padding: EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text(
                "Create List",
                style: TextStyle(fontSize: 22),
              ),
              IntrinsicHeight(
                  child: TextField(
                autofocus: true,
                controller: textController,
                onChanged: (value) {
                  setState(() {
                    saveDisabled = value.isEmpty;
                  });
                },
              )).paddingTop(8),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Text(
                      "DISMISS",
                      style: TextStyle(
                          fontSize: 16, color: AppTheme.primaryColorDark),
                    ).paddingRight(16),
                  ),
                  InkWell(
                    onTap: () {
                      if (textController.text.isNotEmpty) {
                        Navigator.pop(context, textController.text);
                      }
                    },
                    child: Text(
                      "SAVE",
                      style: TextStyle(
                          fontSize: 16,
                          color: saveDisabled
                              ? Colors.grey
                              : AppTheme.primaryColorDark),
                    ),
                  ),
                ],
              ).paddingAll(top: 30)
            ],
          ),
        ),
      ),
    );
  }
}
