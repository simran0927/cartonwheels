import 'package:ecommerceapp/app/app_theme.dart';
import 'package:ecommerceapp/commons/repo/UserRepository.dart';
import 'package:ecommerceapp/constants/values.dart';
import 'package:ecommerceapp/model/ProductData.dart';
import 'package:ecommerceapp/model/UserListData.dart';
import 'package:ecommerceapp/screens/cart/cart_bottom_sheet.dart';
import 'package:ecommerceapp/screens/cart/provider/UpdateCartProvider.dart';
import 'package:ecommerceapp/screens/list/service/ListService.dart';
import 'package:ecommerceapp/screens/search/SearchScreen.dart';
import 'package:ecommerceapp/utils/extensions/basic_extensions.dart';
import 'package:ecommerceapp/utils/responsive/size_extension.dart';
import 'package:ecommerceapp/widgets/ProgressDialogType.dart' as pr;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

import 'checked_product_item.dart';

class UserListDetail extends StatefulWidget {
  final UserList userList;

  UserListDetail(this.userList);

  @override
  _UserListDetailState createState() => _UserListDetailState();
}

class _UserListDetailState extends State<UserListDetail> {
  pr.ProgressDialog progressDialog;
  ListService listService;
  List<ProductData> productLists = [];
  UpdateCartProvider updateCartProvider;

  @override
  void initState() {
    super.initState();
    progressDialog = pr.ProgressDialog(context,
        type: pr.ProgressDialogType.Normal,
        isDismissible: false,
        showLogs: true);
    listService = ListService();
    getAllListProducts(widget.userList.list_id);
  }

  @override
  Widget build(BuildContext context) {
    updateCartProvider = Provider.of<UpdateCartProvider>(context);
    return Scaffold(
        appBar: AppBar(
          backgroundColor: AppTheme.primaryColorDark,
          title: Text(
            widget.userList.list_name,
            overflow: TextOverflow.ellipsis,
          ),
          centerTitle: true,
          actions: <Widget>[
            Stack(
              children: [
                IconButton(
                  iconSize: 18,
                  onPressed: () {
                    showModalBottomSheet(
                      shape: roundedRectangle40,
                      context: context,
                      builder: (context) => CartBottomSheet(),
                    );
                  },
                  icon: Icon(FontAwesomeIcons.shoppingCart),
                  color: Colors.white,
                ),
                updateCartProvider.items != null && updateCartProvider.items.length > 0 ? Positioned(
                  right: 4,
                  top: 10,
                  child: Container(
                    height: 17,
                    width: 17,
                    decoration:
                    BoxDecoration(color: Colors.red, shape: BoxShape.circle),
                    child: Center(
                      child: Text(
                          updateCartProvider.items.length.toString(),

                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 10,
                              color: Colors.white)
                      ),
                    ),
                  ),
                ): SizedBox(),
              ],
            ),
            IconButton(
              icon: Icon(Icons.search).paddingRight(8),
              onPressed: () async {
                await Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            SearchScreen(listId: widget.userList.list_id)));
                getAllListProducts(widget.userList.list_id);
              },
            ),
            /*PopupMenuButton(
              elevation: 3.2,
              initialValue: "Nothing",
              onCanceled: () {},
              tooltip: 'This is tooltip',
              onSelected: (value) {},
              itemBuilder: (BuildContext context) {
                return [
                  PopupMenuItem(
                    value: "Clear all",
                    child: Text("Clear all"),
                  )
                ];
              },
            )*/
          ],
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                height: 50.h,
                color: Colors.black12,
                child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text("${productLists.length} Product",
                            style: TextStyle(
                                fontSize: 16,
                                color: AppTheme.primaryColorDark,
                                fontWeight: FontWeight.w600))
                        .paddingLeft(16)),
              ).elevation(0, elevation: 3),
              Container(
                child: Center(
                    child: Text(
                  "Search products to add them in the list",
                  style: TextStyle(
                      color: AppTheme.primaryColorDark,
                      fontWeight: FontWeight.w500),
                ).paddingAll(top: 33, bottom: 16)),
              ),
              ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: productLists?.length ?? 0,
                  shrinkWrap: true,
                  itemBuilder: (context, pos) {
                    return ListProductItem((id) async {
                      final shouldDelete = await showDialog(
                          context: context,
                          builder: (context) {
                            return AlertDialog(
                                title: Text(
                                    "Are you sure you want to remove this product from list?"),
                                actions: <Widget>[
                                  FlatButton(
                                      onPressed: () {
                                        Navigator.pop(context, false);
                                      },
                                      child: Text("Keep",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w500,
                                              color:
                                                  AppTheme.primaryColorDark))),
                                  FlatButton(
                                      onPressed: () {
                                        Navigator.pop(context, true);
                                      },
                                      child: Text("Delete",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w500,
                                              color:
                                                  AppTheme.primaryColorDark)))
                                ]);
                          });
                      if (shouldDelete != null && shouldDelete) {
                        deleteList(
                            widget.userList.list_id, productLists[pos].proId);
                      }
                    }, (number) async {
                      /* final shouldAdd = await showDialog(
                              context: context,
                              builder: (context) {
                                return ConfirmDialogWidget(
                                    "Are you sure you want to add the product to the cart?");
                              });
                          if(shouldAdd != null && shouldAdd)*/
                      addItemToCart(productLists[pos].proId,number);
                    }, productLists[pos]);
                  }).paddingTop(16)
            ],
          ),
        ));
  }

  getAllListProducts(listId) async {
    await progressDialog.show();
    listService.getListData(listId).then((value) {
      setState(() {
        productLists = value;
      });
    }).whenComplete(() async {
      await progressDialog.hide();
    });
  }

  void deleteList(listId, proId) async {
    await progressDialog.show();
    listService
        .removeProductFromList(listId, proId)
        .then((value) {})
        .whenComplete(() async {
      await progressDialog.hide();
      getAllListProducts(listId);
    });
  }

  void addItemToCart(proId,quantity) async {
    await progressDialog.show();
    listService
        .addItemToCart(proId, UserRepo.user.usersId,quantity: quantity)
        .then((value) {})
        .whenComplete(() async {
      await progressDialog.hide();
      showDialog(
          context: context,
          builder: (context) {
            Future.delayed(Duration(seconds: 2), () {
              updateCartProvider.fetchCartItems();
              Navigator.of(context).pop(true);
            });
            return AlertDialog(
              title: Center(child: Text('Product added to cart.',style: TextStyle(color: AppTheme.primaryColorDark),)),
            );
          });

    });
  }
}
