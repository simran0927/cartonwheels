

import 'package:dio/dio.dart';
import 'package:ecommerceapp/commons/constants/app_constants.dart';
import 'package:ecommerceapp/model/CartResult.dart';
import 'package:ecommerceapp/model/ProductData.dart';
import 'package:ecommerceapp/model/UserListData.dart';
import 'package:ecommerceapp/model/WishlistData.dart';
import 'package:ecommerceapp/network/dio_rest_client.dart';

class  ListService{

  Future<List<UserList>> getAllUserLists(userId) async {
    ApiResponse apiResponse =
    await ApiHitter.getPostApiResponse(AppConstants.LISTS, formData: FormData.fromMap({
      "userid": userId
    }));
    try {
      return apiResponse.response != null
          ?userListDataFromJson(apiResponse.response.data)
          : null;
    } catch (e) {
      print(e..toString());
      return null;
    }
  }

  Future<bool> createList(userId, name) async {
    ApiResponse apiResponse =
    await ApiHitter.getPostApiResponse(AppConstants.CREATE_LISTS, formData: FormData.fromMap({
      "user_id": userId,
      "list_name": name
    }));
    try {
      return apiResponse.response != null && apiResponse.response.data is bool && apiResponse.response.data == true
          ? true
          : false;
    } catch (e) {
      print(e..toString());
      return false;
    }
  }

  Future<bool> deleteList(listId) async {
    ApiResponse apiResponse =
    await ApiHitter.getPostApiResponse(AppConstants.DELETE_LISTS, formData: FormData.fromMap({
      "list_id": listId
    }));
    try {
      return apiResponse.response != null
          ? true
          : false;
    } catch (e) {
      print(e..toString());
      return false;
    }
  }

  Future<bool> removeProductFromList(listId, proId) async {
    ApiResponse apiResponse =
    await ApiHitter.getPostApiResponse(AppConstants.DELETE_LIST_PRODUCT, formData: FormData.fromMap({
      "listid": listId,
      "proid": proId
    }));
    try {
      return apiResponse.response != null
          ? true
          : false;
    } catch (e) {
      print(e..toString());
      return false;
    }
  }

  Future<bool> addItemToCart(proId, userId, {quantity = "1"}) async {
    ApiResponse apiResponse =
    await ApiHitter.getPostApiResponse(AppConstants.ADD_TO_CART, formData: FormData.fromMap({
      "pro_id": proId,
      "quantity": quantity,
      "user_id": userId
    }));
    try {
      return apiResponse.response != null
          ? true
          : false;
    } catch (e) {
      print(e..toString());
      return false;
    }
  }


  Future<List<ProductData>> getListData(listId) async {
    ApiResponse apiResponse =
    await ApiHitter.getPostApiResponse(AppConstants.LIST_ITEMS, formData: FormData.fromMap({
      "list_id": listId
    }));
    try {
      return apiResponse.response != null
          ?productDataFromJson(apiResponse.response.data)
          : null;
    } catch (e) {
      print(e..toString());
      return null;
    }
  }


  Future<bool> addProductToList(listId, product_id) async {
    ApiResponse apiResponse =
    await ApiHitter.getPostApiResponse(AppConstants.SAVE_PRODUCT_LIST, formData: FormData.fromMap({
      "listid": listId,
      "product_id[0]": product_id
    }));
    try {
      return apiResponse.response != null
          ?true
          : false;
    } catch (e) {
      print(e..toString());
      return false;
    }
  }
}