import 'package:ecommerceapp/app/app_theme.dart';
import 'package:ecommerceapp/commons/repo/UserRepository.dart';
import 'package:ecommerceapp/model/WalletTransactions.dart';
import 'package:ecommerceapp/screens/wallet/service/WalletService.dart';
import 'package:ecommerceapp/widgets/ProgressDialogType.dart';
import 'package:flutter/material.dart';
import 'package:focus_detector/focus_detector.dart';

class WalletScreen extends StatefulWidget {
  @override
  _WalletScreenState createState() => _WalletScreenState();
}

class _WalletScreenState extends State<WalletScreen> {

  WalletService walletService;
  List<WalletTransactions> transactions;
  String walletAmount;
  //ProgressDialog progressDialog;

  @override
  void initState() {
    super.initState();
    /*progressDialog = ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: true);*/
    walletService = WalletService();
    getAllWalletAmount();
    getAllWalletTransactions();
  }

  void getAllWalletAmount() async {
    //await progressDialog.show();
    walletService.getWalletTotal(UserRepo.user.usersId).then((value) {
      if (value != null && !value.contains("div")) {
        setState(() {
         walletAmount=value;
         try {
           UserRepo.walletAmount = walletAmount!=null ? double.parse(walletAmount.replaceAll("\"", "")) :0.00;
         } catch (e) {
           print(e);
         }
        });
      }
    }).whenComplete(() async {
      //await progressDialog.hide();
    });
  }

  void getAllWalletTransactions() async {
    //await progressDialog.show();
    walletService.getWalletTransactions(UserRepo.user.usersId).then((value) {
      if (value != null && value.length>0) {
        setState(() {
          transactions=value;
        });
      }
    }).whenComplete(() async {
      //await progressDialog.hide();
    });
  }
  final _resumeDetectorKey = UniqueKey();

  @override
  Widget build(BuildContext context) {
    return FocusDetector(
      key: _resumeDetectorKey,
      onFocusGained: () {
        getAllWalletAmount();
        getAllWalletTransactions();
      },
      child: Scaffold(
        body: SingleChildScrollView(
            child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Card(
                margin: EdgeInsets.only(left: 13, right: 13, top: 13),
                child: SizedBox(
                    width: MediaQuery.of(context).size.width,
                    height: 150,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                            child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            Image.asset(
                              "assets/images/ic_wallet.png",
                              height: 80,
                              width: 80,
                            ),
                            SizedBox(
                              height: 16,
                            )
                          ],
                        )),
                        Expanded(
                          child: FittedBox(
                            child: Text(
                              "\$ ${walletAmount!=null ?walletAmount.replaceAll("\"", ""):0.00}",
                              style: TextStyle(
                                  color: AppTheme.yellow,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 40),
                            ),
                          ),
                        ),
                        Expanded(child: SizedBox())
                      ],
                    )),
                color: AppTheme.primaryColorDark,
                elevation: 3,
                shadowColor: AppTheme.primaryColorDark,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12)),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 16, bottom: 8, top: 8),
                child: Text(
                  "*you can use upto 10\$ for an order.",
                  style: TextStyle(
                      color: AppTheme.yellow,
                      fontWeight: FontWeight.w500,
                      fontSize: 14),
                ),
              ),
              transactions!=null && transactions.length>0?Padding(
                padding: const EdgeInsets.only(left: 16, bottom: 16, top: 22),
                child: Text(
                  "Transactions".toUpperCase(),
                  style: TextStyle(
                      color: AppTheme.yellow,
                      fontWeight: FontWeight.w500,
                      fontSize: 18),
                ),
              ):SizedBox(),
              transactions!=null && transactions.length>0?ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: transactions.length,
                  shrinkWrap: true,
                  itemBuilder: (context, pos) {
                    return buildTransactionCard(context, pos % 2 == 0,transactions[pos]);
                  }):SizedBox()
            ],
          ),
        )),
      ),
    );
  }

  Card buildTransactionCard(BuildContext context, isAdded,WalletTransactions walletTransaction ) {
    return Card(
      margin: EdgeInsets.only(left: 13, right: 13, top: 8),
      color: AppTheme.primaryColorDark,
      elevation: 3,
      shadowColor: AppTheme.primaryColorDark,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
      child: SizedBox(
        width: MediaQuery.of(context).size.width,
        height: 65,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
                padding: EdgeInsets.only(left: 8),
                child: Icon(
                  Icons.add_box,
                  color:AppTheme.yellow,
                )),
            SizedBox(
              width: 22,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Added",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 14,
                      fontWeight: FontWeight.w300),
                ),
                SizedBox(
                  height: 8,
                ),
                Text(
                  "\$${walletTransaction.money.replaceAll("+", "")}",
                  style: TextStyle(
                      color: AppTheme.yellow,
                      fontSize: 12,
                      fontWeight: FontWeight.bold),
                )
              ],
            ),
            Expanded(
                child: Align(
              alignment: Alignment.centerRight,
              child: Padding(
                padding: EdgeInsets.only(right: 8),
                child: Text(
                  walletTransaction?.wltStatus ?? "",
                  style: TextStyle(
                      color: AppTheme.white,
                      fontSize: 12,
                      fontWeight: FontWeight.w300),
                ),
              ),
            ))
          ],
        ),
      ),
    );
  }
}
