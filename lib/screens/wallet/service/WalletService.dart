

import 'package:dio/dio.dart';
import 'package:ecommerceapp/commons/constants/app_constants.dart';
import 'package:ecommerceapp/model/CartResult.dart';
import 'package:ecommerceapp/model/WalletTransactions.dart';
import 'package:ecommerceapp/model/WishlistData.dart';
import 'package:ecommerceapp/network/dio_rest_client.dart';

class  WalletService {

  Future<String> getWalletTotal(userId) async {
    ApiResponse apiResponse =
    await ApiHitter.getPostApiResponse(
        AppConstants.WALLET_TOTAL, formData: FormData.fromMap({
      "user_id": userId
    }),isDev: true);
    try {
      return apiResponse.response != null
          ? apiResponse.response.data
          : null;
    } catch (e) {
      print(e..toString());
      return null;
    }
  }


  Future<List<WalletTransactions>> getWalletTransactions(userId) async {
    ApiResponse apiResponse =
    await ApiHitter.getPostApiResponse(
        AppConstants.WALLET_TRANSACTION, formData: FormData.fromMap({
      "user_id": userId
    }),isDev: true);
    try {
      return apiResponse.response != null
          ? walletTransactionsFromJson(apiResponse.response.data)
          : null;
    } catch (e) {
      print(e..toString());
      return null;
    }
  }

}

