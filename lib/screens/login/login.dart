
import 'package:ecommerceapp/app/app_theme.dart';
import 'package:ecommerceapp/base/base_state.dart';
import 'package:ecommerceapp/base/base_stateful_widget.dart';
import 'package:ecommerceapp/base/base_statefull_screen.dart';
import 'package:ecommerceapp/commons/repo/UserRepository.dart';
import 'package:ecommerceapp/screens/forgot_pass/forgot_password.dart';
import 'package:ecommerceapp/screens/login/service/LoginService.dart';
import 'package:ecommerceapp/screens/mainHome/MyHomePage.dart';
import 'package:ecommerceapp/screens/register/sign_up.dart';
import 'package:ecommerceapp/utils/clipper/MultiCurvedClipper.dart';
import 'package:ecommerceapp/widgets/ProgressDialogType.dart';
import 'package:ecommerceapp/widgets/validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:ecommerceapp/utils/responsive/flutter_screenutil.dart' as Screen;
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/material.dart';
import 'package:ecommerceapp/utils/extensions/basic_extensions.dart';
import 'package:ecommerceapp/utils/responsive/size_extension.dart';
import 'package:ecommerceapp/widgets/TextFieldEye.dart';
import 'package:ecommerceapp/utils/extensions/context.dart';

class LoginView extends BaseStatefulWidget{
  static const routeName = '/login_view';
  @override
  _LoginViewState createState() =>_LoginViewState();

}


class _LoginViewState extends BaseState<LoginView> with BaseStatefulScreen{
  final emailController = TextEditingController();
  final passController = TextEditingController();

  _LoginViewState() : super(defaultPadding: false);

  ProgressDialog progressDialog;
  final LoginService loginService = LoginService();

  @override
  Widget buildBody(BuildContext context) {
    progressDialog = ProgressDialog(context);
    return SingleChildScrollView(
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
          color: AppTheme.primaryColorLight,

        child: Stack(
          children: <Widget>[
            Positioned(
              top: 0,
              right: 0,
              left: 0,
              child: Container(
                height: MediaQuery.of(context).size.height*0.85,
                decoration: BoxDecoration(
                  color: AppTheme.primaryColorDark,
                ),
              ).clip(MultiCurvedClipper()),
            ),
            Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                getLoginTopView().paddingTop(110.h),
                getTextField(context, emailController, "Email Address",
                )
                    .paddingE(EdgeInsets.only(left: 24.w, right: 24.w,top: 60.h)),
                SizedBox(
                  height: 24.h,
                ),
                getPassField(context, passController, "Password")
                    .paddingE(EdgeInsets.only(left: 24.w, right: 24.w)),
                SizedBox(
                  height: 41.h,
                ),
                loginButton("Login",(){
                  if(validate()){
                    loginUser();
                  }
                }).paddingE(EdgeInsets.only(bottom: 24.h)),
                loginButton("Register", (){
                  Navigator.of(context).pushNamed(SignUpScreen.routeName);
                }),
                Expanded(
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: new TextSpan(

                        // Note: Styles for TextSpans must be explicitly defined.
                        // Child text spans will inherit styles from parent
                        style:  TextStyle(
                            color:  AppTheme.primaryColorDark,
                            fontSize: 22.sp),
                        children: <TextSpan>[
                          new TextSpan(text: 'Have you forgotten your password? \n'),
                          new TextSpan(text: 'Click here to '),
                          new TextSpan(
                              text: 'reset password.',
                              recognizer: new TapGestureRecognizer()
                                ..onTap = () => {
                                  Navigator.of(context).pushNamed(ForgotPasswordScreen.routeName)

                                },
                              style: TextStyle(decoration: TextDecoration.underline)),

                        ],
                      ),
                    ).paddingAll(left: 24.0,right:24.0,bottom: 16.0),
                  ),
                ),
                /*Expanded(
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: InkWell(
                      onTap: () =>{
                        Navigator.of(context).pushNamed(ForgotPasswordScreen.routeName)
                      },
                      child: Text("Have you forgotten your password?",
                          style: TextStyle(
                              color:  AppTheme.primaryColorDark,
                              fontSize: 26.sp)),
                    ),
                  ).paddingBottom(16.0),
                )*/
                //textUnderButton(),
                //needHelp(),
               /* Expanded(
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: Column(
                        children: <Widget>[
                          signInWith(),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Image.asset(
                                "assets/images/facebook_circled.png",
                                width: 60.w,
                                height: 60.h,

                              ),
                              SizedBox(width: 12.w,),
                              Container(
                                width: 50.w,
                                height: 50.h,
                                padding: EdgeInsets.all(4.0),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    shape: BoxShape.circle
                                ),
                                child:Image.asset(
                                  "assets/images/google_logo.png",
                                  width: 30.w,
                                  height: 30.h,

                                ) ,
                              )
                            ],
                          )
                        ],
                      ),
                    )),*/

              ],
            ),
          ],
        ),
      ),
    ).noScrollEffect();
  }

  @override
  String screenName() =>null;


  Widget getLoginTopView() {
    return Column(
      children: <Widget>[
        Image.asset(
          "assets/images/app_icon.png",
          width: 240.w,
          height: 135.h,
          fit: BoxFit.fitWidth,
        ),
      ],
    );
  }

  Widget getTextField(context, controller, name) => Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    mainAxisSize: MainAxisSize.min,
    children: <Widget>[
      Text(name,
          style: TextStyle(
              fontSize: 22.sp,
              color: Colors.white,
              fontWeight: FontWeight.bold)),
      SizedBox(height: 8.h),
      SizedBox(
        height: 55.h,
        child: TextField(
          autofocus: false,
          controller: controller,
          cursorColor: Colors.white,
          textAlignVertical: TextAlignVertical.center,
          style: TextStyle(fontSize: 22.0.sp, color: Colors.black87,letterSpacing: 1.2),
          decoration: InputDecoration(
            filled: true,
            floatingLabelBehavior: FloatingLabelBehavior.never,
            fillColor: Color(0xFFEAEAEA),
            hintStyle: TextStyle(color: Colors.black87),

            contentPadding: EdgeInsets.only(left: 14.0.w, right: 14.w),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.white),
              borderRadius: BorderRadius.circular(8.0),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.white),
              borderRadius: BorderRadius.circular(8.0),
            ),
          ),
        ),
      )
    ],
  );

  Widget getPassField(context, TextEditingController controller, name) => Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    mainAxisSize: MainAxisSize.min,
    children: <Widget>[
      Text(name,
          style: TextStyle(
              fontSize: 22.sp,
              color: Colors.white,
              fontWeight: FontWeight.bold)),
      SizedBox(height: 8.h),
      PasswordField(controller,"").elevation(25.7, elevation: 8),
    ],
  );


  Widget loginButton(text,onTapped)=> SizedBox(
    width: MediaQuery.of(context).size.width*0.50,
    height: 60.h,
    child: RaisedButton(
      //splashColor: Colors.lightBlue,
      onPressed: () {
        onTapped();
        //Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);
      },
      color:AppTheme.primaryColorLight,
      child: Text(text,
          style: TextStyle(
              color: Colors.black,
              fontSize: 24.sp,
              fontWeight: FontWeight.bold)),
      //disabledColor: Colors.lightBlue,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24)),
    ),
  ).elevation(24.0, elevation: 8.0);





  Widget signInWith()=>Text("Sign In With",style: TextStyle(
      color: Colors.white,
      fontSize: 18.0.sp
  ),).paddingTop(48.h);


  bool validate() {
    final email = emailController.text;
    final pass = passController.text;

    if (!Validator.isPassword(pass, minLength: 8)) {
      context.showAlert("Password must be of minimum of 8 character.");
      return false;
    }

    if (!Validator.isEmail(email)) {
      context.showAlert("Please enter valid Email.");
      return false;
    }
    return true;
  }


  void loginUser() async {
    await progressDialog.show();
    final email = emailController.text;
    final pass = passController.text;
    loginService
        .loginUser(email, pass)
        .then((value) async {
      await progressDialog.hide();
      if (value != null && value.length>0 ) {
        // await SharedPreferenceUtil.saveUserId(value.data.userId.toString());
        await UserRepo.saveUser(value[0]);
        Navigator.of(context).pushReplacementNamed(MyHomePage.routeName);
      } else {
        context.showAlert("Email or Password is wrong.");
      }
    }).catchError((e) async {
      await progressDialog.hide();
      context.showAlert(e?.message ?? "Sorry, Something went wrong");
    });
  }




}