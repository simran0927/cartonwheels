import 'package:dio/dio.dart';
import 'package:ecommerceapp/commons/constants/app_constants.dart';
import 'package:ecommerceapp/model/LoginData.dart';
import 'package:ecommerceapp/model/common_response.dart';
import 'package:ecommerceapp/network/dio_rest_client.dart';

class LoginService{
  Future<List<LoginData>> loginUser( email, password) async {
    ApiResponse apiResponse =
    await ApiHitter.getPostApiResponse(AppConstants.LOGIN, formData: FormData.fromMap({
      "email": email,
      "password": password,
    }));
    try {
      return apiResponse.response != null
          ?loginDataFromJson(apiResponse.response.data)
          : null;
    } catch (e) {
      print(e..toString());
      return null;
    }

  }
}