import 'dart:typed_data';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:ecommerceapp/app/app_theme.dart';
import 'package:ecommerceapp/base/base_state.dart';
import 'package:ecommerceapp/base/base_stateful_widget.dart';
import 'package:ecommerceapp/base/base_statefull_screen.dart';
import 'package:ecommerceapp/commons/constants/app_constants.dart';
import 'package:ecommerceapp/commons/repo/UserRepository.dart';
import 'package:ecommerceapp/model/ProductData.dart';
import 'package:ecommerceapp/screens/products/service/ProductsService.dart';
import 'package:ecommerceapp/utils/app_utils.dart';
import 'package:ecommerceapp/utils/responsive/flutter_screenutil.dart';
import 'package:ecommerceapp/widgets/ConfirmDialogWidget.dart';
import 'package:ecommerceapp/widgets/ProgressDialogType.dart' as P;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:ecommerceapp/utils/extensions/basic_extensions.dart';
import 'package:ecommerceapp/utils/extensions/context.dart';
import 'package:ecommerceapp/utils/responsive/size_extension.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ProductDetailScreen extends BaseStatefulWidget {
  static const routeName = '/product_details';
  ProductData productData;
  @override
  _ProductDetailScreenState createState() => _ProductDetailScreenState();
}

class _ProductDetailScreenState extends BaseState<ProductDetailScreen>
    with BaseStatefulScreen {

  _ProductDetailScreenState():super(defaultPadding:false);
  @override
  String screenName() => "Product details";

  bool descTextShowFlag = true;
  int number =1;

  P.ProgressDialog progressDialog;
  ProductsService productsService;


@override
  void initState() {
  super.initState();
  Future.delayed(Duration.zero,(){
    setState(() {
      widget.productData = ModalRoute.of(context).settings.arguments as ProductData;

    });
  });

  progressDialog = P.ProgressDialog(context,
      type: P.ProgressDialogType.Normal, isDismissible: false, showLogs: true);
  productsService = ProductsService();
  }


  void add() {
    setState(() {
      number++;
    });
  }

  void minus() {
    setState(() {
      if (number != 1) number--;
    });
  }

  @override
  Widget buildBody(BuildContext context) {
    ScreenUtil.init(context);

    return widget.productData!=null?Stack(
      children: <Widget>[
        Container(
          constraints: BoxConstraints(minHeight: MediaQuery.of(context).size.height),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Align(
                  alignment: Alignment.center,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: 320.h,
                    color: Colors.white,
                    child: CachedNetworkImage(
                      imageUrl:AppConstants.IMAGEURL+widget.productData.photo,
                    ).paddingAll(top: 16.h, bottom: 16.h),
                  )
                ),

                SizedBox(
                  height: 10,
                ),
                Container(
                    padding:
                        EdgeInsets.only(left: 15, right: 15, top: 20, bottom: 10),
                    child: Text(
                      widget.productData.proName,
                      style: TextStyle(
                          color: AppTheme.primaryColorDark,
                          fontSize: 28.0.sp,
                          fontWeight: FontWeight.w600),
                    )),
                RichText(
                  text: TextSpan(
                    style: TextStyle(color: Colors.black, fontSize: 22.sp),
                    children: <TextSpan>[
                      widget.productData.proAfterprice!=null ? TextSpan(
                          text: "\$ "+ widget.productData.proAfterprice,
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 32.sp)):SizedBox(),
                      widget.productData.proAfterprice!=null ?TextSpan(text: '   (\$ '+widget.productData.proPrice+" )",style: TextStyle(
                          decoration: TextDecoration.lineThrough,
                          fontSize: 22.sp,fontWeight: FontWeight.w800,
                          color: Colors.green
                      ),):
                      TextSpan(text: '   \$ '+widget.productData.proPrice+" ",style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 32.sp))
                    ],
                  ),
                ).paddingAll(left: 15, right: 15, bottom: 15),
                Divider(
                  thickness: 1.0,
                  color: Colors.grey,
                ),
                Container(
                  alignment: Alignment.topLeft,
                  width: double.infinity,
                  padding: EdgeInsets.only(left: 15, right: 15, top: 10, bottom: 20),
                  // color: Color(0xFFFFFFFF),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text("Product details",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w700,
                              color: Color(0xFF565656))),
                      SizedBox(
                        height: 15,
                      ),
                    /* descTextShowFlag?SizedBox(
                         height: 65,
                         child: Html(data: widget.productData.proDescription,)):*/
                     SizedBox(
                         child: Html(data: widget.productData.proDescription,)),
                      SizedBox(height: 30,),
                     /* InkWell(
                        onTap: (){
                          setState(() {
                            descTextShowFlag = !descTextShowFlag;
                          });
                        },
                        child: descTextShowFlag? Text("SEE MORE",style: TextStyle(
                          color: Colors.green,
                          fontWeight: FontWeight.w600
                        ),): Text("SEE LESS",style: TextStyle(
                            color: Colors.green,
                            fontWeight: FontWeight.w600
                        ),),
                      )*/
                    ],
                  ),
                ),
                Divider(
                  thickness: 1,
                  color: Colors.grey,
                ),
                /*Container(
                  alignment: Alignment.topCenter,
                  margin: EdgeInsets.only(top:16.0,left: 24,right: 24,bottom: 16),
                  padding: EdgeInsets.only(left: 15, right: 15, top: 20, bottom: 20),
                 decoration: BoxDecoration(
                   border: Border.all(),
                   borderRadius: BorderRadius.all(Radius.circular(8.0))

                 ),
                  child: Text("Grown in Australia",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 24.0,
                    fontWeight: FontWeight.w700
                  ),)
                ),*/
                Container(
                  color: Colors.grey,
                  padding: EdgeInsets.only(left: 15, right: 15, top: 20, bottom: 10),
                    child:RichText(
                      text: TextSpan(
                        style: TextStyle(color: Colors.black, fontSize: 22.sp),
                        children: <TextSpan>[
                          TextSpan(
                              text: "Disclaimer: ",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 22.sp)),
                          TextSpan(text: 'Photos are for illustration purposes only, actual products may vary due to ongoing upgrade.')
                        ],
                      ),
                    ).paddingAll(left: 15, right: 15, bottom: 15)
                ),
                SizedBox(height: 60.0,)
              ],
            ),
          ),
        ),
        Positioned(
          bottom: 0,
          left: 0,
          right: 0,
          child:Container(
            padding: EdgeInsets.all(12.0),
            color: AppTheme.primaryColorLight,
           child: widget.productData.isStockAvailable() ? Row(
             mainAxisAlignment: MainAxisAlignment.spaceBetween,
             children: <Widget>[
               Row(
                 children: <Widget>[
                   Row(
                     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                     children: <Widget>[
                       new InkWell(
                         onTap: minus,
                         child:  Container(
               padding: EdgeInsets.all(5.0),
            decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                color: Colors.amber,
                borderRadius: BorderRadius.all(Radius.circular(8.0))
            ),
            child: Icon(Icons.remove, color: Colors.white,size: 18,),
          )
                       ),
                       SizedBox(width: 6.0,),
                       new Text('$number',
                           style: new TextStyle(fontSize: 18.0)),
                       SizedBox(width:6.0,),
                       new InkWell(
                           onTap: add,
                           child:  Container(
                             padding: EdgeInsets.all(5.0),
                             decoration: BoxDecoration(
                                 shape: BoxShape.rectangle,
                                 color: Colors.amber,
                                 borderRadius: BorderRadius.all(Radius.circular(8.0))
                             ),
                             child: Icon(Icons.add, color: Colors.white,size: 18,),
                           )
                       ),

                     ],
                   ),
                   SizedBox(width: 12.0,),
                   addToCart('ADD TO CART', () async{
                    /* final shouldAdd = await showDialog(
                         context: context,
                         builder: (context) {
                           return ConfirmDialogWidget(
                               "Are you sure you want to add the product to the cart?");
                         });
                     if (shouldAdd != null && shouldAdd)*/
                   addItemToCart();
                   },Colors.green,0.29),
                   SizedBox(width: 8.0,),
                   addToCart('ADD TO WISHLIST', () async{
                    /* final shouldAdd = await showDialog(
                         context: context,
                         builder: (context) {
                           return ConfirmDialogWidget(
                               "Are you sure you want to add the product to the wishlist?");
                         });
                     if (shouldAdd != null && shouldAdd)*/
                     addItemToWishlist();
                   },Colors.amber,0.34),
                 ],
               ),

             ],
           ): Center(child: Text("Out of stock", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 18),)),
          )
        )
      ],
    ):Center(child:Text("No detail found."));
  }


  Widget addToCart(text,onTapped,color,widthpercent)=> SizedBox(
    width: MediaQuery.of(context).size.width*widthpercent,
    height: 40.h,
    child: RaisedButton(
      //splashColor: Colors.lightBlue,
      onPressed: () {
        onTapped();
        //Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);
      },
      color:color,
      child: FittedBox(
        child: Text(text,
            style: TextStyle(
                color: Colors.white,
                fontSize:  16.sp,
                fontWeight: FontWeight.bold)),
      ),
      //disabledColor: Colors.lightBlue,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8)),
    ),
  ).elevation(24.0, elevation: 8.0);

  /*@override
  List<Widget> setAppbarWidgets() {
    return <Widget>[
      IconButton(
        icon: Icon(Icons.shopping_cart),
        onPressed: () {},
      )
    ];
  }*/

  void addItemToCart() async {
    await progressDialog.show();
    productsService
        .addItemToCart(widget.productData.proId, UserRepo.user.usersId,
        quantity: number)
        .then((value) {})
        .whenComplete(() async {
      await progressDialog.hide();
      context.showInfoAlert("Product added to cart");
    });
  }

  void addItemToWishlist() async {
    await progressDialog.show();
    productsService
        .addToWishlist(widget.productData.proId, UserRepo.user.usersId)
        .then((value) {})
        .whenComplete(() async {
      await progressDialog.hide();
      context.showInfoAlert("Product added to wishlist");
      /*Fluttertoast.showToast(
          msg: "Product added to wishlist",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          textColor: Colors.white,
          fontSize: 14.0);*/
    });
  }

}
