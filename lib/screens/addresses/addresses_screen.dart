import 'package:ecommerceapp/app/app_theme.dart';
import 'package:ecommerceapp/commons/repo/UserRepository.dart';
import 'package:flutter/material.dart';
import 'package:ecommerceapp/model/address_model.dart';
import 'package:ecommerceapp/screens/cart/service/CartService.dart';
import 'package:ecommerceapp/utils/extensions/basic_extensions.dart';
import 'package:ecommerceapp/utils/responsive/flutter_screenutil.dart';
import 'package:ecommerceapp/utils/responsive/size_extension.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:place_picker/entities/location_result.dart';
import 'package:place_picker/place_picker.dart';

class AddressesScreen extends StatefulWidget {
  @override
  _AddressesScreenState createState() => _AddressesScreenState();
}

class _AddressesScreenState extends State<AddressesScreen> {
  CartService cartService;
  List<AddressModel> addressList;

  @override
  void initState() {
    super.initState();
    cartService = CartService();
    getAddresses();
    //cartService.getAddress(cartId, quantity)
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
        centerTitle: true,
        elevation: 3,
        leading: IconButton(
        icon: Icon(
        Icons.keyboard_arrow_left,
        color: Colors.white,
        size: 32,
    ),
    onPressed: () {
    Navigator.of(context).pop();
    },
    ),
    backgroundColor: AppTheme.primaryColorDark,
    title: Text(
    "Addresses",
    )),
    backgroundColor: Colors.white,
    body: SingleChildScrollView(
      child: Container(
        constraints: BoxConstraints(
            minHeight: MediaQuery.of(context).size.height),
        margin: EdgeInsets.only(top: 22, bottom: 22),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 35.h,
              width: MediaQuery.of(context).size.width,
              child: RaisedButton(
                color: AppTheme.buttonColorLight,
                child: Text("Choose address to add",
                    style: TextStyle(color: AppTheme.primaryColorDark)),
                onPressed: () async {
                  LocationResult result =
                  await Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => PlacePicker(
                        "AIzaSyDLDmvcZpQKHnbkEg76bk9jMd4QB_kZw4c",
                        displayLocation:
                        LatLng(-33.865143, 151.209900),
                      )));

                  // Handle the result in your way
                  if (result != null) {
                    if(result.country.shortName == "AU") {
                      saveAddress(result.formattedAddress);
                    } else {
                      Fluttertoast.showToast(
                          msg: "Address must be in australia.",
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.BOTTOM,
                          timeInSecForIos: 1,
                          textColor: Colors.white,
                          fontSize: 14.0);
                    }
                  }
                },
              ),
            ).paddingAll(left: 16, right: 16),
            addressList != null && addressList.isNotEmpty
                ? Text(
              "Addresses :",
              style: TextStyle(
                  fontSize: 18, color: AppTheme.primaryColorDark),
            ).paddingAll(top: 16, bottom: 16, left: 16)
                : SizedBox(),
            addressList != null && addressList.isNotEmpty
                ? ListView.builder(
                shrinkWrap: true,
                itemCount: addressList.length,
                itemBuilder: (context, pos) {
                  return addressContainer(addressList[pos]);
                })
                : SizedBox(
                height: MediaQuery.of(context).size.height * 0.55,
                child: Center(child: Text("No address added."))),
          ],
        ),
      ),
    ));
  }

  addressContainer(AddressModel address) {
    return Card(
        color: AppTheme.primaryColorDark,
        elevation: 4,
        shadowColor: AppTheme.primaryColorDark,
        shape:
        RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
        margin: EdgeInsets.only(left: 16, right: 16, top: 13),
        child: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: Padding(
            padding: EdgeInsets.fromLTRB(16, 12, 16, 12),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  child: Text(
                    address?.del_address ?? "",
                    style: TextStyle(color: Colors.white, fontSize: 16),
                  ),
                ),
                InkWell(
                    onTap: () {
                      deleteAddress(address);
                    },
                    child: Icon(
                      Icons.delete,
                      color: Colors.red,
                    ))
              ],
            ),
          ),
        ));
  }

  void deleteAddress(AddressModel address) {
    cartService
        .deleteAddress(UserRepo.user.usersId, address.del_adr_id)
        .then((value) {})
        .catchError((e) {});
    setState(() {
      addressList.remove(address);
    });
  }

  void getAddresses() async {
    cartService.getAddress(UserRepo.user.usersId).then((value) {
      setState(() {
        addressList = value;
      });
    }).catchError((e) {});
  }

  void saveAddress(addressString) async {
    await cartService.saveAddress(UserRepo.user.usersId, addressString);
    getAddresses();
  }
}
