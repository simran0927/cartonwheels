import 'package:ecommerceapp/app/app_theme.dart';
import 'package:ecommerceapp/constants/values.dart';
import 'package:ecommerceapp/model/CartResult.dart';
import 'package:ecommerceapp/screens/cart/cart_bottom_sheet.dart';
import 'package:ecommerceapp/screens/wishlist/wishlist_page.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

Widget appBarWidget(context, List<CartResult> cartItems) {
  return AppBar(
    elevation: 0.0,
    centerTitle: true,
    backgroundColor: AppTheme.primaryColorDark,
    title: Image.asset(
      "assets/images/app_icon.png",
      width: 80,
      height: 40,
    ),
    actions: <Widget>[
      Stack(
        children: [
          IconButton(
            iconSize: 18,
            onPressed: () {
              showModalBottomSheet(
                shape: roundedRectangle40,
                context: context,
                builder: (context) => CartBottomSheet(),
              );
            },
            icon: Icon(FontAwesomeIcons.shoppingCart),
            color: Colors.white,
          ),
          cartItems != null && cartItems.length > 0 ? Positioned(
            right: 4,
            top: 10,
            child: Container(
              height: 17,
              width: 17,
              decoration:
                  BoxDecoration(color: Colors.red, shape: BoxShape.circle),
              child: Center(
                child: Text(
                  cartItems.length.toString(),

                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 10,
                      color: Colors.white)
                ),
              ),
            ),
          ): SizedBox(),
        ],
      ),
      IconButton(
        iconSize: 18,
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => WishlistPage()));
        },
        icon: Icon(FontAwesomeIcons.solidHeart),
        color: Colors.white,
      ),
    ],
  );
}
