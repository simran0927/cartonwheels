import 'package:ecommerceapp/app/app_theme.dart';
import 'package:ecommerceapp/commons/repo/UserRepository.dart';
import 'package:ecommerceapp/utils/extensions/base64.dart';
import 'package:ecommerceapp/screens/account/Account_Screen.dart';
import 'package:ecommerceapp/screens/addresses/addresses_screen.dart';
import 'package:ecommerceapp/screens/login/login.dart';
import 'package:ecommerceapp/screens/more/ContactUs.dart';
import 'package:ecommerceapp/screens/orders/Allorders.dart';
import 'package:ecommerceapp/screens/wishlist/wishlist_page.dart';
import 'package:flutter/material.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:share/share.dart';

class DrawerWidget extends StatefulWidget {
  @override
  _DrawerWidgetState createState() => _DrawerWidgetState();
}

class _DrawerWidgetState extends State<DrawerWidget> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width * 0.65,
      child: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            _createDrawerHeader(context),
            _createDrawerItem(
                icon: Icons.home,
                text: 'Home',
                onTap: () => Navigator.pop(context)),
       /*     _createDrawerItem(
                icon: FontAwesomeIcons.user,
                text: 'Sign In',
             *//*   onTap: () => Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => AppSignIn()),
                    )*//*),*/
          _createDrawerItem(
              icon: Icons.person,
              text: 'My Profile',
              onTap: () {
                Navigator.pop(context);
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => ProfilePage()));
              }),

            _createDrawerItem(
                icon: Icons.location_searching,
                text: 'Addresses',
                onTap: () {
                  Navigator.pop(context);
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => AddressesScreen()));
                }),
            _createDrawerItem(
                icon: Icons.history,
                text: 'Orders',
                onTap: () {
                  Navigator.pop(context);
                  Navigator.of(context).pushNamed(AllOrders.routeName);
                }),
            _createDrawerItem(
                icon: Icons.favorite_border,
                text: 'Wish List',
                onTap: () {
                  Navigator.pop(context);
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => WishlistPage()));
                }) ,_createDrawerItem(
                icon: Icons.share,
                text: 'Refer friend',
                onTap: () {
                  Navigator.pop(context);
                  Share.share("${UserRepo.user.name} has invited you to Cart On Wheels, online Indian Grocery store.\nSignUp with the refer code \"${UserRepo.user.usersId.encodeBase64()}\", on Android or iOS app and get \$10 in your wallet straight away.\nDownload Android app-\nhttps://play.google.com/store/apps/details?id=com.caronwheels&hl=en&gl=AU\nDownload iOS app-\nhttps://apps.apple.com/us/app/cart-on-wheels/id1542195083");
                }),
            /*_createDrawerItem(
                icon: Icons.contacts,
                text: 'Contact Us',
                onTap: () => Navigator.of(context).pushNamed(ContactUs.routeName)),*/

            _createDrawerItem(
              icon: Icons.exit_to_app,
              text: 'LogOut',
              onTap: ()async {
                await UserRepo.clearPreference();
                Navigator.pop(context);
                Navigator.of(context).pushReplacementNamed(LoginView.routeName);
              },
                    ),
          ],
        ),
      ),
    );
  }
}

Widget _createDrawerHeader(context) {
  return MediaQuery.removeViewPadding(
    context: context,
    removeTop: true,
    child: DrawerHeader(
        padding: EdgeInsets.all(0),
        child: ListView(
          children: <Widget>[
            Stack(children: <Widget>[
              Container(
                color: AppTheme.primaryColorDark,
                padding: EdgeInsets.all(20),
                child: Center(
                  child: Image.asset(
                    'assets/images/app_icon.png',
                    width: 130,
                    height: 130,
                  ),
                ),
              ),

            ]),
          ],
        )),
  );
}

Widget _createDrawerItem(
    {IconData icon, String text, GestureTapCallback onTap}) {
  return ListTile(
    title: Row(
      children: <Widget>[
        Icon(
          icon,
          color: Color(0xFF808080),
        ),
        Padding(
          padding: EdgeInsets.only(left: 15.0),
          child: Text(
            text,
            style: TextStyle(color: Color(0xFF484848)),
          ),
        )
      ],
    ),
    onTap: onTap,
  );
}
