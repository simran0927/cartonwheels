import 'package:ecommerceapp/base/base_state.dart';
import 'package:ecommerceapp/base/base_stateful_widget.dart';
import 'package:ecommerceapp/base/base_statefull_screen.dart';
import 'package:ecommerceapp/screens/cart/provider/UpdateCartProvider.dart';
import 'package:ecommerceapp/screens/home/HomeScreen.dart';
import 'package:ecommerceapp/screens/list/UserList.dart';
import 'package:ecommerceapp/screens/mainHome/widgets/AppBarWidget.dart';
import 'package:ecommerceapp/screens/mainHome/widgets/BottomNavBarWidget.dart';
import 'package:ecommerceapp/screens/mainHome/widgets/DrawerWidget.dart';
import 'package:ecommerceapp/screens/more/MoreScreen.dart';
import 'package:ecommerceapp/screens/wallet/WalletScreen.dart';
import 'package:ecommerceapp/utils/responsive/flutter_screenutil.dart';
import 'package:flutter/material.dart';
import 'package:focus_detector/focus_detector.dart';
import 'package:provider/provider.dart';

class MyHomePage extends BaseStatefulWidget {
  static const routeName = '/my_home_page';

  @override
  _MyHomePageNewState createState() => _MyHomePageNewState();
}

class _MyHomePageNewState extends BaseState<MyHomePage>
    with BaseStatefulScreen {
  _MyHomePageNewState() : super(defaultPadding: false);

  final List<Widget> viewContainer = [
    HomeScreen(),
    UserListScreen(),
    WalletScreen(),
    MoreScreen(),
  ];

  int currentIndex = 0;

  @override
  String screenName() => null;

  UpdateCartProvider updateCartProvider;
  final _resumeDetectorKey = UniqueKey();

  @override
  Widget buildBody(BuildContext context) {
    ScreenUtil.init(context);
    updateCartProvider = Provider.of<UpdateCartProvider>(context);
    return FocusDetector(
      key: _resumeDetectorKey,
      onFocusGained: () {
        updateCartProvider.fetchCartItems();
      },
      child: Scaffold(
        appBar: currentIndex != 3
            ? appBarWidget(context,
                updateCartProvider == null ? [] : updateCartProvider.items)
            : null,
        drawer: currentIndex != 3 ? DrawerWidget() : null,
        body: IndexedStack(
          index: currentIndex,
          children: viewContainer,
        ),
        bottomNavigationBar: BottomNavBarWidget((index) {
          setState(() {
            currentIndex = index;
          });
        }),
      ),
    );
  }
}
