

import 'package:dio/dio.dart';
import 'package:ecommerceapp/commons/constants/app_constants.dart';
import 'package:ecommerceapp/model/CartResult.dart';
import 'package:ecommerceapp/model/WishlistData.dart';
import 'package:ecommerceapp/network/dio_rest_client.dart';

class  WishlistService{

  Future<List<WishlistData>> getAllWishlistItems(userId) async {
    ApiResponse apiResponse =
    await ApiHitter.getPostApiResponse(AppConstants.WISHLIST, formData: FormData.fromMap({
      "user_id": userId
    }));
    try {
      return apiResponse.response != null
          ?wishlistDataFromJson(apiResponse.response.data)
          : null;
    } catch (e) {
      print(e..toString());
      return null;
    }
  }

  Future<bool> removeFromWishlist(wishId) async {
    ApiResponse apiResponse =
    await ApiHitter.getPostApiResponse(AppConstants.WISH_REMOVE, formData: FormData.fromMap({
      "wishid": wishId
    }));
    try {
      return apiResponse.response != null
          ? true
          : false;
    } catch (e) {
      print(e..toString());
      return false;
    }
  }
}