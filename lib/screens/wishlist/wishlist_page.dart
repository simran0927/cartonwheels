import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:ecommerceapp/app/app_theme.dart';
import 'package:ecommerceapp/base/base_state.dart';
import 'package:ecommerceapp/base/base_stateful_widget.dart';
import 'package:ecommerceapp/base/base_statefull_screen.dart';
import 'package:ecommerceapp/commons/constants/app_constants.dart';
import 'package:ecommerceapp/commons/repo/UserRepository.dart';
import 'package:ecommerceapp/constants/values.dart';
import 'package:ecommerceapp/model/WishlistData.dart';
import 'package:ecommerceapp/screens/cart/cart_bottom_sheet.dart';
import 'package:ecommerceapp/screens/cart/provider/UpdateCartProvider.dart';
import 'package:ecommerceapp/screens/list/service/ListService.dart';
import 'package:ecommerceapp/screens/wishlist/service/WishlistService.dart';
import 'package:ecommerceapp/utils/extensions/basic_extensions.dart';
import 'package:ecommerceapp/utils/responsive/size_extension.dart';
import 'package:ecommerceapp/widgets/ProgressDialogType.dart' as P;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class WishlistPage extends BaseStatefulWidget {
  _WishlistPageState createState() => _WishlistPageState();
}

class _WishlistPageState extends BaseState<WishlistPage>
    with BaseStatefulScreen {
  var now = DateTime.now();

  get weekDay => DateFormat('EEEE').format(now);

  get day => DateFormat('dd').format(now);

  get month => DateFormat('MMMM').format(now);

  ScrollController scrollController = ScrollController();
  WishlistService wishlistService;
  List<WishlistData> wishListData;
  P.ProgressDialog progressDialog;

  @override
  void initState() {
    super.initState();
    progressDialog = P.ProgressDialog(context,
        type: P.ProgressDialogType.Normal,
        isDismissible: false,
        showLogs: true);
    wishlistService = WishlistService();
    getAllWishListItems();
  }

  @override
  String screenName() => 'Wishlist';

  void getAllWishListItems() async {
    await progressDialog.show();
    wishlistService.getAllWishlistItems(UserRepo.user.usersId).then((value) {
      if (value != null && value.length > 0) {
        setState(() {
          value.forEach((element) {
            element.isLiked = true;
          });
          wishListData = value;
        });
      }
    }).whenComplete(() async {
      await progressDialog.hide();
    });
  }

  void removeFromWishlist(wishId) async {
    wishlistService.removeFromWishlist(wishId).then((value) {});
  }

  List<Widget> buildHeader() {
    return [
      Padding(
        padding: const EdgeInsets.symmetric(vertical: 32.0, horizontal: 0),
        child: Text('$weekDay, ${day}th of $month ', style: headerStyle),
      ),
    ];
  }

  @override
  Widget buildBody(BuildContext context) {
    /*var cart = Provider.of<MyCart>(context);
    cart.items.forEach((element) {
      element.isLiked = true;
    });*/

    return SingleChildScrollView(
      child: Container(
        constraints:
            BoxConstraints(minHeight: MediaQuery.of(context).size.height),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            ...buildHeader(),
            //cart items list
            wishListData != null && wishListData.length > 0
                ? ListView.builder(
                    itemCount: wishListData.length,
                    shrinkWrap: true,
                    controller: scrollController,
                    itemBuilder: (BuildContext context, int index) {
                      return WishListItem(wishListData[index],
                          (cartItem) async {
                        removeFromWishlist(cartItem.wishId);
                        Future.delayed(Duration(milliseconds: 220), () {
                          setState(() {
                            wishListData.remove(cartItem);
                            //  cart.removeAllInCart(cartItem.food);
                          });
                        });
                      },updateCartProvider);
                    },
                  )
                : Center(
                    child: Text("No Items in Wishlist."),
                  ),
            SizedBox(height: 16),
            Divider(),
          ],
        ),
      ),
    );
  }

  UpdateCartProvider updateCartProvider;

  @override
  List<Widget> setAppbarWidgets() {
    updateCartProvider = Provider.of<UpdateCartProvider>(context);
    return [
      updateCartProvider.items != null && updateCartProvider.items.length > 0
          ? Padding(
            padding: const EdgeInsets.only(right:8.0, top: 4.0),
            child: Stack(
              children: [
                IconButton(
                  iconSize: 18,
                  onPressed: () {
                    showModalBottomSheet(
                      shape: roundedRectangle40,
                      context: context,
                      builder: (context) => CartBottomSheet(),
                    );
                  },
                  icon: Icon(FontAwesomeIcons.shoppingCart),
                  color: Colors.white,
                ),
                Positioned(
                    right: 4,
                    top: 10,
                    child: Container(
                      height: 17,
                      width: 17,
                      decoration:
                          BoxDecoration(color: Colors.red, shape: BoxShape.circle),
                      child: Center(
                        child: Text(updateCartProvider.items.length.toString(),
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 10,
                                color: Colors.white)),
                      ),
                    ),
                  ),
              ],
            ),
          )
          : SizedBox()
    ];
  }
}

class WishListItem extends StatefulWidget {
  final WishlistData cartModel;
  final Function(WishlistData) removeCallback;
  UpdateCartProvider updateCartProvider;
  WishListItem(this.cartModel, this.removeCallback,this.updateCartProvider);

  @override
  _WishListItemState createState() => _WishListItemState();
}

class _WishListItemState extends State<WishListItem> {
  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.only(bottom: 8),
      child: Container(
        height: 120,
        padding: EdgeInsets.all(16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(6)),
              child: CachedNetworkImage(
                imageUrl: '${AppConstants.IMAGEURL}${widget.cartModel.photo}',
                width: 100,
                height: 100,
              ),
            ),
            Flexible(
              flex: 3,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    child: Text(
                      widget.cartModel.proName,
                      style: titleStyle,
                      textAlign: TextAlign.start,
                    ),
                  ),
                  Spacer(),
                  widget.cartModel.isStockAvailable()
                      ? addToCart("ADD TO CART", () {
                          addItemToCart(widget.cartModel);
                        })
                      : Text(
                          "Out of stock",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 16,
                              fontWeight: FontWeight.bold),
                        )
                  /*Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Expanded(
                      child: Container(
                        child: Text(
                          widget.cartModel.proDescription,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 3,
                          style: infoStyle,
                          textAlign: TextAlign.start,
                        ),
                      ),
                    ),
                  ),*/
                ],
              ),
            ),
            Flexible(
              flex: 1,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Container(
                    height: 45,
                    width: 70,
                    child: Text(
                      '\$ ${widget.cartModel.proPrice}',
                      style: titleStyle,
                      textAlign: TextAlign.end,
                    ),
                  ),
                  Spacer(),
                  AnimatedContainer(
                    duration: Duration(milliseconds: 200),
                    child: InkWell(
                      onTap: () {
                        setState(() {
                          widget.cartModel.isLiked = false;
                        });
                        widget.removeCallback(widget.cartModel);
                      },
                      child: Icon(
                        Icons.delete,
                        color: Colors.red,
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget addToCart(text, onTapped) => SizedBox(
        width: 130,
        height: 32.h,
        child: RaisedButton(
          //splashColor: Colors.lightBlue,
          onPressed: () {
            onTapped();
            //Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);
          },
          color: Colors.green,
          child: FittedBox(
            child: Text(text,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20.sp,
                    fontWeight: FontWeight.bold)),
          ),
          //disabledColor: Colors.lightBlue,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(24)),
        ),
      ).elevation(24.0, elevation: 3.0);

  void addItemToCart(WishlistData data) async {
    //await progressDialog.show();
    widget.removeCallback(data);
    ListService()
        .addItemToCart(data.proId, UserRepo.user.usersId)
        .then((value) {})
        .whenComplete(() async {
      showDialog(
          context: context,
          builder: (context) {
            Future.delayed(Duration(seconds: 2), () {
              widget.updateCartProvider.fetchCartItems();
              Navigator.of(context).pop(true);
            });
            return AlertDialog(
              title: Center(
                  child: Text(
                'Product added to cart.',
                style: TextStyle(color: AppTheme.primaryColorDark),
              )),
            );
          });
    });
  }
}
