
import 'dart:async';

import 'package:ecommerceapp/app/app_theme.dart';
import 'package:ecommerceapp/base/base_state.dart';
import 'package:ecommerceapp/base/base_stateful_widget.dart';
import 'package:ecommerceapp/base/base_statefull_screen.dart';
import 'package:ecommerceapp/commons/repo/UserRepository.dart';
import 'package:ecommerceapp/screens/login/login.dart';
import 'package:ecommerceapp/screens/mainHome/MyHomePage.dart';
import 'package:ecommerceapp/utils/clipper/QuadClipper.dart';
import 'package:ecommerceapp/utils/clipper/TopCurvedClipper.dart';
import 'package:ecommerceapp/utils/responsive/flutter_screenutil.dart';
import 'package:ecommerceapp/utils/responsive/size_extension.dart';
import 'package:ecommerceapp/utils/extensions/basic_extensions.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';

class SplashScreen extends BaseStatefulWidget{
  @override
  _SplashScreenState createState() =>_SplashScreenState();
}

class _SplashScreenState extends BaseState<SplashScreen> with BaseStatefulScreen{
  _SplashScreenState() : super(defaultPadding: false);
  @override
  String screenName() => null;


  @override
  void initState() {
    super.initState();
    startTime();
  }


  startTime() async {
    var _duration = new Duration(seconds: 2);
    return new Timer(_duration, navigationPage);
  }

  void navigationPage() {
    print('Timer involked');
    UserRepo.getUserDetail().then((value)  {
      if(value != null) {
        Navigator.of(context).pushReplacementNamed(MyHomePage.routeName);
      } else
      Navigator.of(context).pushReplacementNamed(LoginView.routeName);
    });

  }


  @override
  Widget buildBody(BuildContext context) {
    ScreenUtil.init(context);
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      color: AppTheme.primaryColorLight,
      child: Stack(
        children: <Widget>[
          Positioned(
              top: 100,
              bottom: 80,
              right: 24,
              left: 24,
              child: Container(

                height: MediaQuery.of(context).size.height*0.70,
                decoration: BoxDecoration(
                    color: AppTheme.primaryColorDark
                ),
              ).clip(QuadClipper()),
            ),
          Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                setSplashImage(),
                SizedBox(
                  height: 20.h,
                ),
               // splashText(),

              ],
            ),
          ),
        ],
      ),
    );
  }


  Widget setSplashImage() {
    return Image.asset(
      "assets/images/app_icon.png",
      width: 300.w,
      height: 180.h,
      fit: BoxFit.fitWidth,

    );
  }

  Widget splashText() {
    return Text(
      "ECommerce App",
      style: TextStyle(
          color: Colors.white,
          fontSize: 42.0.sp
      ),
    );
  }


}