import 'package:dio/dio.dart';
import 'package:ecommerceapp/commons/constants/app_constants.dart';
import 'package:ecommerceapp/model/LoginData.dart';
import 'package:ecommerceapp/model/common_response.dart';
import 'package:ecommerceapp/network/dio_rest_client.dart';

class ForgetPassService{
  Future<String> forgetPass( email) async {
    ApiResponse apiResponse =
    await ApiHitter.getPostApiResponse(AppConstants.FORGET_PASS, formData: FormData.fromMap({
      "email": email,

    }));
    try {
      return apiResponse.response != null
          ?apiResponse.response.data
          : "Sorry Something went wrong.";
    } catch (e) {
      print(e..toString());
      return null;
    }

  }
}