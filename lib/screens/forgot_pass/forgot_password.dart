import 'package:ecommerceapp/app/app_theme.dart';
import 'package:ecommerceapp/base/base_state.dart';
import 'package:ecommerceapp/base/base_stateful_widget.dart';
import 'package:ecommerceapp/base/base_statefull_screen.dart';
import 'package:ecommerceapp/screens/forgot_pass/service/ForgetPassService.dart';
import 'package:ecommerceapp/utils/clipper/MultiCurvedClipper.dart';
import 'package:ecommerceapp/widgets/ProgressDialogType.dart';
import 'package:ecommerceapp/widgets/validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:ecommerceapp/utils/extensions/basic_extensions.dart';
import 'package:ecommerceapp/utils/extensions/context.dart';
import 'package:ecommerceapp/utils/responsive/size_extension.dart';

class ForgotPasswordScreen extends BaseStatefulWidget {
  static const routeName = '/forgot_pass';

  @override
  _ForgotPasswordScreenState createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends BaseState<ForgotPasswordScreen>
    with BaseStatefulScreen {
  TextEditingController emailController = TextEditingController();

  _ForgotPasswordScreenState() : super(defaultPadding: false);

  ProgressDialog progressDialog;
   ForgetPassService forgetPassService;

  @override
  String screenName() => null;

  @override
  void initState() {
    progressDialog = ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: true);
    forgetPassService = ForgetPassService();
    super.initState();
  }

  @override
  Widget buildBody(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        color: AppTheme.primaryColorDark,
        child: Stack(
          children: <Widget>[
            Positioned(
              top: 0,
              right: 0,
              left: 0,
              child: Container(
                height: MediaQuery.of(context).size.height * 0.40,
                decoration: BoxDecoration(
                  color: AppTheme.primaryColorLight,
                  //color: Color(0xff78380C),
                ),
              ).clip(MultiCurvedClipper()),
            ),
            Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                getForgotPassTopView(),
                Image.asset(
                  'assets/images/forgot_password.png',
                  height: 200.h,
                  width: 200.w,
                  color: AppTheme.primaryColorDark,
                ).paddingE(EdgeInsets.only(
                    top:24.h,
                bottom: 12.h)),
                Text(
                  "FORGOT PASSWORD",
                  style: TextStyle(
                      color: AppTheme.primaryColorLight,
                      fontSize: 32.sp,
                      fontWeight: FontWeight.w700),
                ),
                Text(
                  "Enter your email address associated \nwith your account we will send you a \nlink to reset your password.",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: AppTheme.primaryColorLight,
                      fontWeight: FontWeight.w400,
                      fontSize: 24.sp),
                ).paddingTop(8.h),
                getTextField(context, emailController, "Enter Email Here")
                    .paddingAll(top:20.h,left: 20.w,right: 20.w),
                submitButton("SUBMIT", (){
if(validate()){
  forgetPass();
}
                }).paddingAll(top:50.h,left: 20.w,right: 20.w),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget getForgotPassTopView() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        InkWell(
                onTap: () => Navigator.pop(context),
                child: Image.asset(
                  'assets/images/ic_left_arrow.png',
                  height: 26,
                  width: 26,
                  color: AppTheme.primaryColorDark,
                ))
            .paddingE(EdgeInsets.only(
                top: MediaQuery.of(context).padding.top + 18.h, left: 34.w)),
      ],
    );
  }

  Widget getTextField(context, controller, hint) => SizedBox(
        height: 55.h,
        child: TextField(
          autofocus: false,
          controller: controller,
          cursorColor: Colors.white,
          textAlignVertical: TextAlignVertical.center,
          style: TextStyle(
              fontSize: 22.0.sp, color: Colors.black87, letterSpacing: 1.2),
          decoration: InputDecoration(
            filled: true,
            floatingLabelBehavior: FloatingLabelBehavior.never,
            fillColor: Color(0xFFEAEAEA),
            hintStyle: TextStyle(color: Colors.grey),
            prefixIcon: Icon(
              Icons.email,
              color: AppTheme.primaryColorDark,
            ),
            hintText: hint,
            contentPadding: EdgeInsets.only(left: 14.0.w, right: 14.w),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.white),
              borderRadius: BorderRadius.circular(8.0),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.white),
              borderRadius: BorderRadius.circular(8.0),
            ),
          ),
        ),
      );


  Widget submitButton(text,onTapped)=> SizedBox(
   width: MediaQuery.of(context).size.width*0.50,
    height: 60.h,
    child: RaisedButton(
      //splashColor: Colors.lightBlue,
      onPressed: () {
        onTapped();
        //Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);
      },
      color: AppTheme.primaryColorLight,
      child: Text(text,
          style: TextStyle(
              color: Colors.black,
              fontSize: 24.sp,
              fontWeight: FontWeight.bold)),
      //disabledColor: Colors.lightBlue,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0)),
    ),
  ).elevation(24.0, elevation: 8.0);


  void forgetPass() async {
    await progressDialog.show();
    final email = emailController.text;

    forgetPassService
        .forgetPass(email)
        .then((value) async {
      await progressDialog.hide();
      if (value != null  ) {


      context.showSuccessAlert(value);
      Navigator.pop(context);
      } else {
        context.showAlert("Email or Password is wrong.");
      }
    }).catchError((e) async {
      await progressDialog.hide();
      context.showAlert(e?.message ?? "Sorry, Something went wrong");
    });
  }

  bool validate() {
    final email = emailController.text;
    if (!Validator.isEmail(email)) {
      context.showAlert("Please enter valid Email.");
      return false;
    }
    return true;
  }


}
