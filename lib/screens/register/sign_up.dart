import 'package:ecommerceapp/app/app_theme.dart';
import 'package:ecommerceapp/base/base_state.dart';
import 'package:ecommerceapp/base/base_stateful_widget.dart';
import 'package:ecommerceapp/base/base_statefull_screen.dart';
import 'package:ecommerceapp/commons/repo/UserRepository.dart';
import 'package:ecommerceapp/screens/mainHome/MyHomePage.dart';
import 'package:ecommerceapp/screens/register/service/RegisterService.dart';
import 'package:ecommerceapp/utils/clipper/MultiCurvedClipper.dart';
import 'package:ecommerceapp/utils/extensions/basic_extensions.dart';
import 'package:ecommerceapp/utils/extensions/context.dart';
import 'package:ecommerceapp/utils/extensions/base64.dart';
import 'package:ecommerceapp/utils/responsive/size_extension.dart';
import 'package:ecommerceapp/widgets/DropDownSpinner.dart';
import 'package:ecommerceapp/widgets/ProgressDialogType.dart' as P;
import 'package:ecommerceapp/widgets/TextFieldEye.dart';
import 'package:ecommerceapp/widgets/validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:place_picker/entities/location_result.dart';
import 'package:place_picker/place_picker.dart';
import 'package:place_picker/widgets/place_picker.dart';

class SignUpScreen extends BaseStatefulWidget {
  static const routeName = '/signUp_view';

  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends BaseState<SignUpScreen>
    with BaseStatefulScreen {
  P.ProgressDialog progressDialog;
  final RegisterService registerService = RegisterService();
  final emailController = TextEditingController();
  final passController = TextEditingController();
  final confirmPassController = TextEditingController();
  final addAddress = TextEditingController();
  final phoneNumber = TextEditingController();
  final _nameController = TextEditingController();
  final _familyNameController = TextEditingController();
  final referalCode = TextEditingController();

  _SignUpScreenState() : super(defaultPadding: false);

  @override
  Widget buildBody(BuildContext context) {
    progressDialog = P.ProgressDialog(context);
    return SingleChildScrollView(
      child: Container(
        width: MediaQuery.of(context).size.width,
        color: AppTheme.primaryColorDark,
        //color: Color(0xffF5DEB3),
        child: Stack(children: <Widget>[
          Positioned(
            top: 0,
            right: 0,
            left: 0,
            child: Container(
              height: MediaQuery.of(context).size.height * 0.18,
              decoration: BoxDecoration(
                color: AppTheme.primaryColorLight,
                //color: Color(0xff78380C),
              ),
            ).clip(MultiCurvedClipper()),
          ),
          Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              getLoginTopView(),
              Align(
                alignment: Alignment.topLeft,
                child: Text(
                  "Create Account",
                  style: TextStyle(
                      fontSize: 32.sp,
                      color: Colors.white,
                      fontWeight: FontWeight.bold),
                ).paddingE(EdgeInsets.only(top: 20.h, left: 34.w)),
              ),
              getTextField(context, _nameController, "Name").paddingE(
                  EdgeInsets.only(top: 30.h, left: 34.w, right: 44.w)),
              getTextField(context, _familyNameController, "Family Name").paddingE(
                  EdgeInsets.only(top: 30.h, left: 34.w, right: 44.w)),
              getTextField(context, emailController, "Email Address").paddingE(
                  EdgeInsets.only(top: 22.h, left: 34.w, right: 44.w)),
              getPassField(context, passController, "Enter Password").paddingE(
                  EdgeInsets.only(top: 22.h, left: 34.w, right: 44.w)),
              getPassField(
                      context, confirmPassController, "Enter Confirm Password")
                  .paddingE(
                      EdgeInsets.only(top: 22.h, left: 34.w, right: 44.w)),
              getTextField(context, phoneNumber, "Contact Number",
                      isNumber: true)
                  .paddingE(
                      EdgeInsets.only(top: 22.h, left: 34.w, right: 44.w)),
              getTextField(context, referalCode, "Referral code",
                      isNumber: false)
                  .paddingE(
                      EdgeInsets.only(top: 22.h, left: 34.w, right: 44.w)),
              Stack(
                children: [
                  getTextField(context, addAddress, "Add Address").paddingE(
                      EdgeInsets.only(top: 22.h, left: 34.w, right: 44.w)),
                  Positioned(
                      right: 44.w,
                      top: 18.h,
                      child: SizedBox(
                        height: 22.h,
                        width: 100.w,
                        child: RaisedButton(
                          color: AppTheme.buttonColorLight,
                          child: Text("Pick",
                              style:
                                  TextStyle(color: AppTheme.primaryColorDark)),
                          onPressed: () async {
                            LocationResult result = await Navigator.of(context)
                                .push(MaterialPageRoute(
                                    builder: (context) => PlacePicker(
                                          "AIzaSyDLDmvcZpQKHnbkEg76bk9jMd4QB_kZw4c",
                                          displayLocation:
                                              LatLng(-33.865143, 151.209900),
                                        )));

                            // Handle the result in your way
                            if (result != null) {
                              if(result.country.shortName == "AU") {
                                addAddress.text = result.formattedAddress;
                              } else {
                                Fluttertoast.showToast(
                                    msg: "Address must be in australia.",
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.BOTTOM,
                                    timeInSecForIos: 1,
                                    textColor: Colors.white,
                                    fontSize: 14.0);
                              }
                            }

                          },
                        ),
                      )),
                  Positioned(
                      right: 160.w,
                      top: 18.h,
                      child: SizedBox(
                        height: 22.h,
                        width: 120.w,
                        child: RaisedButton(
                          color: AppTheme.buttonColorLight,
                          child: Text("Manual",
                              style:
                                  TextStyle(color: AppTheme.primaryColorDark)),
                          onPressed: () async {
                            final AddressManual manualAddress =
                                await showDialog(
                                    context: context,
                                    builder: (context) =>
                                        AddManualAddressDialog());
                            if (manualAddress != null)
                              addAddress.text =
                                  "${manualAddress.streetOne}, ${manualAddress.streetTwo}, ${manualAddress.city}, ${manualAddress.state}, ${manualAddress.postCode}";
                          },
                        ),
                      ))
                ],
              ),
              SizedBox(
                height: 30.h,
              ),
              loginButton("Sign Up", () {
                if (validate()) {
                  registerUser();
                }
                // Navigator.of(context).pushNamed(SignUpScreen.routeName);
              }),
              SizedBox(
                height: 70.h,
              )
            ],
          )
        ]),
      ),
    );
  }

  @override
  String screenName() => null;

  Widget getLoginTopView() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        InkWell(
                onTap: () => Navigator.pop(context),
                child: Image.asset(
                  'assets/images/ic_left_arrow.png',
                  height: 26,
                  width: 26,
                  color: AppTheme.primaryColorDark,
                ))
            .paddingE(EdgeInsets.only(
                top: MediaQuery.of(context).padding.top + 18.h, left: 34.w)),
        Image.asset('assets/images/app_icon.png', height: 86.h, width: 120.w)
            .paddingE(EdgeInsets.only(
                top: MediaQuery.of(context).padding.top, right: 44.w))
      ],
    );
  }

  Widget getTextField(context, controller, name, {bool isNumber = false}) =>
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(name,
              style: TextStyle(
                  fontSize: 22.sp,
                  color: Colors.white,
                  fontWeight: FontWeight.bold)),
          SizedBox(height: 8.h),
          SizedBox(
            height: 55.h,
            child: TextField(
              autofocus: false,
              controller: controller,
              keyboardType: isNumber ? TextInputType.phone : TextInputType.text,
              cursorColor: Colors.white,
              textAlignVertical: TextAlignVertical.center,
              style: TextStyle(
                  fontSize: 22.0.sp, color: Colors.black87, letterSpacing: 1.2),
              decoration: InputDecoration(
                filled: true,
                floatingLabelBehavior: FloatingLabelBehavior.never,
                fillColor: Color(0xFFEAEAEA),
                hintStyle: TextStyle(color: Colors.black87),
                contentPadding: EdgeInsets.only(left: 14.0.w, right: 14.w),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white),
                  borderRadius: BorderRadius.circular(8.0),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white),
                  borderRadius: BorderRadius.circular(8.0),
                ),
              ),
            ),
          )
        ],
      );

  Widget getPassField(context, TextEditingController controller, name) =>
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(name,
              style: TextStyle(
                  fontSize: 22.sp,
                  color: Colors.white,
                  fontWeight: FontWeight.bold)),
          SizedBox(height: 8.h),
          PasswordField(controller, "").elevation(25.7, elevation: 8),
        ],
      );

  Widget loginButton(text, onTapped) => SizedBox(
        width: MediaQuery.of(context).size.width * 0.50,
        height: 60.h,
        child: RaisedButton(
          //splashColor: Colors.lightBlue,
          onPressed: () {
            onTapped();
            //Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);
          },
          color: AppTheme.primaryColorLight,
          child: Text(text,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 24.sp,
                  fontWeight: FontWeight.bold)),
          //disabledColor: Colors.lightBlue,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(24)),
        ),
      ).elevation(24.0, elevation: 8.0);

  Widget textUnderButton() => Container(
        child: RichText(
          text: new TextSpan(
            // Note: Styles for TextSpans must be explicitly defined.
            // Child text spans will inherit styles from parent
            style: new TextStyle(
                fontSize: 22.0.sp,
                color: Colors.black,
                fontWeight: FontWeight.bold),
            children: <TextSpan>[
              new TextSpan(text: 'Not A Member? '),
              new TextSpan(
                  text: 'Register ',
                  recognizer: new TapGestureRecognizer()
                    ..onTap = () => {
                          // Navigator.of(context).pushReplacementNamed(SignUpView.routeName)
                        },
                  style: TextStyle(color: AppTheme.primaryColorLight)),
              new TextSpan(text: 'Here'),
            ],
          ),
        ),
      ).paddingTop(12.0.h);

  Widget needHelp() => Text(
        "Need Help?",
        style: TextStyle(color: Colors.black, fontSize: 18.0.sp),
      ).paddingTop(6.0.h);

  Widget signInWith() => Text(
        "Sign In With",
        style: TextStyle(color: Colors.white, fontSize: 18.0.sp),
      ).paddingTop(48.h);

  bool validate() {
    final email = emailController.text;
    final pass = passController.text;
    final name = _nameController.text;
    final confirmPass = confirmPassController.text;
    final phone = phoneNumber.text;
    final address = addAddress.text;
    final familyName= _familyNameController.text;
    final referalCodeString= referalCode.text;

    if (name.isEmpty) {
      context.showAlert("Name can't be empty.");
      return false;
    }
    if (familyName.isEmpty) {
      context.showAlert("Name can't be empty.");
      return false;
    }
    if (!Validator.isPassword(pass, minLength: 8)) {
      context.showAlert("Password must be of minimum of 8 character.");
      return false;
    }
    if (pass != confirmPass) {
      context.showAlert("Password and Confirm pass does not match.");
      return false;
    }
    if (!Validator.isEmail(email)) {
      context.showAlert("Please enter valid Email.");
      return false;
    }

    if(referalCodeString.isNotEmpty && !referalCodeString.isBase64()) {
      context.showAlert("Please enter valid referral code");
      return false;
    }

    if (phone.length < 9 || phone.length > 10) {
      context.showAlert("Please enter valid Phone number.");
      return false;
    }
    if (address.isEmpty) {
      context.showAlert("Address can't be empty.");
      return false;
    }

    return true;
  }

  void registerUser() async {
    await progressDialog.show();
    final email = emailController.text;
    final pass = passController.text;
    final name = _nameController.text;
    final address = addAddress.text;
    final phone = phoneNumber.text;
    final familyName= _familyNameController.text;
    final referalCodeText= referalCode.text.decodeBase64();
    registerService
        .registerUser(name,familyName, email, pass, phone, address, referalCodeText.isEmpty ? null : referalCodeText)
        .then((value) async {
      await progressDialog.hide();
      if (value.status) {
        // await SharedPreferenceUtil.saveUserId(value.data.userId.toString());
        Fluttertoast.showToast(
            msg: "Registered Successfully",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIos: 1,
            textColor: Colors.white,
            fontSize: 14.0);
        //await UserRepo.saveUser(value[0]);
        //Navigator.of(context).pushNamedAndRemoveUntil(MyHomePage.routeName,  ModalRoute.withName('/'));
        Navigator.pop(context);
      } else {
        context.showAlert(value.message);
      }
    }).catchError((e) async {
      await progressDialog.hide();
      context.showAlert(e?.message ?? "Sorry, Something went wrong!");
    });
  }
}

class AddManualAddressDialog extends StatefulWidget {
  @override
  _AddManualAddressDialogState createState() => _AddManualAddressDialogState();
}

class _AddManualAddressDialogState extends State<AddManualAddressDialog> {
  final streetOneTextController = TextEditingController();
  final streetTwoTextController = TextEditingController();
  final cityController = TextEditingController();
  final postCode = TextEditingController();
  String state = "";

  @override
  Widget build(BuildContext context) {
    return Dialog(
      elevation: 3,
      backgroundColor: Colors.white,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
      child: SingleChildScrollView(
        child: IntrinsicHeight(
          child: Container(
            padding: EdgeInsets.all(16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  "Street Address 1",
                  style: TextStyle(fontSize: 16),
                ).paddingTop(16.h),
                IntrinsicHeight(
                    child: TextField(
                  autofocus: true,
                  controller: streetOneTextController,
                )),
                Text(
                  "Street Address 2",
                  style: TextStyle(fontSize: 16),
                ).paddingTop(12),
                IntrinsicHeight(
                    child: TextField(
                  autofocus: true,
                  controller: streetTwoTextController,
                )),
                Text(
                  "City Name",
                  style: TextStyle(fontSize: 16),
                ).paddingTop(12),
                IntrinsicHeight(
                    child: TextField(
                  autofocus: true,
                  controller: cityController,
                )),
                Text(
                  "Choose State",
                  style: TextStyle(fontSize: 16),
                ).paddingTop(12),
                DropDownSpinner(
                  [
                    StringSpinnerItem("NSW"),
                    StringSpinnerItem("QLD"),
                    StringSpinnerItem("SA"),
                    StringSpinnerItem("TAS"),
                    StringSpinnerItem("VIC"),
                    StringSpinnerItem("WA")
                  ],
                  (selectedValue) {
                    if(selectedValue != null)
                    state = (selectedValue as StringSpinnerItem).value;
                  },
                  hint: "State",
                ).paddingTop(8),
                Text(
                  "Post Code",
                  style: TextStyle(fontSize: 16),
                ).paddingTop(12),
                IntrinsicHeight(
                    child: TextField(
                      keyboardType: TextInputType.number,
                  autofocus: true,
                  controller: postCode,
                )),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Text(
                        "DISMISS",
                        style: TextStyle(
                            fontSize: 18, color: AppTheme.primaryColorDark),
                      ).paddingRight(16),
                    ),
                    InkWell(
                      onTap: () {
                        if (validate())
                          Navigator.pop(
                              context,
                              AddressManual(
                                  streetOneTextController.text,
                                  streetTwoTextController.text,
                                  cityController.text,
                                  state,
                                  postCode.text));
                      },
                      child: Text(
                        "DONE",
                        style: TextStyle(
                            fontSize: 18, color: AppTheme.primaryColorDark),
                      ),
                    ),
                  ],
                ).paddingAll(top: 30)
              ],
            ),
          ),
        ),
      ),
    );
  }

  bool validate() {
    if (streetOneTextController.text.isEmpty) {
      showFieldMandatoryToast();
      return false;
    } else if (streetTwoTextController.text.isEmpty) {
      showFieldMandatoryToast();
      return false;
    } else if (cityController.text.isEmpty) {
      showFieldMandatoryToast();
      return false;
    } else if (state.isEmpty) {
      showFieldMandatoryToast();
      return false;
    } else if (postCode.text.isEmpty) {
      showFieldMandatoryToast();
      return false;
    }

    return true;
  }

  void showFieldMandatoryToast() {
    Fluttertoast.showToast(
        msg: "All fields are mandatory",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        textColor: Colors.white,
        fontSize: 14.0);
  }
}

class AddressManual {
  final String streetOne;
  final String streetTwo;
  final String city;
  final String state;
  final String postCode;

  AddressManual(
      this.streetOne, this.streetTwo, this.city, this.state, this.postCode);
}
