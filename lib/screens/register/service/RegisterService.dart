import 'package:dio/dio.dart';
import 'package:ecommerceapp/commons/constants/app_constants.dart';
import 'package:ecommerceapp/network/dio_rest_client.dart';

class RegisterService {
  Future<RegisterResponse> registerUser(name, familyName, email, password, phoneNumber,
      address, referUserId) async {
    ApiResponse apiResponse =
        await ApiHitter.getPostApiResponse(AppConstants.REGISTER,
            formData: FormData.fromMap({
              "name": name,
              "family_name": familyName,
              "email": email,
              "password": password,
              "phone": phoneNumber,
              "Address1": address,
              "refer_usr_id": referUserId == null ? "" : referUserId,
              "refer_price": referUserId == null ? "" : "10"
            }));
    try {
      return apiResponse.response != null
          ? (apiResponse.response.data as String)
                      .contains("\"User Registered successfully.\"")
              ? RegisterResponse(true, "User Registered Successfully")
              : RegisterResponse(false, apiResponse.response.data)
          : RegisterResponse(false, "Sorry,something went wrong");
    } catch (e) {
      print(e..toString());
      return null;
    }
  }
}



class RegisterResponse {

  final bool status;
  final String message;

  RegisterResponse(this.status, this.message);


}