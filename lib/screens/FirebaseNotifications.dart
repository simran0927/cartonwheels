import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:locally/locally.dart';

Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) async {
  if (message.containsKey('data')) {
    // Handle data message
    final dynamic data = message['data'];
    print("background: $data");
  }

  if (message.containsKey('notification')) {
    // Handle notification message
    final dynamic notification = message['notification'];
    try {
      Locally locally = Locally(
        payload: 'Cart On Wheels',
        appIcon: 'mipmap/ic_launcher',
      );

//        final notification = message['notification'] as Map<String, dynamic>;
      locally.show(title: message['notification']['title'], message: message['notification']['body']);
    } catch (e) {
      print(e);
    }
    print("background: $notification");
  }

  // Or do other work.
}

class FirebaseNotifications {
  FirebaseMessaging _firebaseMessaging;

  void setUpFirebase() {
    _firebaseMessaging = FirebaseMessaging();
    firebaseCloudMessaging_Listeners();

  }

  // ignore: non_constant_identifier_names
  void firebaseCloudMessaging_Listeners() {
    if (Platform.isIOS) iOS_Permission();

    _firebaseMessaging.getToken().then((token) {
      print(token);
    });

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('on message $message');

        try {
          Locally locally = Locally(
                    payload: 'Cart On Wheels',
                    appIcon: 'mipmap/ic_launcher',
                  );

//        final notification = message['notification'] as Map<String, dynamic>;
          locally.show(title: message['notification']['title'], message: message['notification']['body']);
        } catch (e) {
          print(e);
        }
      },
      onBackgroundMessage: myBackgroundMessageHandler,
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
      },
    );
  }

  void iOS_Permission() {
    _firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }
}