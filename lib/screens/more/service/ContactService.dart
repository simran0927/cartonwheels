import 'package:dio/dio.dart';
import 'package:ecommerceapp/commons/constants/app_constants.dart';
import 'package:ecommerceapp/model/LoginData.dart';
import 'package:ecommerceapp/model/common_response.dart';
import 'package:ecommerceapp/network/dio_rest_client.dart';

class ContactService{
  Future<String> sendEmail( fname,lname,email, subject,message) async {
    ApiResponse apiResponse =
    await ApiHitter.getPostApiResponse(AppConstants.SEND_EMAIL, formData: FormData.fromMap({
      "email": email,
      "fname":fname,
      "lname":lname,
      "subject":subject,
      "message":message
    }));
    try {
      return apiResponse.response != null
          ? apiResponse.response.data
          : null;
    } catch (e) {
      print(e..toString());
      return null;
    }

  }
}