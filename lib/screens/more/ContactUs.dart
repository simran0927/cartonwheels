import 'package:ecommerceapp/app/app_theme.dart';
import 'package:ecommerceapp/base/base_state.dart';
import 'package:ecommerceapp/base/base_stateful_widget.dart';
import 'package:ecommerceapp/base/base_statefull_screen.dart';
import 'package:ecommerceapp/screens/more/service/ContactService.dart';
import 'package:ecommerceapp/widgets/ProgressDialogType.dart';
import 'package:ecommerceapp/widgets/validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:ecommerceapp/utils/extensions/basic_extensions.dart';
import 'package:ecommerceapp/utils/extensions/context.dart';


class ContactUs extends BaseStatefulWidget {
  static const routeName = '/contact_us';
  @override
  _ContactUsState createState() => _ContactUsState();
}

class _ContactUsState extends BaseState<ContactUs> with BaseStatefulScreen {
  _ContactUsState() : super(defaultPadding: false);

  final emailController = TextEditingController();
  final fnameController = TextEditingController();
  final lnameController = TextEditingController();
  final subjectController = TextEditingController();
  final messageController= TextEditingController();

  ProgressDialog progressDialog;
  final ContactService contactService = ContactService();


  @override
  String screenName() => "Contact Us";


  @override
  Widget buildBody(BuildContext context) {
    progressDialog = ProgressDialog(context);
    return SingleChildScrollView(
      child: Container(

        color: Color(0xffd3d3d3),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(16.0),
              color: Colors.white,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Please let me know what issues you\'re facing and we\'ll get back to you soon.",),
                  SizedBox(height: 16,),

                  TextField(
                    controller: fnameController,
                    decoration: InputDecoration(
                      labelText: "First Name",
                      fillColor: Colors.black,
                      border:OutlineInputBorder(
                        borderSide: const BorderSide(color: Colors.black, width: 2.0),
                        borderRadius: BorderRadius.circular(16.0),
                      ) ,
                      focusedBorder:OutlineInputBorder(
                        borderSide: const BorderSide(color: Colors.black, width: 2.0),
                        borderRadius: BorderRadius.circular(16.0),
                      ),
                    ),
                  ),
                  SizedBox(height: 20,),
                  TextField(
                    controller: lnameController,
                    decoration: InputDecoration(
                      labelText: "Last Name",
                      fillColor: Colors.black,
                      border:OutlineInputBorder(
                        borderSide: const BorderSide(color: Colors.black, width: 2.0),
                        borderRadius: BorderRadius.circular(16.0),
                      ) ,
                      focusedBorder:OutlineInputBorder(
                        borderSide: const BorderSide(color: Colors.black, width: 2.0),
                        borderRadius: BorderRadius.circular(16.0),
                      ),
                    ),
                  ),
                  SizedBox(height: 16,),
                  TextField(
                    controller: emailController,
                    decoration: InputDecoration(
                        labelText: "Enter Email Address",
                      fillColor: Colors.black,
                      border:OutlineInputBorder(
                        borderSide: const BorderSide(color: Colors.black, width: 2.0),
                        borderRadius: BorderRadius.circular(16.0),
                      ) ,
                      focusedBorder:OutlineInputBorder(
                        borderSide: const BorderSide(color: Colors.black, width: 2.0),
                        borderRadius: BorderRadius.circular(16.0),
                      ),
                    ),
                  ),
                  SizedBox(height: 16,),
                  TextField(
                    controller: subjectController,
                    decoration: InputDecoration(
                      labelText: "Enter Subject",
                      fillColor: Colors.black,
                      border:OutlineInputBorder(
                        borderSide: const BorderSide(color: Colors.black, width: 2.0),
                        borderRadius: BorderRadius.circular(16.0),
                      ) ,
                      focusedBorder:OutlineInputBorder(
                        borderSide: const BorderSide(color: Colors.black, width: 2.0),
                        borderRadius: BorderRadius.circular(16.0),
                      ),
                    ),
                  ),
                  SizedBox(height: 24,),

                TextField(
                  controller: messageController,
                  textAlign: TextAlign.start,
                  decoration: InputDecoration(
                      labelText: "Tell Us More",

                    fillColor: Colors.black,
                    border:OutlineInputBorder(
                      borderSide: const BorderSide(color: Colors.black, width: 2.0),
                      borderRadius: BorderRadius.circular(16.0),
                    ) ,
                    focusedBorder:OutlineInputBorder(
                      borderSide: const BorderSide(color: Colors.black, width: 2.0),
                      borderRadius: BorderRadius.circular(16.0),
                    ),
                  ),
                  minLines: 8,
                  maxLines: 20,
                ),
                  SizedBox(height: 16,),
                  submitButton("Submit", (){
                    if(validate())
                    sendEmail();
                  }),

                ],
              ),
            ),
            SizedBox(height: 16,),
            bottomCard("Call Us : 1300591645"),
            SizedBox(height: 16,),
            bottomCard("Send Email : Contact@cartonwheels.com.au"),
          ],
        ),
      ),
    );
  }

  
  Widget bottomCard(text){
    return Container(
      height: 50,
      width: MediaQuery.of(context).size.width,
      color: Colors.white,
      child:Align(
          alignment: Alignment.center,
          child: Text(text,textAlign: TextAlign.center,)),
    );
  }
  Widget submitButton(text,onTapped)=> SizedBox(
    width: MediaQuery.of(context).size.width,
    height: 50,
    child: RaisedButton(
      //splashColor: Colors.lightBlue,
      onPressed: () {
        onTapped();
        //Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);
      },
      color:AppTheme.primaryColorDark,
      child: Text(text,
          style: TextStyle(
              color: AppTheme.primaryColorLight,
              fontSize: 18,
              fontWeight: FontWeight.bold)),
      //disabledColor: Colors.lightBlue,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8)),
    ),
  ).elevation(24.0, elevation: 8.0);



  bool validate() {
    final email = emailController.text;
    final fname = fnameController.text;
    final lname = lnameController.text;
    final subject = subjectController.text;
    final message= messageController.text;


    if (fname.isEmpty) {
      context.showAlert("First name cannot be empty.");
      return false;
    }
   if(lname.isEmpty){
     context.showAlert("Last name cannot be empty.");
     return false;
   }

    if(subject.isEmpty){
      context.showAlert("Subject cannot be empty.");
      return false;
    }

    if(message.isEmpty){
      context.showAlert("Message cannot be empty.");
      return false;
    }


    if (!Validator.isEmail(email)) {
      context.showAlert("Please enter valid Email.");
      return false;
    }
    return true;
  }

  void sendEmail() async {
    await progressDialog.show();
    final email = emailController.text;
    final fname = fnameController.text;
    final lname = lnameController.text;
    final subject = subjectController.text;
    final message= messageController.text;

    contactService
        .sendEmail(fname,lname,email,subject,message)
        .then((value) async {
      await progressDialog.hide();
      if (value != null) {
        emailController.text="";
        fnameController.text="";
        lnameController.text="";
        subjectController.text="";
        messageController.text="";
        context.showSuccessAlert(value);
      } else {
        context.showAlert("Email cannot be sent.");
      }
    }).catchError((e) async {
      await progressDialog.hide();
      context.showAlert(e?.message ?? "Sorry, Something went wrong");
    });
  }
}
