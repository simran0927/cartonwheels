import 'package:ecommerceapp/base/base_state.dart';
import 'package:ecommerceapp/base/base_stateful_widget.dart';
import 'package:ecommerceapp/base/base_statefull_screen.dart';
import 'package:ecommerceapp/screens/more/WebViewModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:webview_flutter/webview_flutter.dart';

class TermsConditions extends BaseStatefulWidget{
  static const routeName = '/terms';
  WebViewModel webViewModel;
  @override
  _TermsConditionsState createState()=>_TermsConditionsState();

}

class _TermsConditionsState extends BaseState<TermsConditions> with BaseStatefulScreen{
  final _key = UniqueKey();

  @override
  String screenName() => widget.webViewModel!=null?widget.webViewModel.screenName:"";

@override
  void initState() {
  Future.delayed(Duration.zero,(){
    setState(() {
      widget.webViewModel = ModalRoute.of(context).settings.arguments as WebViewModel;

    });
  });
    super.initState();

  }

  @override
  Widget buildBody(BuildContext context) {

    return  Column(
      children: [
        widget.webViewModel!=null? Expanded(
            child: WebView(
                key: _key,
                javascriptMode: JavascriptMode.unrestricted,
                initialUrl: widget.webViewModel.url)):Center(child: Text("No Data Found."),)
      ],
    );
  }



}