import 'package:ecommerceapp/app/app_theme.dart';
import 'package:ecommerceapp/base/base_state.dart';
import 'package:ecommerceapp/base/base_stateful_widget.dart';
import 'package:ecommerceapp/base/base_statefull_screen.dart';
import 'package:ecommerceapp/screens/account/Account_Screen.dart';
import 'package:ecommerceapp/screens/more/ContactUs.dart';
import 'package:ecommerceapp/screens/more/TermsConditions.dart';
import 'package:ecommerceapp/screens/more/WebViewModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ecommerceapp/utils/extensions/basic_extensions.dart';

class MoreScreen extends StatefulWidget{
  @override
  _MoreScreenState createState()=>_MoreScreenState();

}

class _MoreScreenState extends State<MoreScreen>{



  Widget contentWidget(onTapped,text){
    return InkWell(
      onTap: (){
        onTapped();
      },
      child: Container(
        color: Colors.white,
        height: 55,
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.only(left: 20),
        child: Align(alignment: Alignment.centerLeft,child: Text(text)),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(
        "More",
      ), centerTitle: true,),
      body: SingleChildScrollView(
        child: Container(

          width: MediaQuery.of(context).size.width,
          margin: EdgeInsets.only(top:24),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,

            children: <Widget>[
              contentWidget((){
                Navigator.of(context).pushNamed(TermsConditions.routeName,arguments: WebViewModel("Terms and Conditions","https://cartonwheels.com.au/api/terms/") );
              }, "Terms of Use"),
              SizedBox(height: 1),
              contentWidget((){
                Navigator.of(context).pushNamed(TermsConditions.routeName,arguments: WebViewModel("FAQs","https://cartonwheels.com.au/api/faq/") );
              }, "FAQs"),
              SizedBox(height: 1),
              contentWidget((){

                Navigator.of(context).pushNamed(TermsConditions.routeName,arguments: WebViewModel("Privacy Policy","https://cartonwheels.com.au/api/privacy/" ));
              }, "Privacy Policy"),
              SizedBox(height: 1),
              contentWidget((){
                Navigator.of(context).pushNamed(TermsConditions.routeName,arguments: WebViewModel("Delivery Policy","https://cartonwheels.com.au/api/deliverypolicy/") );
              }, "Delivery Policy"),
              SizedBox(height: 1,),
              contentWidget((){
                Navigator.of(context).pushNamed(TermsConditions.routeName,arguments: WebViewModel("Return Policy","https://cartonwheels.com.au/api/returnpolicy/"));
              }, "Return Policy"),
              SizedBox(height: 1),

              contentWidget((){
                Navigator.of(context).pushNamed(ContactUs.routeName);
              }, "Contact Us"),
              SizedBox(height: 60),
              Text("Copyright © 2020 EIf Digital Pty Ltd All Rights Reserved.   ABN 98629145026/ Trading as Cart On Wheels",textAlign: TextAlign.center,).paddingAll(left: 20,right: 20)
            ],
          ),
        ),
      ),
    );
  }

}