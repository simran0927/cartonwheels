import 'package:carousel_pro/carousel_pro.dart';
import 'package:ecommerceapp/commons/repo/UserRepository.dart';
import 'package:ecommerceapp/utils/extensions/base64.dart';
import 'package:flutter/material.dart';
import 'package:share/share.dart';

class TopPromoSlider extends StatelessWidget {
  List<String> bannerImages;

  TopPromoSlider(this.bannerImages);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 10, right: 10),
      child: Container(
          height: 220.0,
          width: double.infinity,
          child: Carousel(
            images: bannerImages.map((e) {
              return InkWell(
                onTap: () {
                  if (e.contains("wallet")) {
                    Share.share("${UserRepo.user.name} has invited you to Cart On Wheels, online Indian Grocery store.\nSignUp with the refer code \"${UserRepo.user.usersId.encodeBase64()}\", on Android or iOS app and get \$10 in your wallet straight away.\nDownload Android app-\nhttps://play.google.com/store/apps/details?id=com.caronwheels&hl=en&gl=AU\nDownload iOS app-\nhttps://apps.apple.com/us/app/cart-on-wheels/id1542195083");
                  }
                },
                child: Image.asset(
                  e,
                  fit: BoxFit.fill,
                  width: double.infinity,
                  height: 150,
                ),
              );
            }).toList(),
            dotSize: 4.0,
            dotSpacing: 15.0,
            dotColor: Colors.purple,
            indicatorBgPadding: 5.0,
            dotBgColor: Colors.black54.withOpacity(0.2),
            borderRadius: true,
            radius: Radius.circular(20),
            moveIndicatorFromBottom: 180.0,
            noRadiusForIndicator: true,
          )),
    );
  }
}
