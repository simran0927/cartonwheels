import 'package:dio/dio.dart';
import 'package:ecommerceapp/commons/constants/app_constants.dart';
import 'package:ecommerceapp/model/CategoryData.dart';
import 'package:ecommerceapp/model/common_response.dart';
import 'package:ecommerceapp/network/dio_rest_client.dart';

class HomeService{
  Future<List<CategoryData>> getAllCategories(tableName) async {
    ApiResponse apiResponse =
    await ApiHitter.getPostApiResponse(AppConstants.FETCHALL, formData: FormData.fromMap({
      "table": tableName
    }));
    try {
      return apiResponse.response != null
          ?categoryDataFromJson(apiResponse.response.data)
          : "Null List Found.";
    } catch (e) {
      print(e..toString());
      return null;
    }
  }
}