import 'dart:convert';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:ecommerceapp/commons/constants/app_constants.dart';
import 'package:ecommerceapp/model/CategoryData.dart';
import 'package:ecommerceapp/network/model/CategoryModel.dart';
import 'package:ecommerceapp/screens/products/Products.dart';
import 'package:ecommerceapp/screens/sub_category/sub_category_page.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';

import '../mainHome/widgets/CircularProgress.dart';

class CategoryView extends StatefulWidget {
  final String slug;
  final bool isSubList;
  final List<CategoryData> categoriesList;

  CategoryView({Key key, this.slug, this.isSubList = false, this.categoriesList}) : super(key: key);

  @override
  _CategoryViewState createState() => _CategoryViewState();
}

class _CategoryViewState extends State<CategoryView> {

  List<CategoryModel> categories;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return createListView(context, widget.categoriesList, widget.isSubList);
    /*return FutureBuilder(
      future: getCategoryList(widget.slug, widget.isSubList),
      builder: (context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.waiting:
            return CircularProgress();
          default:
            if (snapshot.hasError)
              return Text('Error: ${snapshot.error}');
            else
              return createListView(context, snapshot, widget.isSubList);
        }
      },
    );*/
  }


  /*Widget createListView(
      BuildContext context, AsyncSnapshot snapshot, bool isSubList) {
    List<CategoryModel> values = snapshot.data;
    return GridView.count(
      shrinkWrap: true,
      crossAxisCount: 3,
      physics: NeverScrollableScrollPhysics(),
      padding: EdgeInsets.all(1.0),
      childAspectRatio: 8.0 / 9.0,
      children: List<Widget>.generate(values.length, (index) {
        return GridTile(
            child: GridTilesCategory(
              name: values[index].name,
              imageUrl: values[index].imageUrl,
              slug: values[index].slug,
              fromSubProducts: isSubList,
            ));
      }),
    );
  }

}*/

Widget createListView(
    BuildContext context, List<CategoryData> categoryList, bool isSubList) {
  return GridView.count(
    shrinkWrap: true,
    crossAxisCount: 3,
    physics: NeverScrollableScrollPhysics(),
    padding: EdgeInsets.all(1.0),
    childAspectRatio: 8.0 / 9.0,
    children: List<Widget>.generate(categoryList.length, (index) {
      return GridTile(
          child: GridTilesCategory(
            name: categoryList[index].catName,
            imageUrl: categoryList[index].photo,
           // slug: values[index].slug,
            fromSubProducts: isSubList,
            catId: categoryList[index].catId
          ));
    }),
  );
}

}





class GridTilesCategory extends StatelessWidget {
  String name;
  String imageUrl;
  String slug;
  bool fromSubProducts = false;
  String catId;

  GridTilesCategory(
      {
        @required this.name,
        @required this.imageUrl,
        @required this.slug,
        this.fromSubProducts,
        @required this.catId});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {

        Navigator.of(context).pushNamed(SubCategoryPage.routeName, arguments: catId);
        /*if (fromSubProducts) {
          print(slug);

        *//*  Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ProductsScreen(
                  slug: "products/?page=1&limit=12&category=" + slug,
                  name: name,
                )),
          );*//*
        } else {
          *//*Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => SubCategoryScreen(
                  slug: slug,
                )),
          );*//*
        }*/
      },
      child: Card(
          color: Colors.white,
          elevation: 0,
          child: Column(
            children: <Widget>[
              Flexible(
                flex: 2,
                child: CachedNetworkImage(
                  imageUrl:AppConstants.IMAGEURL+imageUrl,
                  width: double.infinity,
                  fit: BoxFit.cover,
                  height: double.infinity,
                ),
              ),
              Flexible(
                flex: 1,
                child: Center(
                  child: Text(name,
                      style: TextStyle(
                          color: Color(0xFF000000),
                          fontFamily: 'Roboto-Light.ttf',
                          fontSize: 12)),
                ),
              )
            ],
          )),
    );
  }
}


// https://api.evaly.com.bd/core/public/categories/?parent=bags-luggage-966bc8aac     sub cate by slug
