import 'package:ecommerceapp/app/app_theme.dart';
import 'package:ecommerceapp/model/CategoryData.dart';
import 'package:ecommerceapp/screens/cart/provider/UpdateCartProvider.dart';
import 'package:ecommerceapp/screens/home/SearchWidget.dart';
import 'package:ecommerceapp/screens/home/service/HomeService.dart';
import 'package:ecommerceapp/screens/search/SearchScreen.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:focus_detector/focus_detector.dart';
import 'package:provider/provider.dart';

import '../FirebaseNotifications.dart';
import 'CategoryView.dart';
import 'TopPromoSlider.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  HomeService homeService;
  List<CategoryData> categoryList;
  List<CategoryData> sliderCategoryList = [];

  @override
  void initState() {
    super.initState();
    FirebaseNotifications().setUpFirebase();
    homeService = HomeService();
    getAllCategories();
  }

  void getAllCategories() async {
    homeService.getAllCategories("category").then((value) {
      if (value != null && value.length > 0) {
        setState(() {
          categoryList = value;
          if (categoryList != null && categoryList.length > 0) {
            categoryList.forEach((element) {
              if (element.catSlider == CatSlider.YES) {
                sliderCategoryList.add(element);
              }
            });
          }
        });
      }
    }).whenComplete(() async {

    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          SearchWidget(() async {
            await Navigator.push(context,
                MaterialPageRoute(builder: (context) => SearchScreen()));
          }),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  sliderCategoryList != null && sliderCategoryList.length > 0
                      ? TopPromoSlider(["assets/images/delivery_banner.jpg","assets/images/wallet_banner.png"])
                      : SizedBox(),
                  SizedBox(
                    height: 10,
                    child: Container(
                      color: Color(0xFFf5f6f7),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "Categories".toUpperCase(),
                      style: TextStyle(
                          fontSize: 22,
                          fontWeight: FontWeight.bold,
                          color: AppTheme.primaryColorDark),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                    child: Container(
                      color: Color(0xFFf5f6f7),
                    ),
                  ),
                  /*GridView.builder(
                    itemCount: categoryList?.length ?? 0,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          childAspectRatio: 8 / 9, crossAxisCount: 3),
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index) {
                        return GridTilesCategory(
                          slug: '',
                            name: categoryList[index].catName,
                            imageUrl: categoryList[index].photo,
                            // slug: values[index].slug,
                            fromSubProducts: false,
                            catId: categoryList[index].catId);
                      })*/
                  categoryList != null && categoryList.length > 0
                      ? CategoryView(
                          slug: 'categories/',
                          categoriesList: categoryList,
                        )
                      : Center(
                          child: Text("No Categories Found."),
                        )
                ],
              ),
            ),
          ),
          // PopularMenu(),
        ],
      ),
    );
  }
}
