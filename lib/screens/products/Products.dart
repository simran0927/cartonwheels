import 'package:ecommerceapp/app/app_theme.dart';
import 'package:ecommerceapp/base/base_state.dart';
import 'package:ecommerceapp/base/base_stateful_widget.dart';
import 'package:ecommerceapp/base/base_statefull_screen.dart';
import 'package:ecommerceapp/commons/repo/UserRepository.dart';
import 'package:ecommerceapp/constants/values.dart';
import 'package:ecommerceapp/model/CartResult.dart';
import 'package:ecommerceapp/model/ProductData.dart';
import 'package:ecommerceapp/screens/cart/cart_bottom_sheet.dart';
import 'package:ecommerceapp/screens/cart/provider/UpdateCartProvider.dart';
import 'package:ecommerceapp/screens/cart/service/CartService.dart';
import 'package:ecommerceapp/screens/details/product_details.dart';
import 'package:ecommerceapp/screens/products/filter_sheet.dart';
import 'package:ecommerceapp/screens/products/product_item.dart';
import 'package:ecommerceapp/screens/products/service/ProductsService.dart';
import 'package:ecommerceapp/screens/search/SearchScreen.dart';
import 'package:ecommerceapp/utils/extensions/basic_extensions.dart';
import 'package:ecommerceapp/utils/responsive/size_extension.dart';
import 'package:ecommerceapp/widgets/ProgressDialogType.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:focus_detector/focus_detector.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class ProductParam {
  final String catId;
  final String subCatId;

  ProductParam(this.catId, this.subCatId);
}

class Products extends BaseStatefulWidget {
  static const routeName = '/category_items';

  @override
  _CategoryItemsState createState() => _CategoryItemsState();
}

class _CategoryItemsState extends BaseState<Products> with BaseStatefulScreen {
  _CategoryItemsState() : super(defaultPadding: false);

  ProductsService productsService;
  ProgressDialog progressDialog;
  List<ProductData> productList;
  ProductParam productParam;
  final _resumeDetectorKey = UniqueKey();
  UpdateCartProvider updateCartProvider;

  @override
  String screenName() => "Products";

  @override
  void initState() {
    progressDialog = ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: true);
    super.initState();
    productsService = ProductsService();
    Future.delayed(Duration.zero, () {
      productParam = ModalRoute.of(context).settings.arguments as ProductParam;
      getAllProducts();
     // updateCartProvider.fetchCartItems();
    });
  }

  void getAllProducts() async {
    await progressDialog.show();
    productsService
        .searchProducts("",
            category: productParam.catId, subCat: productParam.subCatId)
        .then((value) {
      if (value != null && value.length > 0) {
        setState(() {
          productList = value;
        });
      }
    }).whenComplete(() async {
      await progressDialog.hide();
    });
  }

  @override
  Widget buildBody(BuildContext context) {
    updateCartProvider = Provider.of<UpdateCartProvider>(context);
    progressDialog = ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: true);

    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      color: AppTheme.primaryColorLight,
      child: Column(
        children: <Widget>[
          Expanded(
            child: Column(
              children: <Widget>[
                topFilterView(context),
                Expanded(
                  child: productList != null && productList.length > 0
                      ? ListView.builder(
                          //physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          scrollDirection: Axis.vertical,
                          itemCount: productList.length,
                          itemBuilder: (context, pos) {
                            return ProductItem(() {
                              Navigator.of(context).pushNamed(
                                  ProductDetailScreen.routeName,
                                  arguments: productList[pos]);
                            }, productList[pos],updateCartProvider);
                          })
                      : Center(
                          child: Text("No Products Found."),
                        ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  List<Widget> setAppbarWidgets() {
    updateCartProvider = Provider.of<UpdateCartProvider>(context);
    return <Widget>[

      Stack(
        children: [
          IconButton(
            iconSize: 18,
            onPressed: () {
              showModalBottomSheet(
                shape: roundedRectangle40,
                context: context,
                builder: (context) => CartBottomSheet(),
              );
            },
            icon: Icon(FontAwesomeIcons.shoppingCart),
            color: Colors.white,
          ),
          updateCartProvider.items != null && updateCartProvider.items.length > 0 ? Positioned(
            right: 4,
            top: 10,
            child: Container(
              height: 17,
              width: 17,
              decoration:
              BoxDecoration(color: Colors.red, shape: BoxShape.circle),
              child: Center(
                child: Text(
                    updateCartProvider.items.length.toString(),

                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 10,
                        color: Colors.white)
                ),
              ),
            ),
          ): SizedBox(),
        ],
      ),
      IconButton(
        icon: Icon(Icons.search),
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => SearchScreen(
                    catId: productParam.catId,
                  )));
        },
      ),
      /* Icon(Icons.add_shopping_cart).paddingAll(left: 16.0,right: 16.0)*/
    ];
  }

  Widget topFilterView(context) {
    return Container(
      padding: EdgeInsets.only(bottom: 0.0),
      child: Column(
        children: <Widget>[
          Container(
            padding:
                EdgeInsets.only(left: 16.0, right: 16.0, top: 8.0, bottom: 8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text("${productList?.length ?? 0} results"),
                /*InkWell(
                  onTap: (){
                    return showModalBottomSheet(context: context, builder: (context){
                      return FilterSheet();
                    });

                  },
                  child: Column(
                    children: <Widget>[
                     Row(
                       children: <Widget>[
                         Icon(Icons.filter_list,
                         color: AppTheme.primaryColorDark,
                         size: 25,),
                         Text("SORT BY",style: TextStyle(color:AppTheme.primaryColorDark ),)
                       ],),
                      Text("Relevance")
                    ],
                  ),
                )*/
              ],
            ),
          ),
          Divider(thickness: 1.0, color: AppTheme.primaryColorDark)
        ],
      ),
    );
  }

}
