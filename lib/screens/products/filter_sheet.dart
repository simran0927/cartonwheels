 import 'package:ecommerceapp/app/app_theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:ecommerceapp/utils/extensions/basic_extensions.dart';
import 'package:flutter/material.dart';

class FilterSheet extends StatefulWidget{
  @override
  _FilterSheetState createState() =>_FilterSheetState();
}

class _FilterSheetState extends State<FilterSheet>{
  @override
  Widget build(BuildContext context) {
   return SingleChildScrollView(
     child: Container(
       padding: EdgeInsets.only(top:16.0,bottom: 8.0),
       child: Column(
         crossAxisAlignment: CrossAxisAlignment.start,
         children: <Widget>[
           Text("Sort by",style: TextStyle(
 fontWeight: FontWeight.w600,
             color: Colors.black,
             fontSize: 25.0
           ),).paddingAll(left:16.0,bottom: 12.0),
           Divider(
               thickness: 1.0,
               color:Colors.black
           ),
           sortBy("Relevance", true),
           sortBy("A to Z (Product name)", false),
           sortBy("Z to A (Product name)", false),
           sortBy("New", false),
           sortBy("Price Low to High", false),
           sortBy("Price High to Low", false),
           sortBy("Unit Price Low to High", false),
           sortBy("Unit Price High to Low", false),

         ],
       ),

          ),
   );
  }


  Widget sortBy(text,isSelect){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(text,style: TextStyle(
          fontSize: 20.0,
        ),),
       isSelect? Icon(Icons.check,color: Colors.green,):SizedBox()
      ],
    ).paddingAll(left:16.0,bottom: 18.0,right: 16.0);
  }

}