import 'package:cached_network_image/cached_network_image.dart';
import 'package:ecommerceapp/app/app_theme.dart';
import 'package:ecommerceapp/commons/constants/app_constants.dart';
import 'package:ecommerceapp/commons/repo/UserRepository.dart';
import 'package:ecommerceapp/model/ProductData.dart';
import 'package:ecommerceapp/screens/cart/provider/UpdateCartProvider.dart';
import 'package:ecommerceapp/screens/products/service/ProductsService.dart';
import 'package:ecommerceapp/utils/extensions/basic_extensions.dart';
import 'package:ecommerceapp/utils/responsive/flutter_screenutil.dart';
import 'package:ecommerceapp/utils/responsive/size_extension.dart';
import 'package:ecommerceapp/widgets/ProgressDialogType.dart' as P;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ProductItem extends StatefulWidget {
  Function onItemPresssed;
  ProductData product;
  UpdateCartProvider updateCartProvider;
  ProductItem(this.onItemPresssed, this.product,this.updateCartProvider);

  @override
  _ProductItemState createState() => _ProductItemState();
}

class _ProductItemState extends State<ProductItem> {
  int number = 1;
  P.ProgressDialog progressDialog;
  ProductsService productsService;

  Widget leftColumn() {
    return Container(
      constraints: BoxConstraints(minHeight: 200),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          /* Image.asset(
            "assets/images/fruit.png",
            width: 90.w,
            height: 90.h,

          ),*/
          CachedNetworkImage(
            imageUrl: AppConstants.IMAGEURL + this.widget.product.photo,
            width: 110.w,
            height: 110.h,

          ),

          Align(
            alignment: Alignment.bottomCenter,
            child: widget.product.isStockAvailable() ? Row(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                new InkWell(
                    onTap: minus,
                    child:   Container(
                      padding: EdgeInsets.all(5.0),
                      decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          color: Colors.amber,
                          borderRadius: BorderRadius.all(Radius.circular(8.0))),
                      child: Icon(
                        Icons.remove,
                        color: Colors.white,
                        size: 15,
                      ),
                    )
                ),
                SizedBox(
                  width: 6.0,
                ),
                new Text('$number', style: new TextStyle(fontSize: 18.0)),
                SizedBox(
                  width: 6.0,
                ),
                new InkWell(
                    onTap: add,
                    child: Container(
                      padding: EdgeInsets.all(5.0),
                      decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          color: Colors.amber,
                          borderRadius: BorderRadius.all(Radius.circular(8.0))),
                      child: Icon(
                        Icons.add,
                        color: Colors.white,
                        size: 15,
                      ),
                    )),
              ],
            ) : SizedBox(),
          ).paddingBottom(16),
        ],
      ),
    );
  }

  Widget rightColumn() {
    return Container(
      constraints: BoxConstraints(minHeight: 200),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            this.widget.product.proName,
            style: TextStyle(fontSize: 28.sp, fontWeight: FontWeight.w600),
          ),
          Text(
            "\$ ${this.widget.product.proAfterprice != null &&
                this.widget.product.proAfterprice.isNotEmpty &&
                this.widget.product.proPrice !=
                    this.widget.product.proAfterprice
                ? this.widget.product.proAfterprice
                : this.widget.product.proPrice}" ,
            style: TextStyle(fontSize: 28.sp, fontWeight: FontWeight.w800),
          ).paddingTop(8.0),
          this.widget.product.proAfterprice != null &&
                  this.widget.product.proAfterprice.isNotEmpty &&
                  this.widget.product.proPrice !=
                      this.widget.product.proAfterprice
              ? Text(
                  "\$ " + this.widget.product.proPrice,
                  style: TextStyle(
                      decoration: TextDecoration.lineThrough,
                      fontSize: 22.sp,
                      fontWeight: FontWeight.w800,
                      color: Colors.green),
                )
              : SizedBox(),
          Text(
            this.widget.product.proTitle,
            style: TextStyle(color: Colors.grey),
          ).paddingTop(8),
          SizedBox(
            height: 18.0,
          ),
          Row(
            children: <Widget>[
              widget.product.isStockAvailable() ? SizedBox(
                width: 8.0,
              ) : SizedBox(),
              widget.product.isStockAvailable() ? Spacer(): SizedBox(),
              widget.product.isStockAvailable() ?
              addToCart('ADD TO CART', () async {
                /*final shouldAdd = await showDialog(
                    context: context,
                    builder: (context) {
                      return ConfirmDialogWidget(
                          "Are you sure you want to add the product to the cart?");
                    });
                if (shouldAdd != null && shouldAdd) */
                addItemToCart();
              }, Colors.green, 0.29) : Text("Out of stock", style: TextStyle(color: Colors.black, fontSize: 18, fontWeight: FontWeight.bold),),
              SizedBox(
                width: 8.0,
              ),
              widget.product.isStockAvailable() ? addToCart('ADD TO WISHLIST', () async {
                /* final shouldAdd = await showDialog(
                    context: context,
                    builder: (context) {
                      return ConfirmDialogWidget(
                          "Are you sure you want to add the product to the wishlist?");
                    });
                if (shouldAdd != null && shouldAdd) */
                addItemToWishlist();
              }, Colors.amber, 0.34) : SizedBox()
            ],
          ).paddingBottom(16),
        ],
      ),
    );
  }

  void addItemToCart() async {
    await progressDialog.show();
    productsService
        .addItemToCart(widget.product.proId, UserRepo.user.usersId,
            quantity: number)
        .then((value) {})
        .whenComplete(() async {
      await progressDialog.hide();
      showDialog(
          context: context,
          builder: (context) {
            Future.delayed(Duration(seconds: 2), () {
              widget.updateCartProvider.fetchCartItems();
              Navigator.of(context).pop(true);
            });
            return AlertDialog(
              title: Center(child: Text('Product added to cart.',style: TextStyle(color: AppTheme.primaryColorDark),)),
            );
          });
    });
  }

  void addItemToWishlist() async {
    await progressDialog.show();
    productsService
        .addToWishlist(widget.product.proId, UserRepo.user.usersId)
        .then((value) {})
        .whenComplete(() async {
      await progressDialog.hide();
      showDialog(
          context: context,
          builder: (context) {
            Future.delayed(Duration(seconds: 2), () {
              Navigator.of(context).pop(true);
            });
            return AlertDialog(
              title: Center(child: Text('Product added to wishlist.',style: TextStyle(color: AppTheme.primaryColorDark),)),
            );
          });
    });
  }

  void add() {
    setState(() {
      number++;
    });
  }

  void minus() {
    setState(() {
      if (number != 1) number--;
    });
  }

  Widget addToCart(text, onTapped, color, widthpercent) => SizedBox(
        width: MediaQuery.of(context).size.width * widthpercent,
        height: 32.h,
        child: RaisedButton(
          //splashColor: Colors.lightBlue,
          onPressed: () {
            onTapped();
            //Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);
          },
          color: color,
          child: FittedBox(
            child: Text(text,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 16.sp,
                    fontWeight: FontWeight.bold)),
          ),
          //disabledColor: Colors.lightBlue,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(24)),
        ),
      ).elevation(24.0, elevation: 8.0);

  @override
  void initState() {
    super.initState();
    progressDialog = P.ProgressDialog(context,
        type: P.ProgressDialogType.Normal,
        isDismissible: false,
        showLogs: true);
    productsService = ProductsService();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    return GestureDetector(
      onTap: () {
        widget.onItemPresssed();
      },
      child: Container(
        constraints: BoxConstraints(minHeight: 200),
        margin: EdgeInsets.only(bottom: 8.0, right: 8.0),
        padding:
            EdgeInsets.only(bottom: 16.0, right: 8.0, left: 8.0, top: 16.0),
        color: Colors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(flex: 3, child: leftColumn()),
            Expanded(flex: 8, child: rightColumn())
          ],
        ),
      ),
    );
  }
}
