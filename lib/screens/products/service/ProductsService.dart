import 'package:dio/dio.dart';
import 'package:ecommerceapp/commons/constants/app_constants.dart';
import 'package:ecommerceapp/model/ProductData.dart';
import 'package:ecommerceapp/network/dio_rest_client.dart';

class ProductsService {
  Future<List<ProductData>> getAllProducts(tableName) async {
    ApiResponse apiResponse = await ApiHitter.getPostApiResponse(
        AppConstants.FETCHALL,
        formData: FormData.fromMap({"table": tableName}));
    try {
      return apiResponse.response != null
          ? productDataFromJson(apiResponse.response.data)
          : null;
    } catch (e) {
      print(e..toString());
      return null;
    }
  }

  Future<List<ProductData>> searchProducts(searchQuery,
      {category, subCat}) async {
    ApiResponse apiResponse = await ApiHitter.getPostApiResponse(
        AppConstants.SEARCH_PRODUCT,
        formData:
            FormData.fromMap({"category": category, "search": searchQuery, "subcategory" : subCat}));
    try {
      return apiResponse.response != null
          ? productDataFromJson(apiResponse.response.data)
          : null;
    } catch (e) {
      print(e..toString());
      return null;
    }
  }

  Future<bool> addToWishlist(pro_id, user_id) async {
    ApiResponse apiResponse = await ApiHitter.getPostApiResponse(
        AppConstants.ADD_TO_WISHLIST,
        formData: FormData.fromMap({"pro_id": pro_id, "user_id": user_id}));
    try {
      return apiResponse.response != null ? true : false;
    } catch (e) {
      print(e..toString());
      return false;
    }
  }

  Future<bool> addItemToCart(proId, userId, {quantity = "1"}) async {
    ApiResponse apiResponse = await ApiHitter.getPostApiResponse(
        AppConstants.ADD_TO_CART,
        formData: FormData.fromMap(
            {"pro_id": proId, "quantity": quantity, "user_id": userId}));
    try {
      return apiResponse.response != null ? true : false;
    } catch (e) {
      print(e..toString());
      return false;
    }
  }
}
