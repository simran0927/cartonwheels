
import 'dart:async';

import 'package:ecommerceapp/app/app_theme.dart';
import 'package:ecommerceapp/commons/repo/UserRepository.dart';
import 'package:ecommerceapp/screens/mainHome/MyHomePage.dart';
import 'package:ecommerceapp/utils/responsive/flutter_screenutil.dart';
import 'package:flutter/cupertino.dart';
import 'package:ecommerceapp/utils/extensions/basic_extensions.dart';
import 'package:ecommerceapp/utils/responsive/size_extension.dart';
import 'package:flutter/material.dart';

class ThankYouScreen extends StatefulWidget{
  @override
  _ThankYouScreenState createState() => _ThankYouScreenState();

}

class _ThankYouScreenState extends State<ThankYouScreen>{

  @override
  void initState() {
    super.initState();
  }

  startTime() async {
    var _duration = new Duration(seconds: 3);
    return new Timer(_duration, navigationToHomePage);
  }

  void navigationToHomePage() {
    Navigator.popUntil(context, ModalRoute.withName(MyHomePage.routeName));
  }


  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
   return WillPopScope(
     onWillPop: () async {
       return false;
     },
     child: Container(
       height: MediaQuery.of(context).size.height,
       width: MediaQuery.of(context).size.width,
       color: AppTheme.white,
       child: Column(
         mainAxisAlignment: MainAxisAlignment.start,
         crossAxisAlignment: CrossAxisAlignment.start,
         mainAxisSize: MainAxisSize.max,
         children: [
           Container(
             height: MediaQuery.of(context).size.height*0.33,
             width: MediaQuery.of(context).size.width,
             decoration: BoxDecoration(color: AppTheme.primaryColorDark),
             child:Center(
               child: Image.asset(
       "assets/images/app_icon.png",
       width: 200.w,
       height: 160.h,
       fit: BoxFit.fitWidth,
               ),
             ),
           ),
           Container(child: Text("Thank You ${UserRepo.user.name}. Your order has been placed successfully.",

             style: TextStyle(fontSize: 24.sp,color: Colors.black,decoration: TextDecoration.none),).paddingAll(top:24.0,left: 16.0,right: 16.0))
         ],
       ),

     ),
   );
  }

}