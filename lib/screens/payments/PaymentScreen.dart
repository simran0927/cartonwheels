import 'dart:async';
import 'dart:convert';

import 'package:ecommerceapp/commons/constants/app_constants.dart';
import 'package:ecommerceapp/commons/repo/UserRepository.dart';
import 'package:ecommerceapp/screens/mainHome/MyHomePage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:io' show Platform;

class PaymentScreen extends StatefulWidget {
  @override
  PaymentScreenState createState() => PaymentScreenState();
}

class PaymentScreenState extends State<PaymentScreen> {
  String orderId;
  var _duration = new Duration(seconds: 2);

  @override
  void initState() {
    super.initState();
    final flutterWebviewPlugin = new FlutterWebviewPlugin();
    flutterWebviewPlugin.onUrlChanged.listen((String url) async {
      print("Url:" + url);

      if(url.contains("api/eway_redirect_wallet_pay")) {
        if(Platform.isIOS) {
          Future.delayed(Duration(seconds: 3), () async {
            Fluttertoast.showToast(
                msg: "Order Successful",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                textColor: Colors.white,
                fontSize: 14.0);
          });
          await Future.delayed(Duration(seconds: 8));
          Navigator.popUntil(context, ModalRoute.withName(MyHomePage.routeName));
        } else {
          Fluttertoast.showToast(
              msg: "Order Successful",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              textColor: Colors.white,
              fontSize: 14.0);
      await Future.delayed(Duration(seconds: 4));
      Navigator.popUntil(context, ModalRoute.withName(MyHomePage.routeName));
        }

      }
      else if (url.contains("api/eway_redirect")) {
        var uri = Uri.parse(url);
        final order = uri.queryParameters['order'];

        if (order != null) {
          if(Platform.isIOS) {
            await Future.delayed(Duration(seconds: 8));
          } else
            await Future.delayed(Duration(seconds: 4));
            orderId = order;
            Navigator.pop(context, orderId);
        } else {
          showMessageAndPop("Sorry, Something went wrong!!");
        }

        /*Navigator.popUntil(context, ModalRoute.withName(MyHomePage.routeName));*/
      } else if (url.contains("api/eway_cancel")) {
        showMessageAndPop("Payment cancelled");
      }
    });
  }

  void showMessageAndPop(String message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 3,
        textColor: Colors.white,
        fontSize: 14.0);
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return WebviewScaffold(
        url: "${AppConstants.baseUrl}${AppConstants.PAYMENT}",
        withZoom: false,
        appCacheEnabled: false,
        clearCookies: true,
        clearCache: true,
        withJavascript: true,
        withLocalStorage: true,
        hidden: true,
        headers: {'payment-data': jsonEncode(UserRepo.paymentModel.toJson())},
      initialChild: Center(child: CircularProgressIndicator(),),
    );
    /*  return WebView(
      initialUrl: "${AppConstants.baseUrl}${AppConstants.PAYMENT}",
      javascriptMode: JavascriptMode.unrestricted,
      onPageStarted: (url) {
        print("Url:" + url);
        if (url.contains("api/eway_redirect_wallet_pay")) {
          Fluttertoast.showToast(
              msg: "Order Successful",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIos: 1,
              textColor: Colors.white,
              fontSize: 14.0);
p
          Navigator.popUntil(
              context, ModalRoute.withName(MyHomePage.routeName));
        } else if (url.contains("api/eway_cancel")) {
          Fluttertoast.showToast(
              msg: "Payment cancelled",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIos: 1,
              textColor: Colors.white,
              fontSize: 14.0);
          Navigator.pop(context);
        }
      },
    );*/
  }


/*  String preparePaymentUrl() {
    final paymentUri = Uri(
        host: "hrhk.in",
        scheme: "http",
        path: "ecommerce/api/" + AppConstants.PAYMENT,
        queryParameters: {
          "fname": UserRepo.user.name,
          "lname": "",
          "addr1": UserRepo.user.address1,
          "postcode": "3000",
          "delivary_date": "Tuesday,03-11-2020",
          "email": UserRepo.user.email,
          "phone": UserRepo.user.phone,
          "odr_total": "1",
          "pay_type": "wallet",
          "service_fee": "0",
          "pay_from_wallet": "1",
          "full_total": "1",
          "odrnote": "asdas",
          "did_not_find": "asdasds",
          "login_type": "app",
          "user_id": UserRepo.user.usersId
        });
    return paymentUri.toString();
  }*/
}
