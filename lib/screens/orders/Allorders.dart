
import 'package:ecommerceapp/app/app_theme.dart';
import 'package:ecommerceapp/base/base_state.dart';
import 'package:ecommerceapp/base/base_stateful_widget.dart';
import 'package:ecommerceapp/base/base_statefull_screen.dart';
import 'package:ecommerceapp/commons/repo/UserRepository.dart';
import 'package:ecommerceapp/model/OrderHistory.dart';
import 'package:ecommerceapp/screens/orders/OrderItem.dart';
import 'package:ecommerceapp/screens/orders/service/OrderService.dart';
import 'package:ecommerceapp/widgets/ProgressDialogType.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/framework.dart';

class AllOrders extends BaseStatefulWidget{
  static const routeName = '/all_orders';
  @override
  _AllOrdersState createState() => _AllOrdersState();

}

class _AllOrdersState extends BaseState<AllOrders> with BaseStatefulScreen{

  _AllOrdersState():super(defaultPadding:false);

  ProgressDialog progressDialog;
  OrderService orderService;
  List<OrderHistory> ordersList;

  @override
  void initState() {
    progressDialog = ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: true);
    super.initState();
    orderService = OrderService();
    getAllOrders();
  }

  @override
  Widget buildBody(BuildContext context) {

    return Container(

      child: ordersList!=null && ordersList.length>0?ListView.builder(
          itemCount: ordersList.length,
          itemBuilder: (context,position){
            return OrderItem(ordersList[position]);
          }):Center(child: Text("You have not placed any order."),),
    );
  }

  @override
  String screenName() =>"Order History";


  void getAllOrders() async {
    await progressDialog.show();
    orderService.seeAllOrders(UserRepo.user.usersId).then((value) {
      if (value != null && value.length > 0) {
        setState(() {
          ordersList = value.reversed.toList();
        });
      }
    }).whenComplete(() async {
      await progressDialog.hide();
    });
  }


}



/*class AllOrdersIndependent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
          itemCount: 8,
          itemBuilder: (context,position){
            return OrderItem();
          }),
    );
  }
}*/
