import 'package:dio/dio.dart';
import 'package:ecommerceapp/commons/constants/app_constants.dart';
import 'package:ecommerceapp/model/LoginData.dart';
import 'package:ecommerceapp/model/OrderHistory.dart';
import 'package:ecommerceapp/model/common_response.dart';
import 'package:ecommerceapp/network/dio_rest_client.dart';

class OrderService{
  Future<List<OrderHistory>> seeAllOrders(userId) async {
    ApiResponse apiResponse =
    await ApiHitter.getPostApiResponse(AppConstants.ORDER_HISTORY, formData: FormData.fromMap({
      "user_id":userId
    }));
    try {
      return apiResponse.response != null
          ? orderHistoryFromJson(apiResponse.response.data)
          : null;
    } catch (e) {
      print(e..toString());
      return null;
    }

  }
}