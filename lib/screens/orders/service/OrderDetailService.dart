import 'package:dio/dio.dart';
import 'package:ecommerceapp/commons/constants/app_constants.dart';
import 'package:ecommerceapp/model/LoginData.dart';
import 'package:ecommerceapp/model/OrderDetail.dart';
import 'package:ecommerceapp/model/OrderHistory.dart';
import 'package:ecommerceapp/model/common_response.dart';
import 'package:ecommerceapp/network/dio_rest_client.dart';

class OrderDetailService{
  Future<List<OrderDetail>> seeOrderDetails(orderId) async {
    ApiResponse apiResponse =
    await ApiHitter.getPostApiResponse(AppConstants.ORDER_DETAIL, formData: FormData.fromMap({
      "orderid":orderId
    }));
    try {
      return apiResponse.response != null
          ? orderDetailFromJson(apiResponse.response.data)
          : null;
    } catch (e) {
      print(e..toString());
      return null;
    }

  }
}