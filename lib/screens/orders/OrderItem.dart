import 'package:ecommerceapp/app/app_theme.dart';
import 'package:ecommerceapp/model/OrderHistory.dart';
import 'package:ecommerceapp/utils/app_utils.dart';
import 'package:ecommerceapp/utils/responsive/flutter_screenutil.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ecommerceapp/utils/extensions/basic_extensions.dart';
import 'package:ecommerceapp/utils/responsive/size_extension.dart';
import 'package:ecommerceapp/utils/responsive/screenutil.dart';
import 'package:intl/intl.dart';

class OrderItem extends StatefulWidget {

   OrderHistory order;
   OrderItem(this.order);
  @override
  _OrderItemState createState() => _OrderItemState();
}

class _OrderItemState extends State<OrderItem> {

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
   return Material(
     child: Container(
       padding: EdgeInsets.only(top: 24.h),
       color: AppTheme.primaryColorLight,
       child: Container(

         color: Colors.grey,
         child: Column(
           crossAxisAlignment: CrossAxisAlignment.center,
           mainAxisAlignment: MainAxisAlignment.start,
           mainAxisSize: MainAxisSize.max,
           children: <Widget>[
             Text("Placed on ${(this.widget.order.ordDate).toString().split(" ")[0]} ${DateFormat('hh:mm:ss').format(this.widget.order.ordDate)}",style: TextStyle(
               fontSize: 20.sp,
               fontWeight: FontWeight.w400,
               color: Colors.black38,
               decoration: TextDecoration.none,
             ),).paddingAll(top: 12.h,bottom: 8.0.h),
             Container(
               color: Colors.white,
               width: MediaQuery.of(context).size.width,
               child: Column(
                 crossAxisAlignment: CrossAxisAlignment.center,
                 mainAxisSize: MainAxisSize.max,
                 mainAxisAlignment: MainAxisAlignment.start,
                 children: <Widget>[
                   Text("Scheduled for ${this.widget.order.delivaryDate}",
                   style: TextStyle(
                     fontSize: 20.sp,
                     color: Colors.black,
                     fontWeight: FontWeight.w400,
                     decoration: TextDecoration.none,
                   ),).paddingAll(top:12.h,bottom:8.0.h),
                   Divider(height: 1,thickness: 1,color: Colors.grey,),
                   Row(
                     children: <Widget>[
                       Text("Delivered by Cart On Wheels.",style: TextStyle(
                         fontSize: 16.sp,
                         color: Colors.black38,
                         decoration: TextDecoration.none,
                       ),).paddingAll(top:12.h,left: 24.w)
                     ],
                   ),
                   Row(
                     children: <Widget>[
                       Text(this.widget.order.address,style: TextStyle(
                         fontSize: 20.sp,
                         color: Colors.black38,
                         decoration: TextDecoration.none,
                       ),).paddingAll(top:4.h,left: 24.w,bottom: 12.h)
                     ],
                   ),
                   Row(
                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                     mainAxisSize: MainAxisSize.max,
                     crossAxisAlignment: CrossAxisAlignment.start,
                     children: <Widget>[
                       Row(

                         mainAxisAlignment: MainAxisAlignment.start,
                         mainAxisSize: MainAxisSize.max,
                         crossAxisAlignment: CrossAxisAlignment.start,
                         children: <Widget>[
                       Container(
                         padding:EdgeInsets.all(6.0),
                         decoration: BoxDecoration(
                           shape: BoxShape.rectangle,
                           borderRadius: BorderRadius.all(Radius.circular(8.0)),
                           color: Colors.orangeAccent
                         ),
                         child: Icon(Icons.account_balance,
                         size: 25,
                          color: Colors.brown,
                         ),
                       ).paddingRight(16.0),
                         Column(
                           mainAxisAlignment: MainAxisAlignment.start,
                           crossAxisAlignment: CrossAxisAlignment.start,
                           mainAxisSize: MainAxisSize.max,
                           children: <Widget>[
                             Text("Order Id:"),
                             Text("Transaction Id:"),



                           ],
                         )
                       ],),
                       Column(
                         mainAxisAlignment: MainAxisAlignment.start,
                         crossAxisAlignment: CrossAxisAlignment.end,
                         children: <Widget>[
                           Text(" ${this.widget.order.odrId}"),
                           Text(this.widget.order.transactionId,textAlign: TextAlign.end,),
                         ],
                       )
                     ],
                   ).paddingAll(top:16.h,left: 16.h,right: 16.h,bottom:16.h),

                   Divider(height: 1,thickness: 1,color: Colors.grey,),
                   Row(
                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                     mainAxisSize: MainAxisSize.max,
                     crossAxisAlignment: CrossAxisAlignment.center,
                     children: <Widget>[
                       Text("Final paid amount"),
                       Text("\$ ${this.widget.order.odrTotal}")
                     ],
                   ).paddingAll(top: 16, left: 16,right: 16,bottom: 16),
                   InkWell(
                     onTap: (){},
                     child: Container(
                       width: MediaQuery.of(context).size.width,
                       padding: EdgeInsets.all(12.0),
                       decoration: BoxDecoration(
                         color: Colors.orange,
                         borderRadius: BorderRadius.all(Radius.circular(8.0))
                       ),
                       child: Text(this.widget.order.ordStatus,
                         textAlign: TextAlign.center,
                         style: TextStyle(
                         color: Colors.white
                       ),),
                     ),
                   ).paddingAll(bottom: 16,left:16,right:16)
                 ],
               ),
             )
           ],
         ),
       ),
     ),
   );
  }
}
