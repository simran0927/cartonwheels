import 'package:ecommerceapp/app/app_theme.dart';
import 'package:ecommerceapp/base/base_state.dart';
import 'package:ecommerceapp/base/base_stateful_widget.dart';
import 'package:ecommerceapp/base/base_statefull_screen.dart';
import 'package:ecommerceapp/commons/repo/UserRepository.dart';
import 'package:ecommerceapp/model/OrderDetail.dart';
import 'package:ecommerceapp/model/OrderHistory.dart';
import 'package:ecommerceapp/screens/list/service/ListService.dart';
import 'package:ecommerceapp/screens/orders/OrderProductItem.dart';
import 'package:ecommerceapp/screens/orders/service/OrderDetailService.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';

class OrderDetailScreen extends BaseStatefulWidget{
  static const routeName = '/order_detail';
  @override
  _OrderDetailScreenState createState()=>_OrderDetailScreenState();

}

class _OrderDetailScreenState extends BaseState<OrderDetailScreen> with BaseStatefulScreen{
  OrderHistory orderHistory;
  OrderDetailService orderDetailService;
  List<OrderDetail> orderDetailList;
  ListService listService;


  @override
  void initState() {
    orderDetailService=OrderDetailService();
    listService = ListService();
    Future.delayed(Duration.zero,(){
      setState(() {
        orderHistory = ModalRoute.of(context).settings.arguments as OrderHistory;
        if(orderHistory!=null){getOrderDetail();}
      });
    });
  }

  @override
  String screenName() =>"Order Detail";

  void getOrderDetail() async {
    orderDetailService.seeOrderDetails(orderHistory.odrId).then((value) {
      if (value != null && value.length > 0) {
         setState(() {
          orderDetailList = value;
        });
      }
    }).whenComplete(() async {

    });
  }




  @override
  Widget buildBody(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: orderDetailList!=null && orderDetailList.length>0?
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 8,),
            Text(" ${DateFormat(DateFormat.WEEKDAY).format(orderHistory.ordDate)} ,  ${DateFormat("dd-MM-yyyy").format(orderHistory.ordDate)}",
                style: TextStyle(
                    fontSize: 20,
                    color: AppTheme.primaryColorDark,
                    fontWeight: FontWeight.w600)),
            SizedBox(height: 15,),
            ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
                itemCount: orderDetailList.length,
                itemBuilder: (context,pos){
              return OrderProductItem((number){
                addItemToCart(orderDetailList[pos].proId,number);
              },orderDetailList[pos]);
            })
          ],
        ):Center(child: Text("No Items found for this order"),),
      ),
    );
  }

  void addItemToCart(proId,number) async {

    listService
        .addItemToCart(proId, UserRepo.user.usersId, quantity: number)
        .then((value) {})
        .whenComplete(() async {
      showDialog(
          context: context,
          builder: (context) {
            Future.delayed(Duration(seconds: 2), () {
              Navigator.of(context).pop(true);
            });
            return AlertDialog(
              title: Center(child: Text('Product added to cart.',style: TextStyle(color: AppTheme.primaryColorDark),)),
            );
          });

    });
  }

}