import 'package:cached_network_image/cached_network_image.dart';
import 'package:ecommerceapp/commons/constants/app_constants.dart';
import 'package:ecommerceapp/model/OrderDetail.dart';
import 'package:ecommerceapp/model/ProductData.dart';
import 'package:ecommerceapp/screens/details/product_details.dart';
import 'package:ecommerceapp/utils/extensions/basic_extensions.dart';
import 'package:ecommerceapp/utils/responsive/flutter_screenutil.dart';
import 'package:ecommerceapp/utils/responsive/size_extension.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';



class OrderProductItem extends StatefulWidget {

  Function(String) onAddToCartClicked;
  final OrderDetail product;

  OrderProductItem( this.onAddToCartClicked, this.product);

  @override
  _OrderProductItemState createState() => _OrderProductItemState();
}

class _OrderProductItemState extends State<OrderProductItem> {
  int number ;

  Widget leftColumn() {
    return CachedNetworkImage(
      imageUrl: AppConstants.IMAGEURL + this.widget.product.photo,

    );
  }

  Widget rightColumn() {
    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          widget.product.proName,
          maxLines: 2,
          overflow: TextOverflow.ellipsis,
        ).paddingRight(16),
        Text(
          "\$ ${this.widget.product.proAfterprice != null &&
              this.widget.product.proAfterprice.isNotEmpty &&
              this.widget.product.proPrice !=
                  this.widget.product.proAfterprice
              ? this.widget.product.proAfterprice
              : this.widget.product.proPrice}",
          style: TextStyle(fontSize: 28.sp, fontWeight: FontWeight.w800),
        ).paddingTop(8.0),
        this.widget.product.proAfterprice != null &&
            this.widget.product.proAfterprice.isNotEmpty &&
            this.widget.product.proPrice !=
                this.widget.product.proAfterprice
            ? Text(
          "\$ " + this.widget.product.proPrice,
          style: TextStyle(
              decoration: TextDecoration.lineThrough,
              fontSize: 22.sp,
              fontWeight: FontWeight.w800,
              color: Colors.green),
        ) : SizedBox(),
        Text(
          this.widget.product.proTitle,
          style: TextStyle(color: Colors.grey),
        ).paddingTop(8),
        SizedBox(
          height: 16.0,
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[

            Row(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                new InkWell(
                    onTap: minus,
                    child: Container(
                      padding: EdgeInsets.all(4.0),
                      decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          color: Colors.amber,
                          borderRadius: BorderRadius.all(Radius.circular(8.0))),
                      child: Icon(
                        Icons.remove,
                        color: Colors.white,
                        size: 12,
                      ),
                    )
                ),
                SizedBox(
                  width: 6.0,
                ),
                new Text('$number', style: new TextStyle(fontSize: 18.0)),
                SizedBox(
                  width: 6.0,
                ),
                InkWell(
                    onTap: add,
                    child: Container(
                      padding: EdgeInsets.all(4.0),
                      decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          color: Colors.amber,
                          borderRadius: BorderRadius.all(Radius.circular(8.0))),
                      child: Icon(
                        Icons.add,
                        color: Colors.white,
                        size: 12,
                      ),
                    )),
              ]
            ),
            Spacer(),
            addToCart('ADD TO CART', widget.onAddToCartClicked)
          ],
        ),
      ],
    );
  }

  void add() {
    setState(() {
      number++;
    });
  }

  void minus() {
    setState(() {
      if (number != 1) number--;
    });
  }

  Widget addToCart(text, onTapped) => SizedBox(
    width: 130,
    height: 32.h,
    child: RaisedButton(
      //splashColor: Colors.lightBlue,
      onPressed: () {
        onTapped(number.toString());
        //Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);
      },
      color: Colors.green,
      child: FittedBox(
        child: Text(text,
            style: TextStyle(
                color: Colors.white,
                fontSize: 20.sp,
                fontWeight: FontWeight.bold)),
      ),
      //disabledColor: Colors.lightBlue,
      shape:
      RoundedRectangleBorder(borderRadius: BorderRadius.circular(24)),
    ),
  ).elevation(24.0, elevation: 3.0);


  @override
  void initState() {
    super.initState();
    number =int.parse(this.widget.product.proQuty);
  }
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    return Stack(
      children: <Widget>[
        Container(
          color: Colors.white,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(flex: 3, child: leftColumn()),
              Expanded(flex: 7, child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: rightColumn(),
              ))
            ],
          ),
        ),

      ],
    ).paddingTop(4);
  }
}
