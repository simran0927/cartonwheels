import 'package:ecommerceapp/app/app_theme.dart';
import 'package:ecommerceapp/model/ProductData.dart';
import 'package:ecommerceapp/screens/cart/provider/UpdateCartProvider.dart';
import 'package:ecommerceapp/screens/details/product_details.dart';
import 'package:ecommerceapp/screens/list/service/ListService.dart';
import 'package:ecommerceapp/screens/products/product_item.dart';
import 'package:ecommerceapp/screens/products/service/ProductsService.dart';
import 'package:ecommerceapp/screens/search/search_product_list.dart';
import 'package:ecommerceapp/utils/extensions/basic_extensions.dart';
import 'package:ecommerceapp/widgets/ProgressDialogType.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';

class SearchScreen extends StatefulWidget {
  final String listId;
  final String catId;

  SearchScreen({this.listId, this.catId});

  @override
  SearchScreenState createState() => SearchScreenState();
}

class SearchScreenState extends State<SearchScreen> {
  ProductsService productsService;
  ListService listService;
  ProgressDialog progressDialog;
  List<ProductData> productList;
  TextEditingController searchController;

  @override
  void initState() {
    super.initState();
    progressDialog = ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: true);
    productsService = ProductsService();
    listService = ListService();
    //getAllProducts();
  }

  void getAllProducts() async {
    await progressDialog.show();
    productsService.getAllProducts("products").then((value) {
      if (value != null && value.length > 0) {
        setState(() {
          productList = value;
        });
      }
    }).whenComplete(() async {
      await progressDialog.hide();
    });
  }

  void getSearchProduct(searchQuery) async {
    productsService.searchProducts(searchQuery, category: widget.catId).then((value) {
      if (value != null && value.length > 0) {
        setState(() {
          productList = value;
        });
      }
    });
  }

  Widget build(BuildContext context) {
    final updateCartProvider = Provider.of<UpdateCartProvider>(context);
    return Scaffold(
      backgroundColor: AppTheme.primaryColorLight,
      appBar: AppBar(
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        centerTitle: true,
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
        ),
        title: TextField(
          controller: searchController,
          autofocus: true,
          decoration: const InputDecoration(
            hintText: 'Search Products...',
            border: InputBorder.none,
            hintStyle: const TextStyle(color: Colors.black),
          ),
          style: const TextStyle(color: Colors.black, fontSize: 16.0),
          onChanged: (value) {
            if(value.isNotEmpty)
            getSearchProduct(value);
          },
        ),
      ),
      body: buildBody(context,updateCartProvider),
    );
  }

  Widget buildBody(BuildContext context,UpdateCartProvider updateCartProvider) {
    return Column(
      children: <Widget>[
        Expanded(
          child: Column(
            children: <Widget>[
              topFilterView(context),
              Expanded(
                child: productList != null && productList.length > 0
                    ? ListView.builder(
                        //physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        scrollDirection: Axis.vertical,
                        itemCount: productList.length,
                        itemBuilder: (context, pos) {
                          return widget.listId!=null?SearchProductItem(() {
                            addToList(productList[pos], widget.listId);
                          }, productList[pos]):
                          ProductItem(
                                  (){
                                Navigator.of(context).pushNamed(ProductDetailScreen.routeName,arguments: productList[pos]);
                              },productList[pos],updateCartProvider
                          );
                        })
                    : Center(
                        child: Text("No Products Found."),
                      ),
              )
            ],
          ),
        ),
      ],
    );
  }

  @override
  List<Widget> setAppbarWidgets() {
    return <Widget>[
      IconButton(
        icon: Icon(Icons.search),
        onPressed: () {},
      ),
      Icon(Icons.add_shopping_cart).paddingAll(left: 16.0, right: 16.0)
    ];
  }

  Widget topFilterView(context) {
    return Container(
      padding: EdgeInsets.only(bottom: 0.0),
      child: Column(
        children: <Widget>[
          SizedBox(height: 8),
          Container(
            padding:
                EdgeInsets.only(left: 16.0, right: 16.0, top: 8.0, bottom: 8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text("${productList?.length ?? 0} results"),
                /* InkWell(
                  onTap: () {
                    return showModalBottomSheet(
                        context: context,
                        builder: (context) {
                          return FilterSheet();
                        });
                  },
                  child: Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Icon(
                            Icons.filter_list,
                            color: AppTheme.primaryColorDark,
                            size: 25,
                          ),
                          Text(
                            "SORT BY",
                            style: TextStyle(color: AppTheme.primaryColorDark),
                          )
                        ],
                      ),
                      Text("Relevance")
                    ],
                  ),
                )*/
              ],
            ),
          ),
          Divider(thickness: 0.6, color: AppTheme.primaryColorDark)
        ],
      ),
    );
  }

  void addToList(ProductData productList, String listId) async {
    await progressDialog.show();
    listService
        .addProductToList(listId, productList.proId)
        .then((value) {})
        .whenComplete(() async {
      await progressDialog.hide();
      Fluttertoast.showToast(
          msg: "Product added to list",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          textColor: Colors.white,
          fontSize: 14.0);
      Navigator.pop(context);
    });
  }
}
