import 'package:cached_network_image/cached_network_image.dart';
import 'package:ecommerceapp/commons/constants/app_constants.dart';
import 'package:ecommerceapp/model/ProductData.dart';
import 'package:ecommerceapp/screens/details/product_details.dart';
import 'package:ecommerceapp/utils/extensions/basic_extensions.dart';
import 'package:ecommerceapp/utils/responsive/flutter_screenutil.dart';
import 'package:ecommerceapp/utils/responsive/size_extension.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class SearchProductItem extends StatefulWidget {
  final VoidCallback onAddToListClicked;
  final ProductData product;

  SearchProductItem(this.onAddToListClicked, this.product);

  @override
  _SearchProductItemState createState() => _SearchProductItemState();
}

class _SearchProductItemState extends State<SearchProductItem> {
  int number = 0;

  Widget leftColumn() {
    return CachedNetworkImage(
      imageUrl: AppConstants.IMAGEURL+this.widget.product.photo,
      
    );
  }

  Widget rightColumn() {
    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          widget.product.proName,
          maxLines: 2,
          overflow: TextOverflow.ellipsis,
        ).paddingRight(16),
        Text( "\$ ${this.widget.product.proAfterprice != null &&
            this.widget.product.proAfterprice.isNotEmpty &&
            this.widget.product.proPrice !=
                this.widget.product.proAfterprice
            ? this.widget.product.proAfterprice
            : this.widget.product.proPrice}",style: TextStyle(fontSize: 28.sp,fontWeight: FontWeight.w800),).paddingTop(8.0),
        this.widget.product.proAfterprice != null &&
            this.widget.product.proAfterprice.isNotEmpty &&
            this.widget.product.proPrice !=
                this.widget.product.proAfterprice
            ? Text("\$ "+this.widget.product.proPrice,style: TextStyle(
            decoration: TextDecoration.lineThrough,
            fontSize: 22.sp,fontWeight: FontWeight.w800,
            color: Colors.green
        ),) : SizedBox() ,
        Text(this.widget.product.proTitle,style: TextStyle(color: Colors.grey),).paddingTop(8),
        SizedBox(
          height: 30.0,
        ),
        widget.product.isStockAvailable() ? Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Spacer(),
            addToCart('ADD TO LIST', widget.onAddToListClicked)
          ],
        ): Row(
          children: [
            Spacer(),
            Text("Out of stock", style: TextStyle(color: Colors.black54),),
          ],
        ),
      ],
    );
  }

  Widget addToCart(text, onTapped) => SizedBox(
        width: MediaQuery.of(context).size.width * 0.32,
        height: 32.h,
        child: RaisedButton(
          //splashColor: Colors.lightBlue,
          onPressed: () {
            onTapped();
            //Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);
          },
          color: Colors.green,
          child: Text(text,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 20.sp,
                  fontWeight: FontWeight.bold)),
          //disabledColor: Colors.lightBlue,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(24)),
        ),
      ).elevation(24.0, elevation: 8.0);

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    return GestureDetector(
      onTap: () {
        Navigator.of(context).pushNamed(ProductDetailScreen.routeName);
      },
      child: Stack(
        children: <Widget>[
          Container(
            color: Colors.white,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(flex: 3, child: leftColumn()),
                Expanded(flex: 7, child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: rightColumn(),
                ))
              ],
            ),
          ),
        ],
      ),
    ).paddingTop(4);
  }
}
