
import 'package:ecommerceapp/base/base_state.dart';
import 'package:ecommerceapp/base/base_stateful_widget.dart';
import 'package:ecommerceapp/base/base_statefull_screen.dart';
import 'package:ecommerceapp/model/SubCategoryData.dart';
import 'package:ecommerceapp/screens/products/Products.dart';
import 'package:ecommerceapp/screens/sub_category/service/SubCategoryService.dart';
import 'package:ecommerceapp/widgets/ProgressDialogType.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:ecommerceapp/utils/extensions/basic_extensions.dart';

class SubCategoryPage extends BaseStatefulWidget{
  static const routeName = '/subCategory_items';
  @override
  _SubCategoryPageState createState() =>_SubCategoryPageState();

}

class _SubCategoryPageState extends BaseState<SubCategoryPage> with BaseStatefulScreen{

  ProgressDialog progressDialog;
  SubCategoryService subCategoryService;
  List<SubCategoryData> subCategoryList;
  String catid;


  @override
  String screenName() => "Sub Category";

  @override
  void initState() {
    progressDialog = ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: true);
    super.initState();
    Future.delayed(Duration.zero,(){

        catid = ModalRoute.of(context).settings.arguments as String;
        getAllSubCategories();
    });

    subCategoryService = SubCategoryService();

  }

  void getAllSubCategories() async{
    await progressDialog.show();
    subCategoryService.getAllSubCategoriesOfCat("subcategory", catid).then((value) {
      if (value != null &&
          value.length>0) {
        setState(() {
          subCategoryList=value;
        });
      }
    }).whenComplete(() async {
      await progressDialog.hide();
    });
  }

  @override
  Widget buildBody(BuildContext context) {
    progressDialog = ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: true);
   return subCategoryList!=null && subCategoryList.length>0?Container(
     child: ListView.builder(
         itemCount: subCategoryList.length,
         itemBuilder:(context,pos){
  return subCategoryItem(subCategoryList[pos]);
     }),
   ):Container(child: Center(child:Text("No values found."),));
  }

  Widget subCategoryItem(SubCategoryData subCategoryItem){
    return InkWell(
      onTap: (){
        Navigator.of(context).pushNamed(Products.routeName, arguments: ProductParam(subCategoryItem.catId, subCategoryItem.subcatId));
      },
      child: Container(
        height: 70,
        padding: EdgeInsets.all(16),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(subCategoryItem.subcatName, style: TextStyle(fontWeight: FontWeight.w600,color: Colors.grey),).paddingBottom(16.0),
          Divider(thickness: 1,height: 1,color: Colors.grey,)
          ],
        ),
      ),
    );
  }
}