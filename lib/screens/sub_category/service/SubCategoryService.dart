import 'package:dio/dio.dart';
import 'package:ecommerceapp/commons/constants/app_constants.dart';
import 'package:ecommerceapp/model/ProductData.dart';
import 'package:ecommerceapp/model/SubCategoryData.dart';

import 'package:ecommerceapp/model/common_response.dart';
import 'package:ecommerceapp/network/dio_rest_client.dart';

class SubCategoryService{
  Future<List<SubCategoryData>> getAllSubCategories(tableName) async {
    ApiResponse apiResponse =
    await ApiHitter.getPostApiResponse(AppConstants.FETCHALL, formData: FormData.fromMap({
      "table": tableName
    }));
    try {
      return apiResponse.response != null
          ?subCategoryDataFromJson(apiResponse.response.data)
          : "Null List Found.";
    } catch (e) {
      print(e..toString());
      return null;
    }
  }


  Future<List<SubCategoryData>> getAllSubCategoriesOfCat(tableName, catId) async {
    ApiResponse apiResponse =
    await ApiHitter.getPostApiResponse(AppConstants.SUBCATFORCAT, formData: FormData.fromMap({
      "table": tableName,
      "cat_id": catId
    }));
    try {
      return apiResponse.response != null
          ?subCategoryDataFromJson(apiResponse.response.data)
          : "Null List Found.";
    } catch (e) {
      print(e..toString());
      return null;
    }
  }
}