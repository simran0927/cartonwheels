
import 'package:ecommerceapp/commons/repo/UserRepository.dart';
import 'package:ecommerceapp/model/CartResult.dart';
import 'package:ecommerceapp/screens/cart/service/CartService.dart';
import 'package:flutter/cupertino.dart';

class UpdateCartProvider extends ChangeNotifier{
  List<CartResult> _cartItems;

  List<CartResult> get items {
    // if (_showFavoritesOnly) {
    //   return _items.where((prodItem) => prodItem.isFavorite).toList();
    // }
    return _cartItems;
  }

  Future<void> fetchCartItems() async{
    await CartService().getAllCartItems(UserRepo.user.usersId).then((result) {
      _cartItems =result as List<CartResult>;
      notifyListeners();
    });
  }
}