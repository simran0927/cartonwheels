import 'package:dio/dio.dart';
import 'package:ecommerceapp/commons/constants/app_constants.dart';
import 'package:ecommerceapp/model/CartResult.dart';
import 'package:ecommerceapp/model/address_model.dart';
import 'package:ecommerceapp/model/deleverydate_model.dart';
import 'package:ecommerceapp/network/dio_rest_client.dart';

class CartService {
  Future<List<CartResult>> getAllCartItems(userId) async {
    ApiResponse apiResponse = await ApiHitter.getPostApiResponse(
        AppConstants.CART,
        formData: FormData.fromMap({"user_id": userId}));
    try {
      return apiResponse.response != null
          ? cartResultFromJson(apiResponse.response.data)
          : null;
    } catch (e) {
      print(e..toString());
      return null;
    }
  }

  Future<bool> removeFromCart(cartId) async {
    ApiResponse apiResponse = await ApiHitter.getPostApiResponse(
        AppConstants.CART_REMOVE,
        formData: FormData.fromMap({"cartid": cartId}));
    try {
      return apiResponse.response != null ? true : false;
    } catch (e) {
      print(e..toString());
      return false;
    }
  }

  Future<bool> updateCart(cartId, quantity) async {
    ApiResponse apiResponse = await ApiHitter.getPostApiResponse(
        AppConstants.UPDATE_CART,
        formData: FormData.fromMap({"cart_id": cartId, "quantity": quantity}));
    try {
      return apiResponse.response != null ? true : false;
    } catch (e) {
      print(e..toString());
      return false;
    }
  }

  Future<bool> saveAddress(userId, address) async {
    ApiResponse apiResponse = await ApiHitter.getPostApiResponse(
        AppConstants.SAVE_ADDRESS,
        formData: FormData.fromMap(
            {"adr_user_id": userId, "addr": address, "status": "Active"}),
        isDev: true);
    try {
      return apiResponse.response != null ? true : false;
    } catch (e) {
      print(e..toString());
      return false;
    }
  }

  Future<bool> deleteAddress(userId, addressId) async {
    ApiResponse apiResponse = await ApiHitter.getPostApiResponse(
        AppConstants.DELETE_ADDRESS,
        formData: FormData.fromMap({"deladr": addressId, "user_id": userId}),
        isDev: true);
    try {
      return apiResponse.response != null ? true : false;
    } catch (e) {
      print(e..toString());
      return false;
    }
  }

  Future<List<AddressModel>> getAddress(userId) async {
    ApiResponse apiResponse = await ApiHitter.getPostApiResponse(
        AppConstants.ADDRESS,
        formData: FormData.fromMap({"user_id": userId}),
        isDev: true);
    try {
      return apiResponse.response != null
          ? addressModelFromJson(apiResponse.response.data)
          : null;
    } catch (e) {
      print(e..toString());
      return null;
    }
  }

  Future<List<DeleveryDateModel>> getDeleveryDatesFromPostCode(postCode) async {
    ApiResponse apiResponse = await ApiHitter.getPostApiResponse(
        AppConstants.DELIVERY_DATE,
        formData: FormData.fromMap({"postcode": postCode}),
        isDev: true);
    try {
      return apiResponse.response != null
          ? deleveryDateModelFromJson(apiResponse.response.data)
          : null;
    } catch (e) {
      print(e..toString());
      return null;
    }
  }
}
