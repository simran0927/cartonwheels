import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:ecommerceapp/app/app_theme.dart';
import 'package:ecommerceapp/commons/constants/app_constants.dart';
import 'package:ecommerceapp/constants/values.dart';
import 'package:ecommerceapp/model/CartResult.dart';
import 'package:ecommerceapp/screens/cart/service/CartService.dart';
import 'package:ecommerceapp/utils/extensions/number_parse.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';

import 'FinalCheckout.dart';

class CheckOutPage extends StatefulWidget {
  final List<CartResult> cart;

  CheckOutPage(this.cart);

  _CheckOutPageState createState() => _CheckOutPageState();
}

class _CheckOutPageState extends State<CheckOutPage>
    with SingleTickerProviderStateMixin {
  var now = DateTime.now();

  get weekDay => DateFormat('EEEE').format(now);

  get day => DateFormat('dd').format(now);

  get month => DateFormat('MMMM').format(now);
  double oldTotal = 0;
  double total = 0;
  double oldSubtotal = 0;
  double subtotal = 0;
  double oldServiceFee = 0;
  double serviceFee = 0;
  double totalWithServiceFee = 0;
  CartService cartService;

  ScrollController scrollController = ScrollController();
  AnimationController animationController;

  onCheckOutClick() async {
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => FinalCheckOut(widget.cart,subtotal,serviceFee,total),
        ));
    /* try {
      List<Map> data = List.generate(cart.cartItems.length, (index) {
        return {"id": cart.cartItems[index].food.id, "quantity": cart.cartItems[index].quantity};
      }).toList();

      var response = await Dio().post('/api/order/food', queryParameters: {"token": "token"}, data: data);
      print(response.data);

      if (response.data['status'] == 1) {
        cart.clearCart();
        Navigator.of(context).pop();
      } else {
        //Toast.show(response.data['message'], context);
      }
    } catch (ex) {
      print(ex.toString());
    }*/
  }

  @override
  void initState() {
    animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 200))
          ..forward();
    cartService = CartService();
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('CheckOut'), centerTitle: true),
      body: FutureBuilder<List<CartResult>>(
          future: null,
          builder: (context, snapshot) {
            return SingleChildScrollView(
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    ...buildHeader(),
                    //cart items list
                    widget.cart.length != 0
                        ? ListView.builder(
                            itemCount: widget.cart.length,
                            shrinkWrap: true,
                            controller: scrollController,
                            itemBuilder: (BuildContext context, int index) {
                              return buildCartItemList(widget.cart[index]);
                            },
                          )
                        : Center(
                            child:
                                Text("Please add items in cart to continue."),
                          ),
                    SizedBox(height: 16),
                    Text(
                      "Cart Total",
                      style: TextStyle(
                          color: Colors.green,
                          fontSize: 22,
                          fontWeight: FontWeight.bold),
                      textAlign: TextAlign.left,
                    ),

                    Divider(),
                    buildPriceInfo(widget.cart),
                    checkoutButton(context),
                  ],
                ),
              ),
            );
          }),
    );
  }

  List<Widget> buildHeader() {
    return [
      Padding(
        padding: const EdgeInsets.only(top: 24.0, bottom: 32),
        child: Text('$weekDay, ${day}th of $month ', style: headerStyle),
      ),
    ];
  }

  void removeFromCart(cartId) async {
    cartService.removeFromCart(cartId).then((value) {});
  }

  void updateCart(cartId, quantity) async {
    cartService.updateCart(cartId, quantity).then((value) {});
  }

  Widget buildPriceInfo(List<CartResult> cart) {
    oldTotal = total;
    total = 0;
    oldSubtotal = subtotal;
    subtotal = 0;
    oldServiceFee = serviceFee;
    serviceFee = 0;
    for (CartResult cartModel in cart) {
      total +=
          (cartModel.proAfterprice != null && cartModel.proAfterprice.isNotEmpty
                  ? cartModel.proAfterprice.parseDouble()
                  : cartModel.proPrice.parseDouble()) *
              cartModel.quantity.parseInt();
      subtotal = total;
      serviceFee = total > 39 ? 0.00 : 5.00;
    }

    //oldTotal = total;
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text('Subtotal', style: subtitleStyle),
            AnimatedBuilder(
              animation: animationController,
              builder: (context, child) {
                return Text(
                    '\$ ${lerpDouble(oldSubtotal, subtotal, animationController.value).toStringAsFixed(2)}',
                    style: subtitleStyle);
              },
            ),
          ],
        ),
        SizedBox(
          height: 4,
        ),
        /*Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text('GST', style: subtitleStyle),
            AnimatedBuilder(
              animation: animationController,
              builder: (context, child) {
                return Text(
                    '\$ ${lerpDouble(oldgst, gst, animationController.value).toStringAsFixed(2)}',
                    style: subtitleStyle);
              },
            ),
          ],
        ),
        SizedBox(
          height: 4,
        ),*/
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text('Service Fee', style: subtitleStyle),
            AnimatedBuilder(
              animation: animationController,
              builder: (context, child) {
                return Text(
                    '\$ ${lerpDouble(oldServiceFee, serviceFee, animationController.value).toStringAsFixed(2)}',
                    style: subtitleStyle);
              },
            ),
          ],
        ),
        SizedBox(
          height: 8,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text('Total', style: headerStyle),
            AnimatedBuilder(
              animation: animationController,
              builder: (context, child) {
                return Text(
                    '\$ ${lerpDouble(oldTotal, total > 39 ? total : total + 5, animationController.value).toStringAsFixed(2)}',
                    style: headerStyle);
              },
            ),
          ],
        ),
        SizedBox(height: 16,),
        (total )<39.00?Align(
          alignment: Alignment.center,
          child: Text("Please add more product of \$ ${(39.00 - (total)).toStringAsFixed(2)} for free service.",textAlign: TextAlign.center,
          style: TextStyle(color: Colors.green,fontSize: 16),
          ),
        ):SizedBox()
      ],
    );
  }

  Widget checkoutButton(context) {
    return Container(
      margin: EdgeInsets.only(top: 24, bottom: 64),
      width: double.infinity,
      child: RaisedButton(
        child: Text('Checkout',
            style: TextStyle(fontSize: 16, color: Colors.white)),
        onPressed: () {
          onCheckOutClick();
        },
        padding: EdgeInsets.symmetric(horizontal: 64, vertical: 12),
        color: mainColor,
        shape: StadiumBorder(),
      ),
    );
  }

  Widget buildCartItemList(CartResult cartModel) {
    return Card(
      margin: EdgeInsets.only(bottom: 16),
      child: Container(
        height: 100,
        padding: EdgeInsets.all(8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(6)),
              child: CachedNetworkImage(
                imageUrl: "${AppConstants.IMAGEURL + cartModel.photo}",

                width: 100,
                height: 100,
              ),
            ),
            Flexible(
              flex: 3,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(

                    
                    child: SizedBox(
                      child: Text(
                        cartModel.proName,

                        style: titleStyle,
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      InkWell(
                        customBorder: roundedRectangle4,
                        onTap: () {
                          /*   cart.decreaseItem(cartModel);*/
                          setState(() {
                            if (int.parse(cartModel.quantity) != 1) {
                              cartModel.quantity =
                                  (int.parse(cartModel.quantity) - 1)
                                      .toString();
                              updateCart(cartModel.cartId, cartModel.quantity);
                            }
                          });

                          animationController.reset();
                          animationController.forward();
                        },
                        child: Icon(Icons.remove_circle),
                      ),
                      Padding(
                        padding:
                            EdgeInsets.symmetric(horizontal: 16.0, vertical: 2),
                        child: Text('${cartModel.quantity}', style: titleStyle),
                      ),
                      InkWell(
                        customBorder: roundedRectangle4,
                        onTap: () {
                          /*print("icrease ${cartModel.food.price}");
                          cart.increaseItem(cartModel);*/
                          setState(() {
                            cartModel.quantity =
                                (int.parse(cartModel.quantity) + 1).toString();
                          });
                          updateCart(cartModel.cartId, cartModel.quantity);
                          animationController.reset();
                          animationController.forward();
                        },
                        child: Icon(Icons.add_circle),
                      ),
                    ],
                  )
                ],
              ),
            ),
            Flexible(
              flex: 1,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    children: [
                      Container(
                        width: 70,
                        child: Text(
                          "\$ ${cartModel.proAfterprice != null && cartModel.proAfterprice.isNotEmpty && cartModel.proPrice != cartModel.proAfterprice ? cartModel.proAfterprice : cartModel.proPrice}",
                          style: titleStyle,
                          textAlign: TextAlign.end,
                        ),
                      ),
                      SizedBox(
                        width: 4,
                      ),
                      cartModel.proAfterprice != null &&
                          cartModel.proAfterprice.isNotEmpty &&
                          cartModel.proPrice != cartModel.proAfterprice
                          ? Container(
                        width: 70,
                            child: Text(
                        "\$ " + cartModel.proPrice,
                        textAlign: TextAlign.end,
                        style: TextStyle(
                              decoration: TextDecoration.lineThrough,
                              fontWeight: FontWeight.w800,
                              color: Colors.green),
                      ),
                          )
                          : SizedBox(),
                    ],
                  ),
                  InkWell(
                    onTap: () {
                      removeFromCart(cartModel.cartId);
                      setState(() {
                        widget.cart.remove(cartModel);
                      });
                      if(widget.cart.isEmpty) {
                        Fluttertoast.showToast(
                            msg: "Please add products to checkout.",
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.BOTTOM,
                            timeInSecForIos: 1,
                            textColor: Colors.white,
                            fontSize: 14.0);
                        Navigator.pop(context);
                      }
                      animationController.reset();
                      animationController.forward();
                    },
                    customBorder: roundedRectangle12,
                    child: Icon(Icons.delete_sweep, color: Colors.red),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
