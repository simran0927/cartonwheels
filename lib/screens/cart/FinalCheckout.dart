import 'package:cached_network_image/cached_network_image.dart';
import 'package:ecommerceapp/app/app_theme.dart';
import 'package:ecommerceapp/commons/constants/app_constants.dart';
import 'package:ecommerceapp/commons/repo/UserRepository.dart';
import 'package:ecommerceapp/constants/values.dart';
import 'package:ecommerceapp/model/CartResult.dart';
import 'package:ecommerceapp/model/address_model.dart';
import 'package:ecommerceapp/model/payment_model.dart';
import 'package:ecommerceapp/screens/cart/service/CartService.dart';
import 'package:ecommerceapp/screens/mainHome/MyHomePage.dart';
import 'package:ecommerceapp/screens/orders/service/OrderDetailService.dart';
import 'package:ecommerceapp/screens/payments/PaymentScreen.dart';
import 'package:ecommerceapp/screens/payments/ThankYouScreen.dart';
import 'package:ecommerceapp/utils/extensions/basic_extensions.dart';
import 'package:ecommerceapp/utils/responsive/flutter_screenutil.dart';
import 'package:ecommerceapp/utils/responsive/size_extension.dart';
import 'package:ecommerceapp/widgets/ProgressDialogType.dart' as pr;
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:place_picker/entities/location_result.dart';
import 'package:place_picker/place_picker.dart';
import 'package:place_picker/widgets/place_picker.dart';

class FinalCheckOut extends StatefulWidget {
  final List<CartResult> cart;

  double total;

  double subtotal;

  double serviceFee;

  CartService cartService;

  FinalCheckOut(this.cart, this.subtotal, this.serviceFee, this.total);

  @override
  _FinalCheckOutState createState() => _FinalCheckOutState();
}

class _FinalCheckOutState extends State<FinalCheckOut> {
  TextEditingController nameController = TextEditingController();
  TextEditingController familyNameController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController postCodeController = TextEditingController();
  TextEditingController notesController = TextEditingController();
  TextEditingController descController = TextEditingController();
  String selectedDate;
  List<String> deleveryDates;

  CartService cartService;
  final dateFormat = DateFormat("EEEE,dd-MM-yyyy");

  List<String> getDeliveryDateFromWeekDays(
      {List<String> date = const ["tuesday", "monday", "friday"]}) {
    final dayData = <int, String>{
      1: "monday",
      2: "tuesday",
      3: "wednesday",
      4: "thursday",
      5: "friday",
      6: "saturday",
      7: "sunday"
    };
    final result = <int>[];
    date.forEach((dateString) {
      var dayFindCount = 0;
      final today = DateTime.now();
      var futureDate = today;
      while (dayFindCount != 2) {
        futureDate = futureDate.add(Duration(days: 1));
        if (dayData[futureDate.weekday] == dateString) {
          result.add(futureDate.millisecondsSinceEpoch);
          dayFindCount++;
        }
      }
    });
    result.sort();
    return result
        .map((e) => dateFormat.format(DateTime.fromMillisecondsSinceEpoch(e)))
        .toList();
  }


  List<String> getNewDeliveryDateFromWeekDays() {
    final result = <DateTime>[];
    DateTime time = DateTime.now().add(Duration(days: 2));
    for (int i = 0; i <= 6; i++) {
      result.add(time);
      time = time.add(Duration(days: 1));
    }
    return result.map((e) => dateFormat.format(e)).toList();
  }

  // List<String> getNewDeliveryDateFromWeekDays() {
  //   final result = <DateTime>[];
  //   DateTime now = new DateTime.now();
  //   DateTime time;
  //
  //   if(now.hour<12){
  //    time = DateTime.now();
  //   }else{
  //      time = DateTime.now().add(Duration(days: 1));
  //   }
  //   for (int i = 0; i <= 6; i++) {
  //     result.add(time);
  //     time = time.add(Duration(days: 1));
  //   }
  //   return result.map((e) => dateFormat.format(e)).toList();
  // }

  bool deleveryAvailable = false;
  pr.ProgressDialog progressDialog;

  @override
  void initState() {
    super.initState();
    progressDialog = pr.ProgressDialog(context,
        type: pr.ProgressDialogType.Normal,
        isDismissible: false,
        showLogs: true);
    cartService = CartService();
    deleveryDates = getNewDeliveryDateFromWeekDays();
    postCodeController.addListener(() {
      getDeleveryDates(postCodeController.text);
    });
    addressController.text = UserRepo.user.address1;
    nameController.text = UserRepo.user.name;
    familyNameController.text = UserRepo.user.familyName;
  }

  getDeleveryDates(postCode) async {
    cartService.getDeleveryDatesFromPostCode(postCode).then((value) {
      setState(() {
        if (value != null && value.isNotEmpty) {
          setState(() {
            deleveryAvailable = true;
          });
        } else {
          setState(() {
            selectedDate = null;
            deleveryAvailable = false;
          });
        }
      });
    }).catchError((e) {});
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    nameController.text = UserRepo.user.name;
    familyNameController.text = UserRepo.user.familyName;
    return Scaffold(
        appBar: AppBar(
            centerTitle: true,
            elevation: 3,
            leading: IconButton(
              icon: Icon(
                Icons.keyboard_arrow_left,
                color: Colors.white,
                size: 32,
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            backgroundColor: AppTheme.primaryColorDark,
            title: Text("Confirm Details")),
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                getTextFieldMain("Given name", "(Required)", nameController),
                getTextFieldMain(
                    "Family name", "(Required)", familyNameController),

                Stack(
                  children: [
                    getTextField(context, addressController, "Delivery Address")
                        .paddingE(
                            EdgeInsets.only(top: 22, left: 22, right: 22)),
                    Positioned(
                        right: 25,
                        top: 18.h,
                        child: SizedBox(
                          height: 22.h,
                          width: 210.w,
                          child: RaisedButton(
                            color: AppTheme.buttonColorLight,
                            child: FittedBox(
                              child: Text("Change Address",
                                  style: TextStyle(
                                      color: AppTheme.primaryColorDark)),
                            ),
                            onPressed: () async {
                              final String address = await showDialog(
                                  context: context,
                                  builder: (context) => ChangeAddressDialog());
                              if (address != null)
                                addressController.text = address;
                            },
                          ),
                        ))
                  ],
                ),
                getNumberTextFieldMain(
                    "Enter your post code to select delivery date",
                    "(Required)",
                    6,
                    postCodeController),
                deleveryAvailable
                    ? Text(
                        "Select Delivery date",
                        style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                            color: AppTheme.primaryColorDark),
                      ).paddingAll(top: 25, left: 25, right: 25)
                    : SizedBox(),
                deleveryAvailable
                    ? ListView.builder(
                        shrinkWrap: true,
                        itemCount: deleveryDates.length,
                        physics: NeverScrollableScrollPhysics(),
                        itemBuilder: (context, pos) {
                          return ListTile(
                            title: Text(deleveryDates[pos]),
                            leading: Radio(
                              value: deleveryDates[pos],
                              groupValue: selectedDate,
                              onChanged: (String value) {
                                setState(() {
                                  FocusScope.of(context).unfocus();
                                  selectedDate = value;
                                });
                              },
                            ),
                          );
                        })
                    : Text("No delivery date for this postcode.")
                        .paddingLeft(25),
                Text(
                  "Products",
                  style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                      color: AppTheme.primaryColorDark),
                ).paddingAll(top: 25, left: 25, right: 25),
                ListView.builder(
                  itemCount: widget.cart.length,
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, int index) {
                    return buildCartItemList(widget.cart[index]);
                  },
                ).paddingTop(25),
                buildPriceInfo(this.widget.cart)
                    .paddingAll(left: 25, right: 25),
                getTextFieldMain("Order notes", "(Optional)", notesController),
                getTextFieldMain("Didn't find anything? Please mention here.",
                    "(Optional)", descController),
                RichText(
                    text: TextSpan(
                        text:
                            "Your personal data will be used to process your order, support your experience throughout this website, and for other purposes described in our ",
                        style: TextStyle(color: Colors.black54),
                        children: [
                      TextSpan(
                          text: "privacy policy.",
                          style: TextStyle(
                              color: Colors.black,
                              decoration: TextDecoration.underline),
                          recognizer: TapGestureRecognizer()
                            ..onTapDown = (detail) {})
                    ])).paddingAll(top: 25, left: 25, right: 25),
                checkoutButton(context)
              ],
            ),
          ),
        ));
  }

  Widget buildPriceInfo(List<CartResult> cart) {
    //oldTotal = total;
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text('Subtotal', style: subtitleStyle),
            Text('\$ ${this.widget.subtotal.toStringAsFixed(2)}',
                style: subtitleStyle)
          ],
        ),
        SizedBox(
          height: 4,
        ),
        /* Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text('GST', style: subtitleStyle),
            Text('\$ ${this.widget.gst.toStringAsFixed(2)}',
                style: subtitleStyle)
          ],
        ),
        SizedBox(
          height: 4,
        ),*/
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text('Service Fee', style: subtitleStyle),
            Text('\$ ${this.widget.serviceFee.toStringAsFixed(2)}',
                style: subtitleStyle)
          ],
        ),
        SizedBox(
          height: 4,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text('Wallet amount adjusted', style: subtitleStyle),
            Text('\$ ${getWalletAmount().toStringAsFixed(2)}',
                style: subtitleStyle),
          ],
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text("*you can use upto 10\$ for an order.",
              style: subtitleStyle),
        ),
        SizedBox(
          height: 12,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text('Grand Total', style: headerStyle),
            Text('\$ ${getGrandTotal().toStringAsFixed(2)}', style: headerStyle)
          ],
        ),
        SizedBox(
          height: 16,
        )
      ],
    );
  }

  double getGrandTotal() {
    var getGrandTotal = getTotal() - getWalletAmount();
    if (getGrandTotal > 0) {
      return getGrandTotal;
    } else {
      return 0.00;
    }
  }

  double getWalletAmount() => (UserRepo.walletAmount > 10.0)
      ? (getTotal() > 10.0)
          ? 10.0
          : getTotal()
      : (getTotal() > UserRepo.walletAmount)
          ? UserRepo.walletAmount
          : getTotal();

  double getTotal() => double.parse(this.widget.total.toDouble() > 39.00
      ? this.widget.total.toStringAsFixed(2)
      : (this.widget.total + 5.00).toStringAsFixed(2));

  Widget checkoutButton(context) {
    return Container(
      margin: EdgeInsets.only(top: 24, bottom: 48, left: 16, right: 16),
      width: double.infinity,
      child: RaisedButton(
        child: Text('Proceed to Checkout',
            style: TextStyle(fontSize: 16, color: Colors.white)),
        onPressed: () async {
          if (await validate()) {
            UserRepo.paymentModel = PaymentModel(
                fname: UserRepo.user.name,
                lname: "",
                email: UserRepo.user.email,
                postcode: postCodeController.text,
                delivaryDate: selectedDate,
                addr1: addressController.text,
                loginType: "app",
                odrnote: notesController.text,
                didNotFind: descController.text,
                phone: UserRepo.user.phone,
                userId: UserRepo.user.usersId,
                odrTotal: getGrandTotal().toStringAsFixed(2),
                fullTotal: getTotal().toStringAsFixed(2),
                payFromWallet: getWalletAmount().toStringAsFixed(2),
                serviceFee: widget.serviceFee.toStringAsFixed(2),
                payType: (getGrandTotal() > 0.0) ? "eway" : "wallet");
            final orderId = await Navigator.push(context,
                MaterialPageRoute(builder: (context) => PaymentScreen()));
            if (orderId != null) {
              checkPaymentStatus(orderId);
            }
          }
          //onCheckOutClick();
        },
        padding: EdgeInsets.symmetric(horizontal: 64, vertical: 12),
        color: mainColor,
        shape: StadiumBorder(),
      ),
    );
  }

  checkPaymentStatus(orderId) async{
    await progressDialog.hide();
    OrderDetailService().seeOrderDetails(orderId).then((value) async {
      await progressDialog.hide();
      if (value != null && value.length > 0) {
        if(value[0].isSuccessful()) {
          Fluttertoast.showToast(
              msg: "Order Successful",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIos: 1,
              textColor: Colors.white,
              fontSize: 14.0);
          Navigator.popUntil(context, ModalRoute.withName(MyHomePage.routeName));
        } else {
          Fluttertoast.showToast(
              msg: "Payment Declined",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIos: 1,
              textColor: Colors.white,
              fontSize: 14.0);
        }
      }
    });
  }

  Widget buildCartItemList(CartResult cartModel) {
    return Card(
      margin: EdgeInsets.only(bottom: 16),
      child: Container(
        height: 100,
        padding: EdgeInsets.all(8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(6)),
              child: CachedNetworkImage(
                imageUrl: "${AppConstants.IMAGEURL + cartModel.photo}",
                width: 100,
                height: 100,
              ),
            ),
            Flexible(
              flex: 3,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Container(
                    height: 45,
                    child: Text(
                      cartModel.proName,
                      style: titleStyle,
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Text(
                    "Quantity: ${cartModel.quantity}",
                    style: subtitleStyle,
                    textAlign: TextAlign.center,
                  )
                ],
              ),
            ),
            Flexible(
              flex: 1,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    height: 45,
                    width: 70,
                    child: Text(
                      '\$ ${cartModel.proPrice}',
                      style: titleStyle,
                      textAlign: TextAlign.end,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  getTextFieldMain(
      String name, String hintString, TextEditingController controller) {
    return Column(
      children: [
        Padding(
            padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
            child: new Row(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Expanded(
                  child: new Text(
                    name,
                    style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                        color: AppTheme.primaryColorDark),
                  ),
                ),
              ],
            )),

        Padding(
            padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
            child: new Row(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                new Flexible(
                  child: new TextField(
                    controller: controller,
                    decoration: InputDecoration(hintText: hintString),
                  ),
                ),
              ],
            )),
      ],
    );
  }

  getNumberTextFieldMain(String name, String hintString, maxLength,
      TextEditingController controller) {
    return Column(
      children: [
        Padding(
            padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
            child: new Row(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Expanded(
                  child: new Text(
                    name,
                    style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                        color: AppTheme.primaryColorDark),
                  ),
                ),
              ],
            )),
        Padding(
            padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
            child: new Row(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                new Flexible(
                  child: new TextField(
                    controller: controller,
                    maxLength: maxLength,
                    keyboardType: TextInputType.numberWithOptions(
                        decimal: false, signed: false),
                    decoration: InputDecoration(hintText: hintString),
                  ),
                ),
              ],
            )),
      ],
    );
  }

  Widget getTextField(context, controller, name, {bool isNumber = false}) =>
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(name,
                  style: TextStyle(
                      fontSize: 16,
                      color: AppTheme.primaryColorDark,
                      fontWeight: FontWeight.bold))
              .paddingLeft(3),
          SizedBox(height: 8),
          SizedBox(
            height: 55,
            child: TextField(
              autofocus: false,
              controller: controller,
              keyboardType: isNumber ? TextInputType.phone : TextInputType.text,
              cursorColor: Colors.white,
              textAlignVertical: TextAlignVertical.center,
              style: TextStyle(
                  fontSize: 16.0, color: Colors.black87, letterSpacing: 1.2),
              decoration: InputDecoration(
                filled: true,
                floatingLabelBehavior: FloatingLabelBehavior.never,
                fillColor: Color(0xFFEAEAEA),
                hintStyle: TextStyle(color: Colors.black87),
                contentPadding: EdgeInsets.only(left: 14.0.w, right: 14.w),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white),
                  borderRadius: BorderRadius.circular(8.0),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white),
                  borderRadius: BorderRadius.circular(8.0),
                ),
              ),
            ),
          )
        ],
      );

  validate() async {
    if (addressController.text.isEmpty) {
      await Fluttertoast.showToast(
          msg: "Please fill address.",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          textColor: Colors.white,
          fontSize: 14.0);
      return false;
    }

    if (postCodeController.text.isEmpty) {
      await Fluttertoast.showToast(
          msg: "Please fill postcode.",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          textColor: Colors.white,
          fontSize: 14.0);
      return false;
    }

    if (selectedDate.isEmpty) {
      await Fluttertoast.showToast(
          msg: "Please select delivery date.",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          textColor: Colors.white,
          fontSize: 14.0);
      return false;
    }

    return true;
  }
}

class ChangeAddressDialog extends StatefulWidget {
  @override
  _ChangeAddressDialogState createState() => _ChangeAddressDialogState();
}

class _ChangeAddressDialogState extends State<ChangeAddressDialog> {
  CartService cartService;
  List<AddressModel> addressList;

  @override
  void initState() {
    super.initState();
    cartService = CartService();
    getAddresses();
    //cartService.getAddress(cartId, quantity)
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
        elevation: 3,
        backgroundColor: Colors.white,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
        child: SingleChildScrollView(
          child: Container(
            constraints: BoxConstraints(
                minHeight: MediaQuery.of(context).size.height * 0.45),
            margin: EdgeInsets.only(top: 22, bottom: 22),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 35.h,
                  width: MediaQuery.of(context).size.width,
                  child: RaisedButton(
                    color: AppTheme.buttonColorLight,
                    child: Text("Choose address to add",
                        style: TextStyle(color: AppTheme.primaryColorDark)),
                    onPressed: () async {
                      LocationResult result =
                          await Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => PlacePicker(
                                    "AIzaSyDLDmvcZpQKHnbkEg76bk9jMd4QB_kZw4c",
                                    displayLocation:
                                        LatLng(-33.865143, 151.209900),
                                  )));

                      // Handle the result in your way
                      if (result != null) {
                        if (result.country.shortName == "AU") {
                          saveAddress(result.formattedAddress);
                        } else {
                          Fluttertoast.showToast(
                              msg: "Address must be in australia.",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.BOTTOM,
                              timeInSecForIos: 1,
                              textColor: Colors.white,
                              fontSize: 14.0);
                        }
                      }
                    },
                  ),
                ).paddingAll(left: 16, right: 16),
                addressList != null && addressList.isNotEmpty
                    ? Text(
                        "Pick address from below :",
                        style: TextStyle(
                            fontSize: 18, color: AppTheme.primaryColorDark),
                      ).paddingAll(top: 16, bottom: 16, left: 16)
                    : SizedBox(),
                addressList != null && addressList.isNotEmpty
                    ? ListView.builder(
                        shrinkWrap: true,
                        itemCount: addressList.length,
                        itemBuilder: (context, pos) {
                          return addressContainer(addressList[pos]);
                        })
                    : SizedBox(
                        height: MediaQuery.of(context).size.height * 0.40,
                        child: Center(child: Text("No address added."))),
              ],
            ),
          ),
        ));
  }

  addressContainer(AddressModel address) {
    return InkWell(
      onTap: () {
        Navigator.pop(context, address.del_address);
      },
      child: Card(
          color: AppTheme.primaryColorDark,
          elevation: 4,
          shadowColor: AppTheme.primaryColorDark,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
          margin: EdgeInsets.only(left: 16, right: 16, top: 13),
          child: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: Padding(
              padding: EdgeInsets.fromLTRB(16, 12, 16, 12),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    child: Text(
                      address?.del_address ?? "",
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                  ),
                  InkWell(
                      onTap: () {
                        deleteAddress(address);
                      },
                      child: Icon(
                        Icons.delete,
                        color: Colors.red,
                      ))
                ],
              ),
            ),
          )),
    );
  }

  void deleteAddress(AddressModel address) {
    cartService
        .deleteAddress(UserRepo.user.usersId, address.del_adr_id)
        .then((value) {})
        .catchError((e) {});
    setState(() {
      addressList.remove(address);
    });
  }

  void getAddresses() async {
    cartService.getAddress(UserRepo.user.usersId).then((value) {
      setState(() {
        addressList = value;
      });
    }).catchError((e) {});
  }

  void saveAddress(addressString) async {
    await cartService.saveAddress(UserRepo.user.usersId, addressString);
    getAddresses();
  }
}
