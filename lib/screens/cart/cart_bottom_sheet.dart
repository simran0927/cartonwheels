import 'package:cached_network_image/cached_network_image.dart';
import 'package:ecommerceapp/commons/constants/app_constants.dart';
import 'package:ecommerceapp/commons/repo/UserRepository.dart';
import 'package:ecommerceapp/constants/values.dart';
import 'package:ecommerceapp/model/CartResult.dart';
import 'package:ecommerceapp/screens/cart/service/CartService.dart';
import 'package:ecommerceapp/utils/extensions/number_parse.dart';
import 'package:flutter/material.dart';

import 'checkout_page.dart';

class CartBottomSheet extends StatefulWidget {
  @override
  _CartBottomSheetState createState() => _CartBottomSheetState();
}

class _CartBottomSheetState extends State<CartBottomSheet> {
  CartService cartService;
  List<CartResult> cartItems;

  @override
  void initState() {
    cartService = CartService();
    getCartItems();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return cartItems != null && cartItems.isNotEmpty
        ? Container(
            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  alignment: Alignment.center,
                  width: double.infinity,
                  child: Container(
                    width: 90,
                    height: 8,
                    decoration: ShapeDecoration(
                        shape: StadiumBorder(), color: Colors.black26),
                  ),
                ),
                buildTitle(),
                Divider(),
                buildItemsList(cartItems),
                Divider(),
                buildPriceInfo(cartItems),
                SizedBox(height: 8),
                addToCardButton(cartItems, context),
              ],
            ),
          )
        : noItemWidget();
  }

  Widget buildTitle(/*cart*/) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text('Your Order', style: headerStyle),
        /*RaisedButton.icon(
          icon: Icon(Icons.delete_forever),
          color: Colors.red,
          shape: StadiumBorder(),
          splashColor: Colors.white60,
          onPressed: () {
            setState(() {
              //cart.clearCart();
            });
          },
          textColor: Colors.white,
          label: Text('Clear'),
        ),*/
      ],
    );
  }

  Widget buildItemsList(List<CartResult> cart) {
    return Expanded(
      child: ListView.builder(
        itemCount: cart.length,
        physics: BouncingScrollPhysics(),
        itemBuilder: (context, index) {
          return Card(
            child: ListTile(
              leading: CircleAvatar(
                  backgroundImage: CachedNetworkImageProvider(
                      '${AppConstants.IMAGEURL + cart[index].photo}')),
              title: Text('${cart[index].proName}', style: subtitleStyle),
              subtitle: Row(
                children: [
                  Text(
                      "\$ ${cart[index].proAfterprice != null && cart[index].proAfterprice.isNotEmpty && cart[index].proPrice != cart[index].proAfterprice ? cart[index].proAfterprice : cart[index].proPrice}"),
                  SizedBox(
                    width: 4,
                  ),
                  cart[index].proAfterprice != null &&
                          cart[index].proAfterprice.isNotEmpty &&
                          cart[index].proPrice != cart[index].proAfterprice
                      ? Text(
                          "\$ " + cart[index].proPrice,
                          style: TextStyle(
                              decoration: TextDecoration.lineThrough,
                              fontWeight: FontWeight.w800,
                              color: Colors.green),
                        )
                      : SizedBox()
                ],
              ),
              trailing: Text('x ${cart[index].quantity}', style: subtitleStyle),
            ),
          );
        },
      ),
    );
  }

  Widget noItemWidget() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Expanded(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text('You don\'t have any order yet!!', style: titleStyle2),
                  SizedBox(height: 16),
                  Icon(Icons.remove_shopping_cart, size: 40),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildPriceInfo(List<CartResult> cart) {
    double total = 0;
    for (CartResult cartModel in cart) {
      total +=
          (cartModel.proAfterprice != null && cartModel.proAfterprice.isNotEmpty
                  ? cartModel.proAfterprice.parseDouble()
                  : cartModel.proPrice.parseDouble()) *
              cartModel.quantity.parseInt();
    }

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text('Total:', style: headerStyle),
        Text('\$ ${total.toStringAsFixed(2)}', style: headerStyle),
      ],
    );
  }

  Widget addToCardButton(List<CartResult> cart, context) {
    return Center(
      child: RaisedButton(
        child: Text('CheckOut',
            style: TextStyle(fontSize: 16, color: Colors.white)),
        onPressed: cart.isEmpty
            ? null
            : () {
                Navigator.of(context).pop();
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => CheckOutPage(cart)));
              },
        padding: EdgeInsets.symmetric(horizontal: 64, vertical: 12),
        color: mainColor,
        shape: StadiumBorder(),
      ),
    );
  }

  void getCartItems() async {


    cartService.getAllCartItems(UserRepo.user.usersId).then((value) {
      setState(() {
        cartItems = value;
      });
    });
  }
}
