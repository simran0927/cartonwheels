import 'package:flutter/material.dart';

mixin BaseScreen {
  /// should be overridden in extended widget
  String screenName();

  /// should be overridden in extended widget
  bool showDrawer();

  bool useBaseLayout();

  int setBaseTabLayout();

  Color setBackgroundColor();

  List<Widget> setAppbarWidgets();

  PreferredSizeWidget setBottomWidget();

  //getAppConfig();

  /// should be overridden in extended widget
  Widget buildBody(BuildContext context);

  void onError(Object e);

  void onShowProgress();

  void onHideProgress();
}
