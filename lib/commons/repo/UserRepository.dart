import 'dart:convert';

import 'package:ecommerceapp/commons/constants/pref_constant.dart';
import 'package:ecommerceapp/model/LoginData.dart';
import 'package:ecommerceapp/model/payment_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserRepo {
  static LoginData user;
  static double walletAmount = 0.0;
  static PaymentModel paymentModel;

  static Future<LoginData> getUserDetail() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final userJsonString = prefs.getString(PrefConstant.USER);
    if (userJsonString != null && userJsonString.isNotEmpty) {
      var loginJson = await json.decode(prefs.getString(PrefConstant.USER));
      LoginData loginData = LoginData.fromJson(loginJson);
      user = loginData;
      return loginData;
    } else {
      user = null;
      return null;
    }
  }

  static Future<bool> clearPreference() async {
    user = null;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.clear();
  }

  static Future<bool> saveUser(LoginData userData) async {
    user = userData;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(PrefConstant.USER, json.encode(userData.toJson()));
  }
}
