class AppConstants {
  AppConstants._();

  //https://cartonwheels.com.au/api/
  // base url
  static const String baseUrl = "https://cartonwheels.com.au/api/";
  //static const String baseUrl = "https://cartonwheels.com.au/api/";
  static const String baseUrl_dev = "https://cartonwheels.com.au/api/";

  // receiveTimeout
  static const int receiveTimeout = 5000;

  // connectTimeout
  static const int connectionTimeout = 3000;

  // booking endpoints
  static const String AUTH_API = 'authenticate';
  static const String GET_SALES_RANKING_API = "";
  static const String SEARCH_FILTER_API = "";
  static const String SEARCH_DATA_API = "";
  static const String GET_PROPERTIES_API = "";
  static const String REGISTER = "register";
  static const String LOGIN = "loginmatch";
  static const String FETCHALL = "fetchall";
  static const String SUBCATFORCAT = "subcategory";
  static const String IMAGEURL = "https://cartonwheels.com.au/upload/admin/users/";
  static const String CART = "cart/";
  static const String WISHLIST = "wishlist/";
  static const String LISTS = "lists/";
  static const String CREATE_LISTS = "createlist/";
  static const String DELETE_LISTS = "listremove/";
  static const String DELETE_LIST_PRODUCT = "proremovefromlist/";
  static const String LIST_ITEMS = "listitems/";
  static const String SAVE_PRODUCT_LIST = "saveselectproduct/";
  static const String ADD_TO_WISHLIST = "addwishlist/";
  static const String ADD_TO_CART = "addtocart/";
  static const String SEARCH_PRODUCT = "search/";
  static const String FORGET_PASS = "forgetpasswordemail";
  static const String UPDATE_USER = "update_user";
  static const String WISH_REMOVE = "wishremove/";
  static const String CART_REMOVE = "cartremove/";
  static const String UPDATE_CART = "updatecartquantity/";
  static const String SEND_EMAIL = "emailsend/";
  static const String ORDER_HISTORY = "ordershistory/";
  static const String DELETE_ADDRESS = "delete_address/";
  static const String SAVE_ADDRESS = "save_del_address/";
  static const String ADDRESS = "addresses/";
  static const String DELIVERY_DATE = "check_delivery/";
  static const String ORDER_DETAIL="items";

  static const String WALLET_TOTAL="wallettotal/";
  static const String WALLET_TRANSACTION="getwallet/";
  static const String PAYMENT="placeorder/";
  //Preferences
  static const String AUTH_TOKEN = "AUTH_TOKEN";
}
