import 'package:dio/dio.dart';
import 'package:ecommerceapp/commons/constants/app_constants.dart';


import 'interceptor/LoggingInterceptor.dart';

class ApiHitter {
  static Dio _dio;
  static Dio _dioDev;

  static Dio getDio() {
    if (_dio == null) {
      BaseOptions options = new BaseOptions(
          baseUrl: AppConstants.baseUrl, //_config.baseApi, //
          connectTimeout: AppConstants.connectionTimeout,
          receiveTimeout: AppConstants.receiveTimeout);
      _dio = new Dio(options)
        ..interceptors.add(LoggingInterceptor(requestBody: true));
      return _dio;
    } else {
      return _dio;
    }
  }

  static Dio getDevDio() {
    if (_dioDev == null) {
      BaseOptions options = new BaseOptions(
          baseUrl: AppConstants.baseUrl_dev, //_config.baseApi, //
          connectTimeout: AppConstants.connectionTimeout,
          receiveTimeout: AppConstants.receiveTimeout);
      _dioDev = new Dio(options)
        ..interceptors.add(LoggingInterceptor(requestBody: true));
      return _dioDev;
    } else {
      return _dioDev;
    }
  }

  static Future<ApiResponse> getApiResponse(String endPoint,
      {Map<String, dynamic> headers,
        Map<String, dynamic> queryParameters, bool isDev = false}) async {
    try {
      Response response = await getRealDio(isDev).get(endPoint,
          options: Options(headers: headers), queryParameters: queryParameters);
      return ApiResponse(true, response: response);
    } catch (error, stacktrace) {
      try {
        return ApiResponse(false,
            msg: "${error.response.data['message'] ?? "Sorry Something went wrong."}");
      } catch (e) {
        return ApiResponse(false, msg: "Sorry Something went wrong.");
      }
    }
  }

  static Future<ApiResponse> getPostApiResponse(String endPoint,
      {Map<String, dynamic> headers, Map<String, dynamic> data, FormData formData, bool isDev = false}) async {
    try {
      var response = await getRealDio(isDev)
          .post(endPoint, options: Options(headers: headers), data: formData==null?data:formData);
      return ApiResponse(true, response: response, msg: "success");
    } catch (error, stacktrace) {
      try {
        return ApiResponse(false,
            msg: "${error.response.data['message'] ?? "Sorry Something went wrong."}");
      } catch (e) {
        return ApiResponse(false, msg: "Sorry Something went wrong.");
      }
    }
  }

  static getRealDio(bool isDev) {
    if(isDev) {
      return getDevDio();
    } else {
      return getDio();
    }
  }
}

class ApiResponse {
  final bool status;
  final String msg;
  final Response response;

  ApiResponse(this.status, {this.msg = "success", this.response});
}
