import 'package:ecommerceapp/commons/repo/UserRepository.dart';
import 'package:ecommerceapp/screens/cart/provider/UpdateCartProvider.dart';
import 'package:ecommerceapp/screens/splash/splash.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'app/app_routes.dart';
import 'app/app_theme.dart';


void main() {
  UpdateCartProvider updateCartProvider = UpdateCartProvider();
  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider.value(value: updateCartProvider),
    ],
    child: MaterialApp(
        theme: ThemeData(
            primaryColor: AppTheme.primaryColorDark,
            primaryColorDark: AppTheme.primaryColorDark,
            brightness: Brightness.light),
        routes: PageRouter.routes(),
        debugShowCheckedModeBanner: false,
        home: SplashScreen()),
  ));

  UserRepo.getUserDetail().then((value) {
    if (value != null) {
      updateCartProvider.fetchCartItems();
    }
  });
}
