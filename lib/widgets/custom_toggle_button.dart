import 'package:flutter/material.dart';
import '../widgets/toggle_button_theme.dart';

class CustomToggleButton extends StatefulWidget {
  final List<String> items;
  final Function(int) onToggle;

  CustomToggleButton(this.items, this.onToggle);

  @override
  _CustomToggleButtonState createState() => _CustomToggleButtonState();
}

class _CustomToggleButtonState extends State<CustomToggleButton> {
  @override
  bool isFirstItemSelected;
  var width;
  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    setState(() {
      this.isFirstItemSelected = true;
      // if (propertyProvider.selectedButtonIndex == 0) {
      //   this.isFirstItemSelected = true;
      // } else {
      //   this.isFirstItemSelected = false;
      // }
    });
  }

  Widget build(BuildContext context) {
   //final propertyProvider = Provider.of<PropertProvider>(context);
width=MediaQuery.of(context).size.width*0.80;
    return (widget.items.length > 0 && widget.items.length == 2)
        ? ClipRRect(
            borderRadius: BorderRadius.circular(30),
            child: Container(
              width: width,
              color: ToggleButtonTheme.background_color, //(0xff053981),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  this.isFirstItemSelected
                      ? ButtonTheme(
                          minWidth: width*0.50, //54.0,
                          height: ToggleButtonTheme.buttonHeight,
                          buttonColor: ToggleButtonTheme.selected_button_color,
                          child: RaisedButton(
                            textColor: ToggleButtonTheme
                                .selected_button_text_color, //Color(0xff0246A3),  //text color blue
                            padding: const EdgeInsets.all(0.0),
                            child: Text(
                              widget.items[0],
                              style: ToggleButtonTheme.text_style,
                            ),
                            onPressed: () {},
                            shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(20.0),
                            ),
                          ),
                        )
                      : ButtonTheme(
                          minWidth: width*0.50,
                          height: ToggleButtonTheme.buttonHeight,
                          buttonColor: ToggleButtonTheme.background_color,
                          child: FlatButton(

                              padding: const EdgeInsets.all(0.0),
                              textColor: ToggleButtonTheme
                                  .unselected_button_text_color,
                              child: Text(widget.items[0],
                                  style: ToggleButtonTheme.text_style),
                              onPressed: () {
                                setState(() {
                                  this.isFirstItemSelected = true;
                                  widget.onToggle(0);
                                 // propertyProvider.selectedButtonIndex = 0;
                                });
                              }),
                        ),
                  this.isFirstItemSelected
                      ? ButtonTheme(
                          minWidth: width*0.50,
                          height: ToggleButtonTheme.buttonHeight,
                          buttonColor: ToggleButtonTheme.background_color,
                          child: FlatButton(
                            padding: const EdgeInsets.all(0.0),
                            textColor:
                                ToggleButtonTheme.unselected_button_text_color,
                            child: Text(widget.items[1],
                                style: ToggleButtonTheme.text_style),
                            onPressed: () {
                              setState(() {
                                this.isFirstItemSelected = false;
                                widget.onToggle(1);
                                //propertyProvider.selectedButtonIndex = 1;
                              });
                            },
                          ),
                        )
                      : ButtonTheme(
                          minWidth: width*0.50,
                          height: ToggleButtonTheme.buttonHeight,
                          buttonColor: ToggleButtonTheme
                              .selected_button_color, //Colors.white,
                          child: RaisedButton(
                            padding: const EdgeInsets.all(0.0),
                            textColor:
                                ToggleButtonTheme.selected_button_text_color,
                            child: Text(widget.items[1],
                                style:ToggleButtonTheme.text_style ),
                            onPressed: () {},
                            shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(20.0),
                            ),
                          ),
                        )
                ],
              ),
            ),
          )
        : Container();
  }
}
