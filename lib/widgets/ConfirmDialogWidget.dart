import 'package:ecommerceapp/app/app_theme.dart';
import 'package:flutter/material.dart';

class ConfirmDialogWidget extends StatelessWidget {
  final String msg;

  const ConfirmDialogWidget(this.msg);
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        title: Text(
            msg),
        actions: <Widget>[
          FlatButton(
              onPressed: () {
                Navigator.pop(context, true);
              },
              child: Text("Yes",
                  style: TextStyle(
                      fontWeight: FontWeight.w500,
                      color: AppTheme
                          .primaryColorDark))),
          FlatButton(
              onPressed: () {
                Navigator.pop(context, false);
              },
              child: Text("No",
                  style: TextStyle(
                      fontWeight: FontWeight.w500,
                      color:
                      AppTheme.primaryColorDark)))
        ]);
  }
}
