import 'package:ecommerceapp/widgets/SpinnerItem.dart';
import 'package:flutter/material.dart';
import 'package:ecommerceapp/utils/responsive/size_extension.dart';
import 'package:ecommerceapp/utils/extensions/basic_extensions.dart';


class DropDownSpinner<T extends SpinnerItem> extends StatefulWidget {
  final List<T> items;
  final int selectPos;
  final String hint;

  get dropDownState => DropDownWidget<T>();
  final ValueChanged<T> valueChangeListener;

  DropDownSpinner(this.items, this.valueChangeListener,
      {this.selectPos, this.hint = "CHOOSE ONE"});

  @override
  State<StatefulWidget> createState() {
    return dropDownState;
  }
}

class DropDownWidget<T extends SpinnerItem> extends State<DropDownSpinner>  with AutomaticKeepAliveClientMixin<DropDownSpinner>{
  T selectedValue;
  List<DropdownMenuItem<T>> itemList;

  @override
  void initState() {
    super.initState();
    itemList = widget.items
        .map((item) => DropdownMenuItem<T>(
            value: item,
            child: Text(item?.toValue() ?? "ss",
                style: TextStyle(color: Colors.grey))))
        .toList();
    selectedValue = widget.selectPos == null ? null : itemList[widget.selectPos].value;
    widget.valueChangeListener(selectedValue);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 42.h,
      decoration: BoxDecoration(
        color: Colors.white,
          borderRadius: BorderRadius.circular(25)),
      child: Padding(
        padding:  EdgeInsets.fromLTRB(8.0.w, 4.0.h, 8.0.w, 4.0.h),
        child: DropdownButtonHideUnderline(
          child: DropdownButton<T>(
            isDense: true,
            style: TextStyle(fontSize: 14.sp),
            hint: Text(widget.hint),
            value: selectedValue,
            items: itemList,
            onChanged: (value) {
              widget.valueChangeListener(value); // no need
              selectedValue = value;
              setState(() {
                selectedValue = value;
              });
            },
            isExpanded: true,
          ),
        ),
      ),
    ).elevation(25.7, elevation: 8);
  }

  @override
  bool get wantKeepAlive => true;
}

class StringSpinnerItem implements SpinnerItem {
  String value;
  StringSpinnerItem(this.value);
  @override
  String toValue() {
    return value;
  }

}
