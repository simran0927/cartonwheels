import 'package:ecommerceapp/app/app_theme.dart';
import 'package:flutter/material.dart';


class BaseAppbar extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final bool title_localized;
  final AppBar appBar;
  final List<Widget> widgets;
  final PreferredSizeWidget bottom;
  final double elevation;

  /// you can add more fields that meet your needs

  const BaseAppbar(
      {Key key,
      this.title,
      this.title_localized = false,
      this.appBar,
      this.widgets,
      this.bottom,
      this.elevation})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      centerTitle: true,
      elevation: elevation,
      leading: IconButton(icon:Icon(Icons.keyboard_arrow_left,color: Colors.white,
      size: 32,),
      onPressed: (){
        Navigator.of(context).pop();
      },),
      backgroundColor:AppTheme.primaryColorDark,
        title: Text(
            !title_localized
                ? title:null,


//          TextStyle(
//            color: AppTheme.secondaryAppThemeColor,
//          )
            ),
        actions: widgets,
        bottom: bottom);
  }

//  @override
//  Size get preferredSize => new Size.fromHeight(appBar.preferredSize.height);

  @override
  // TODO: implement preferredSize
  Size get preferredSize => bottom != null
      ? new Size.fromHeight(appBar.preferredSize.height * 2)
      : new Size.fromHeight(appBar.preferredSize.height);
}
