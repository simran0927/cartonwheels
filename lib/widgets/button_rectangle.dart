import 'package:flutter/material.dart';

import '../app/app_theme.dart';

class RectangleButton extends StatelessWidget {
  final Function _onPressAction;
  final String _title;

  RectangleButton(this._onPressAction, this._title);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      child: Text(_title),
      onPressed: _onPressAction,
      color: AppTheme.mainAppThemeColor,
      textColor: AppTheme.white,
      padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
    );
  }
}
