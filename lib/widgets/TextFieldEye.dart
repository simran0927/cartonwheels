import 'package:flutter/material.dart';
import 'package:ecommerceapp/utils/responsive/size_extension.dart';

class PasswordField extends StatefulWidget {

final TextEditingController controller;
final String hint;

  PasswordField(this.controller, this.hint);

  @override
  _PasswordFieldState createState() => _PasswordFieldState();
}

class _PasswordFieldState extends State<PasswordField> {

  bool _hidePassword = true;
  @override
  Widget build(BuildContext context) {
    return TextField(
      obscureText: _hidePassword,
      autofocus: false,
      controller: widget.controller,
      cursorColor: Colors.white,
      textAlignVertical: TextAlignVertical.center,
      style: TextStyle(fontSize: 22.0.sp, color: Colors.black87,letterSpacing: 1.2),
      decoration: InputDecoration(
        filled: true,
        floatingLabelBehavior: FloatingLabelBehavior.never,
        fillColor: Color(0xFFEAEAEA),
        hintText: widget.hint,
        hintStyle: TextStyle(color: Colors.black87),

        contentPadding: EdgeInsets.only(left: 14.0.w, right: 14.w),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.white),
          borderRadius: BorderRadius.circular(8.0),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.white),
          borderRadius: BorderRadius.circular(8.0),
        ),
        suffixIcon: IconButton(
          icon: Icon(
            Icons.remove_red_eye,
            color: this._hidePassword ? Colors.black : Colors.grey,
          ),
          onPressed: () {
            setState(() => this._hidePassword = !this._hidePassword);
          },
        ),
      ),

    );

  }


}
