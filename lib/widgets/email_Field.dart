import 'package:flutter/material.dart';

import '../app/app_localizations.dart';

class EmailField extends StatelessWidget {
  final TextEditingController controller;

  EmailField(this.controller);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: this.controller,
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      decoration: InputDecoration(
        hintText: AppTranslations.of(context).text("email"),
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );
  }
}
