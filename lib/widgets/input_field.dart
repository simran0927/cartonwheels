import 'package:flutter/material.dart';

import '../app/app_theme.dart';

class RoundedCornerInput extends StatelessWidget {
  final String hintText;
  final String defaultText;
  final bool isSecureInput;
  final TextEditingController textController;

  RoundedCornerInput(
      {this.hintText,
      this.isSecureInput,
      this.textController,
      this.defaultText});

  @override
  Widget build(BuildContext context) {
    return TextField(
      obscureText: isSecureInput,
      controller: textController,
      decoration: InputDecoration(
        labelStyle: TextStyle(color: AppTheme.nearlyBlack),
        labelText: this.hintText,
        contentPadding: EdgeInsets.only(left: 25, right: 25),
        hintStyle: TextStyle(color: AppTheme.nearlyBlack),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: AppTheme.nearlyBlack),
          borderRadius: BorderRadius.circular(25.7),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: AppTheme.nearlyBlack),
          borderRadius: BorderRadius.circular(25.7),
        ),
      ),
      style: TextStyle(fontSize: 18, color: AppTheme.nearlyBlack),
    );
  }
}
