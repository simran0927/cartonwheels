
import 'package:flutter/material.dart';
import 'package:ecommerceapp/utils/responsive/size_extension.dart';

  // Styles used in the Dashboard Module specific widgets
class ToggleButtonTheme {
  ToggleButtonTheme._();

static const double buttonWidth = 142.0;
static const double buttonHeight = 76.0;

  //Colors used for Toggle button  
  static const Color background_color = Color(0xffD8D8D8);
  static const Color selected_button_color = Color(0xff302f2f);
  static const Color selected_button_text_color = Color(0xffD8D8D8);
  static const Color unselected_button_text_color = Color(0xff302f2f);

  //Padding, Margins, Divider and EmptySpace used in the Aldar Application

  static const EdgeInsets button_padding = const EdgeInsets.only(left: 20, right: 20);

 static const String fontName = 'Rubik';

  //Text style used in the Toggle Button
static TextStyle text_style = TextStyle(
    fontFamily: fontName,
    fontSize: 24.0.sp,
  );

}